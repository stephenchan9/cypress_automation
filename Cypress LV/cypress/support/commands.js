// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import { addMatchImageSnapshotCommand } from 'cypress-image-snapshot/command';

addMatchImageSnapshotCommand({
  failureThreshold: 0.03, // threshold for entire image
  failureThresholdType: 'percent', // percent of image or number of pixels
  customDiffConfig: { threshold: 0.1 }, // threshold for each pixel
  capture: 'viewport', // capture viewport in screenshot
  scale: true
});
/*
Cypress.Commands.add('compareScreenshots',(image1, image2, testFolder, threshold) => {
  var filePath = "cd cypress/screenshots/integration/testFolder && pixelmatch img1.png img2.png output.png threshold && output.png";
  var fp = filePath.replace('testFolder', testFolder);
  fp = fp.replace('img1', image1);
  fp = fp.replace('img2', image2);
  fp = fp.replace('threshold', threshold);

  cy.log(fp)
  var url = "www.test.com/output.png";
  var img = new Image();
  img.src = "output.png";
  document.body.appendChild(img);

  cy.exec(fp,{failOnNonZeroExit: false}).then((result)=>{
    cy.writeFile('cypress/integration/imgDiff.txt', result.stdout)
  })
});
*/

//Pan the Map
Cypress.Commands.add('panMap', (x_coord, y_coord, z) => {
  cy.window().then((win) => {
    expect(win.Dmp).to.exist;
    //var map = win.Dmp.Map.getMap();
    var center_of_map = win.Dmp.Map.getCenter(win.map);
    cy.log(center_of_map)
    //Lat y, Long x
    win.Dmp.Map.setView(win.map, { center: { x: x_coord, y: y_coord }, zoom: z });
    cy.wait(20000)
  })
});

Cypress.Commands.add('searchAddress',(address) => {
  //Wait for XHR
  cy.server()
  cy.route('POST', /getByKey.aspx/).as('getParcel')
  //Enters a parcel to Search
  var a = address + '{enter}';
  cy.get('#searchInputBox').clear().type(a)
  cy.wait('@getParcel')
  cy.wait(7000) //Give some time for the modal to load.
  cy.get('#identify_results_section').then(($modal) => {
    var display = $modal.css('display');
    cy.expect(display).to.equal('block')
  })
});

Cypress.Commands.add('searchAddress2',(address) => {
  //Wait for XHR
  cy.server()
  cy.route('POST', /getByKey.aspx/).as('getParcel')

  //Enters a parcel to Search
  var a = address
  cy.get('#searchInputBox').clear().type(a)
  cy.wait(2000)
  cy.get('#searchInputBox').type('{downarrow}{enter}')

  cy.wait('@getParcel')
  cy.wait(7000) //Give some time for the modal to load.
  cy.get('#identify_results_section').then(($modal) => {
    var display = $modal.css('display');
    cy.expect(display).to.equal('block')
  })
});

// Add iframe support until becomes part of the framework
Cypress.Commands.add('iframe', { prevSubject: 'element' }, $iframe => {
  return new Cypress.Promise(resolve => {
    $iframe.on('load', () => {
      resolve($iframe.contents().find('body'))
    })
  })
});

//Drag and Drop file
//Credit to andygock: https://github.com/cypress-io/cypress/issues/669
Cypress.Commands.add('upload_file', (selector, fileUrl, type = '') => {
  return cy.fixture(fileUrl, 'base64')
    .then(Cypress.Blob.base64StringToBlob)
    .then(blob => {
      const nameSegments = fileUrl.split('/')
      const name = nameSegments[nameSegments.length - 1]
      const testFile = new File([blob], name, { type })
      const event = { dataTransfer: { files: [testFile] } }
      return cy.get(selector).trigger('drop', event)
    })
});

//Credit to MysteriousNothing
//https://github.com/cypress-io/cypress/issues/170
Cypress.Commands.add('upload_file2', (fileName, selector) => {
    cy.get(selector).then(subject => {
        cy.fixture(fileName).then((content) => {
            const el = subject[0]
            const testFile = new File([content], fileName)
            const dataTransfer = new DataTransfer()

            dataTransfer.items.add(testFile)
            el.files = dataTransfer.files
        })
    })
});
