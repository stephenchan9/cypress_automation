Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('My First Test', function() {
  before('Opens LandVision', function() {
    cy.visit('apps.spatialstream.com/production/dashboard/8/10/2/BDEst2.aspx?account=bde890&login=ewilliams',{ timeout:40000 }).wait(20000)
  })
  it('Identify parcel and open panel', function() {
    cy.get('body').click(500,300)
	  cy.get('.infoboxText-showMore').click()
	  cy.wait(8000)
    //Open More commands
    cy.get('#summary').contains('More').click()
	  cy.wait(500)
    //Launches Card designer
    cy.get('#summary').contains('Design Cards').click()
	  cy.wait(5000)
    //Open fieldpicker
    cy.get('.no-stripes > tbody > .se-add > .js-data > a').click()
	  cy.wait(1000)
    //Add Value Transfer
    cy.get('#selectableFields').select('VALUE TRANSFER')
	  cy.wait(600)
	  cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2)').click().wait(200)
    //Close Fieldpicker
    cy.get('.new_form > .ui-dialog-titlebar > .ui-dialog-titlebar-close > .ui-icon').click().wait(200)
  	cy.get('.no-stripes > tbody > tr').contains('VALUE TRANSFER').click()
  	cy.get('#summary > ul > li:first').click().wait(1000)
    //Modify added field and save the Surface
    cy.get('div.ui-widget-content > select:first').select('$###,###')
    cy.wait(1700)
    cy.get('input.editAttributeInput:first').clear().type('Value Transfer Test')
    cy.get('.ui-dialog-content > :nth-child(1) > .right').click()
    cy.get('#saveAndClose > .command_name').click()
    cy.get('#transformerSaveOk > .ui-button-text').click().wait(4000)
    //Identify parcel and open panel
    cy.get('body').click(500,300)
    cy.get('.infoboxText-showMore').click()
    cy.wait(4000)
    //Open More Commands
    cy.get('#summary').contains('More').click()
	  cy.wait(500)
    //Launches Card Designer
    cy.get('#summary').contains('Design Cards').click()
	  cy.wait(5000)
    //Resets Surface
    cy.get('#reset > .command_name').click()
	  cy.get('span.ui-button-text:last').click()
  })

})
