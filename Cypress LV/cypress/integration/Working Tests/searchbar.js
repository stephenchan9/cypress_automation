Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Search Bar/Locate Tests', function() {
  //Initial start of the test
  before(function() {
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(10000)
  });
  //End the test when it fails at any point
  afterEach(function() {
  })

  it('Search by Address', function() {
    var address = '273 N CALAVERAS ST, FRESNO, CA'
    //Enter a Address to search
    cy.get('#searchInputBox').clear().type(address)
    cy.get('#searchButton').click()
    cy.waitForParcel()
  });
  it('Search by APN', function() {
    var apn = '650-663-30';
    //Enter a APN to search
    cy.get('#searchInputBox').clear().type(apn)
    cy.get('#searchButton').click()
    cy.waitForParcel()
  })
  it('Search by Lat/Long', function() {
    var coordinates = '36.11158, -115.17558';
    //Enter a APN to search
    cy.get('#searchInputBox').clear().type(coordinates)
    cy.get('#searchButton').click()
    cy.wait(25000)
    cy.get('#identify_results_section .title_group .title').contains(coordinates).should('be.visible')
  })
  it.only('Search by Street Intersection', function(){
    var intersection = 'Long Beach Blvd & E Pacific Coast Hwy, Long Beach, CA 90813'
    //Enter a Intersection to search
    cy.get('#searchInputBox').clear().type(intersection)
    cy.get('#searchButton').click()
    cy.wait(25000)
    cy.get('#identify_results_section .title_group .title').contains(intersection).should('be.visible')
  })
})
