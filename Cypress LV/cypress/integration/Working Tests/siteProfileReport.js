Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Create Site Profile Report', function(){
  before(function(){
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)
  })
  afterEach(function(){
    if (this.currentTest.state === 'failed') {
       Cypress.runner.stop()
     }
  })

  it('Create site profile report with Site Overview, Transaction History, Full Property Detail, Demographics', function(){
    var address = '6157 PREMIERE AVE, LAKEWOOD, CA 90712'
    cy.searchAddress(address)
    //---Wait for the parcel to load
    cy.waitForParcel()
    //---End of Check
    cy.get('.SE_unedit').contains('More').click()
    cy.get('.command_name').contains('Site Profile Report').click()
    cy.wait(4000)
    cy.get('.ui-dialog-title').contains('Report Settings').then(() => {
      cy.get('.ui-button-text').contains('Clear All').click()
      cy.get('label').contains('Site Overview').click()
      cy.get('label').contains('Transaction History').click()
      cy.get('label').contains('Full Property Detail').click()
      cy.get('label').contains('Demographics').click()
      cy.get('.ui-button-text').contains('Generate Report').click()
    })
    cy.get('[title="Site Profile Report"]').should('be.visible').click()
  })
  it('Create site profile report with Birds Eye View, Flood Map Details, Nearby Builder Sites, Demographics', function(){
    var address = '6157 PREMIERE AVE, LAKEWOOD, CA 90712'
    cy.searchAddress(address)
    //---Wait for the parcel to load
    cy.waitForParcel()
    //---End of Check
    cy.get('.SE_unedit').contains('More').click()
    cy.get('.command_name').contains('Site Profile Report').click()
    cy.wait(4000)
    cy.get('.ui-dialog-title').contains('Report Settings').then(() => {
      cy.get('.ui-button-text').contains('Clear All').click()
      cy.get('label').contains('Birds Eye View').click()
      cy.get('label').contains('Flood Map Details').click()
      cy.get('label').contains('Nearby Builder Sites').click()
      cy.get('label').contains('Demographics').click()
      cy.get('.ui-button-text').contains('Generate Report').click()
    })
    cy.get('[title="Site Profile Report"]').should('be.visible')
  })
  it('Create site profile report, select the Site Overview, Neighborhood map and Regional map.', function(){
    var address = '6157 PREMIERE AVE, LAKEWOOD, CA 90712'
    cy.searchAddress(address)
    //---Wait for the parcel to load
    cy.waitForParcel()
    //---End of Check
    cy.get('.SE_unedit').contains('More').click()
    cy.get('.command_name').contains('Site Profile Report').click()
    cy.wait(4000)
    cy.get('.ui-dialog-title').contains('Report Settings').then(() => {
      cy.get('.ui-button-text').contains('Clear All').click()
      cy.get('label').contains('Site Overview').click()
      cy.get('label').contains('Neighborhood & Regional Map').click()
      cy.get('.ui-button-text').contains('Generate Report').click()
    })
    cy.get('[title="Site Profile Report"]').should('be.visible')
  })
  it('Create site profile report, Select Site Overview, Full Property Detail, Transaction History, Birds eye view.', function(){
    var address = '6157 PREMIERE AVE, LAKEWOOD, CA 90712'
    cy.searchAddress(address)
    //---Wait for the parcel to load
    cy.waitForParcel()
    //---End of Check
    cy.get('.SE_unedit').contains('More').click()
    cy.get('.command_name').contains('Site Profile Report').click()
    cy.wait(4000)
    cy.get('.ui-dialog-title').contains('Report Settings').then(() => {
      cy.get('.ui-button-text').contains('Clear All').click()
      cy.get('label').contains('Site Overview').click()
      cy.get('label').contains('Full Property Detail').click()
      cy.get('label').contains('Transaction History').click()
      cy.get('label').contains('Birds Eye View').click()
      cy.get('.ui-button-text').contains('Generate Report').click()
    })
    cy.get('[title="Site Profile Report"]').should('be.visible').click()
  })
  it('Create site profile report, Select Site Overview, Neighborhood Map & Regional Map.', function(){
    var address = '6157 PREMIERE AVE, LAKEWOOD, CA 90712'
    cy.searchAddress(address)
    //---Wait for the parcel to load
    cy.waitForParcel()
    //---End of Check
    cy.get('.SE_unedit').contains('More').click()
    cy.get('.command_name').contains('Site Profile Report').click()
    cy.wait(4000)
    cy.get('.ui-dialog-title').contains('Report Settings').then(() => {
      cy.get('.ui-button-text').contains('Clear All').click()
      cy.get('label').contains('Site Overview').click()
      cy.get('label').contains('Neighborhood & Regional Map').click()
      cy.get('label').contains('Transaction History').click()
      cy.get('label').contains('Birds Eye View').click()
      cy.get('.ui-button-text').contains('Generate Report').click()
    })
    cy.wait(6000)
    cy.get('[title="Site Profile Report"]').should('be.visible').click()
  })
  it('Create site profile report. Test the Select All & Clear All buttons. Verify the Demographics setting goes away if unchecked.',function(){
    var address = '6157 PREMIERE AVE, LAKEWOOD, CA 90712'
    cy.searchAddress(address)
    //---Wait for the parcel to load
    cy.waitForParcel()
    //---End of Check
    cy.get('.SE_unedit').contains('More').click()
    cy.get('.command_name').contains('Site Profile Report').click()
    cy.wait(4000)
    cy.get('.ui-dialog-title').contains('Report Settings').should('be.visible').then(() => {
      cy.get('fieldset').last().then(($id) => {
        var id = $id.attr('id')
        id = '#' + id.replace('_DemogSet_Fieldset', '')
        cy.get(id + '_SelectAll_Button').click()
        cy.get('.ui-button-text').contains('Clear All').click()
        //Block is displayed
        cy.get('label').contains('Demographics').click()
        cy.get(id + '_DemogSet_Div').should('have.attr', 'style', 'display: block;')
        //Block is not displayed
        cy.get('label').contains('Demographics').click()
        cy.get(id + '_DemogSet_Div').should('have.attr', 'style', 'display: none;')
      })
    })
  })
  it('Create site profile report. Select various radius settings and values.', function(){
    var address = '6157 PREMIERE AVE, LAKEWOOD, CA 90712'
    cy.searchAddress(address)
    //---Wait for the parcel to load
    cy.waitForParcel()
    //---End of Check
    cy.get('.SE_unedit').contains('More').click()
    cy.get('.command_name').contains('Site Profile Report').click()
    cy.wait(4000)
    cy.get('.ui-dialog-title').contains('Report Settings').should('be.visible').then(() => {
      cy.get('fieldset').last().then(($id) => {
        var id = $id.attr('id')
        id = '#' + id.replace('_DemogSet_Fieldset', '')
        //Radius
        cy.get(id + '_Radius2_Select').select('1,5')
        //Drive Time (Minutes)
        cy.get(id + '_Drive3_Select').select('10,20,30')
      })
      cy.get('.ui-button-text').contains('Generate Report').click()
    })
  })
  it('Create site profile report. Select 15 min Drive Time demographic settings.', function(){
    var address = '6157 PREMIERE AVE, LAKEWOOD, CA 90712'
    cy.searchAddress(address)
    //---Wait for the parcel to load
    cy.waitForParcel()
    //---End of Check
    cy.get('.SE_unedit').contains('More').click()
    cy.get('.command_name').contains('Site Profile Report').click()
    cy.wait(4000)
    cy.get('.ui-dialog-title').contains('Report Settings').should('be.visible').then(() => {
      cy.get('fieldset').last().then(($id) => {
        var id = $id.attr('id')
        id = '#' + id.replace('_DemogSet_Fieldset', '')
        //Drive Time (Minutes)
        cy.get(id + '_Drive1_Select').select('15')
      })
      cy.wait(8000)
      cy.get('.ui-button-text').contains('Generate Report').click()
    })
  })

})
