Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Export Tests after a Property Search', function(){
  //Open LV and set the boomark view to Irvine.
  before(function(){
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)

  })
  //Ensure that the Search filter modal is open.
  beforeEach(function(){
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Property Search').should('have.value','Property Search')
  })

  it('Export to CSV test.', function() {
    cy.get('.filterItemText').contains('Characteristics').click()
    //Input the years in the box.
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type="text"]:nth-child(3)')
    .clear().type('2000')
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type="text"]:nth-child(5)')
    .clear().type('2018')
    //Last Market Sale 12 months
    cy.get('.filterItemText').contains('Last Market Sale').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(6) > div:nth-child(6) > input[type="radio"]:nth-child(9)').check()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(5000)
    cy.get('#filterFormDiv_filterSearchSelectContainer').should('not.be.visible').wait(10000)
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(20000)
      cy.get('#PropertySearch_0_tab').should('be.visible').then(($tabCount) => {
        expect($tabCount.text()).to.include(count)
      })
    })
    cy.get('#gridLayerCommands [title=Options]').click()
    cy.get('.insertText').contains('Export to CSV').click().wait(5000)
  })
  it('Export to Address Labels test. Owner and Occupant, Include APN, Avery 5160', function(){
    cy.get('.filterItemText').contains('Characteristics').click()
    //Input the years in the box.
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type="text"]:nth-child(3)')
    .clear().type('2000')
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type="text"]:nth-child(5)')
    .clear().type('2018')
    //Last Market Sale 12 months
    cy.get('.filterItemText').contains('Last Market Sale').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(6) > div:nth-child(6) > input[type="radio"]:nth-child(9)').check()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(5000)
    cy.get('#filterFormDiv_filterSearchSelectContainer').should('not.be.visible').wait(10000)

    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(20000)
      cy.get('#PropertySearch_0_tab').should('be.visible').then(($tabCount) => {
        expect($tabCount.text()).to.include(count)
      })
    })
    cy.get('#gridLayerCommands [title=Options]').click()
    cy.get('.insertText').contains('Address Labels').click()
    cy.get('#addresslabel-form').then(($selection) => {
      cy.get('#addresslabel-form > div.mailFieldContainer > div:nth-child(3) > input:nth-child(7)')
      .check()
      cy.get('#addresslabel-form > div.mailFieldContainer > div:nth-child(6) > input:nth-child(1)')
      .check()
      cy.get('#LABEL_TYPE').select('Avery5160')
      cy.get('[value=OK]').click()
    })
    cy.wait(3000)
  })
})
