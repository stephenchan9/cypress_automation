Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Short Tests: Test small filter bar features', function(){
  before(function(){
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)
  })

  it('Filter Bar: Click on Filter Options', function(){
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv_menu').should('be.visible')
  })
  it('Filter Bar: Uncheck some fields in "Property Type".', function(){
    //Click the MultiFamily checkbox
    cy.get('#filtersContainer span').contains('Property Type').trigger('mouseover').click() //Remove the filter
    cy.get('#filterDiv > div:nth-child(4) > input[type="checkbox"]:nth-child(4)').trigger('mouseover').uncheck({force:true})
    cy.get('#filterDiv > div:nth-child(4) > input[type="checkbox"]:nth-child(31)').trigger('mouseover').uncheck({force:true})
    cy.get('#applyFilter_dt').click()
    cy.wait(25000) //Wait for results to load
    cy.get('#viewStatus_viewStatus_count').then(($count) =>{
      expect($count.text()).to.not.eq('0')
      expect($count.text()).to.not.eq(' --')
    })
  })
  //Pan the map. Still needs work
  it('Filter Bar Search Results: Pan the map and observe count change in search results.', function(){
    cy.get('#MapPanel')
      .trigger('mousedown', { which: 1, pageX: 600, pageY: 100 })
      .trigger('mousemove', { which: 1, pageX: 600, pageY: 600 })
      .trigger('mouseup')
  })
  it('Filter Bar: Apply Filter to Layer', function(){
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv_menu').should('be.visible')
    cy.get('#filterDiv [commandname=ApplyFilter]').should('be.visible').click().wait(4000)
  })
  it('Apply Filter to Layer: Select Remove Filter', function(){
    //Click Legend filter icon
    cy.get('#legendIcon').click()
    cy.get('#legend_legendArea').should('be.visible')
  })
  it('Clear Filter', function(){
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv [commandname=ClearFilter]').click().wait(4000)
  })
  it('Remove Filter', function(){
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv [commandname=RemoveFilter]').click().wait(4000)
    //Verify the filter row dissappears.
    cy.get('#filterRow').then(($row)=> {
      var display = $row.css('display');
      expect(display).to.have.string('none');
    })
  })
})

describe('Workflows: Long tests that start from a fresh reload of LV', function(){
  beforeEach(function(){
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)
  })

  it('Perform Filter Search, Add the filter to layer, then delete the layer workflow',function(){
    //Open the filter bar options
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv_menu').should('be.visible')
    //Change the criteria in the filter bar.
    //Click the MultiFamily checkbox
    cy.get('#filtersContainer span').contains('Property Type').trigger('mouseover')
    cy.get('#filterDiv > div:nth-child(4) > input[type="checkbox"]:nth-child(4)').uncheck()
    cy.get('#applyFilter_dt').click()
    cy.wait(4000)
    cy.get('#viewStatus_viewStatus_count').then(($count) =>{
      expect($count.text()).to.not.eq('0')
      expect($count.text()).to.not.eq(' --')
    })
    //Pan the map to get new results.
    cy.get('#MapPanel')
      .trigger('mousedown', { which: 1, pageX: 600, pageY: 100 })
      .trigger('mousemove', { which: 1, pageX: 600, pageY: 600 })
      .trigger('mouseup')
    //Apply filter to layer.
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv_menu').should('be.visible')
    cy.get('#filterDiv [commandname=ApplyFilter]').should('be.visible').click().wait(4000)
    //Click Legend filter icon
    cy.get('#legendIcon').click()
    cy.get('#legend_legendArea').should('be.visible')
    //Remove the filter.
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv [commandname=RemoveFilter]').click().wait(4000)
    cy.get('#filterRow').then(($row)=> { //Verify the filter row dissappears.
      var display = $row.css('display');
      expect(display).to.have.string('none');
    })
  })
  it('Create a new filter in legends modal, Add a couple fields ', function(){
    cy.get('#legendIcon').click()
    cy.get('#legend_legendList [title="Options"]').first().click()
    cy.wait(3000)
    cy.get('.bMCommand .insertText').contains('Create New Filter').first().click()
  })
  it.skip('Edit Filter Option and Add Values while NOT changing the name. Delete the filter afterwards. IN PROGRESS, CURRENT BUG REPORTED IN JIRA, LV-1522', function(){
    // 1. Click the filter bar options in the left hand side of screen.
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv_menu').should('be.visible')
    // 2. Click Edit Filter
    cy.get('.bt-content #filterDiv_menu [commandname=EditFilter]').click()
    cy.get('[aria-labelledby="ui-dialog-title-filterDiv_editor"]').should('be.visible')
    // 3. Edit Search Criteria and Add fields
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    // Address_ID, Textbox Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('ADDRESS_ID').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('TextBoxWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    //Land Market Value, Range-Slider Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('VAL_MRKT_LAND').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('RangeSliderWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    //Improvement Percent, Textbox Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('IMPRV_PCT').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('TextBoxWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    // 4. Click OK
    cy.get('[aria-labelledby=ui-dialog-title-filterDiv_editor] .ui-button-text').contains('OK').click()
    cy.wait(15000)
    // 5. Verify the Search works and was created
    cy.get('.right_arrow').should('be.visible').click()
    cy.get('.filterSpan span').contains('ADDRESS ID').should('exist')
    cy.get('.filterSpan span').contains('LAND MARKET VALUE').should('exist')
    cy.get('.filterSpan span').contains('IMPROVEMENT PERCENT').should('exist')
    cy.get('#applyFilter_dt').click()
    cy.wait(10000)
    // 6. Delete the filter
    cy.get('#filterDiv_selectionButton').click()
    cy.get('.bt-content > #filterDiv_menu > [commandname="DeleteFilter"] > span').click()
  })
  it('Edit Filter Option and Add Values while changing the name. Delete the filter afterwards.', function(){
    // 1. Click the filter bar options in the left hand side of screen.
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv_menu').should('be.visible')
    // 2. Click Edit Filter
    cy.get('.bt-content #filterDiv_menu [commandname=EditFilter]').click()
    cy.get('[aria-labelledby="ui-dialog-title-filterDiv_editor"]').should('be.visible')
    // 3. Edit Search Criteria and Add fields
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    // Address_ID, Textbox Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('ADDRESS_ID').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('TextBoxWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    //Land Market Value, Range-Slider Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('VAL_MRKT_LAND').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('RangeSliderWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    //Improvement Percent, Textbox Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('IMPRV_PCT').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('TextBoxWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    // 4. Edit the name of the Search and click OK
    //Change the name of the search
    var randName = require('randomstring');
    cy.get('#filterDiv_editor_saveAsDiv input').type(randName.generate(7))
    cy.get('[aria-labelledby=ui-dialog-title-filterDiv_editor] .ui-button-text').contains('OK').click()
    cy.wait(10000)
    // 5. Verify the Search works and was created
    cy.get('.right_arrow').should('be.visible').click()
    cy.get('.filterSpan span').contains('ADDRESS ID').should('exist')
    cy.get('.filterSpan span').contains('LAND MARKET VALUE').should('exist')
    cy.get('.filterSpan span').contains('IMPROVEMENT PERCENT').should('exist')
    cy.wait(10000)
    // 6. Delete the filter.
    cy.get('#filterDiv_selectionButton').click()
    cy.get('.bt-content > #filterDiv_menu > [commandname="DeleteFilter"] > span').click()
  })
  //Unable to save search. Getting an error with saving the search as the same name.
  it.skip('Edit Filter Option and Add Values while NOT changing the name. Reset to default afterwards. IN PROGRESS, CURRENT BUG REPORTED IN JIRA, LV-1522', function(){
    // 1. Click the filter bar options in the left hand side of screen.
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv_menu').should('be.visible')
    // 2. Click Edit Filter
    cy.get('.bt-content #filterDiv_menu [commandname=EditFilter]').click()
    cy.get('[aria-labelledby="ui-dialog-title-filterDiv_editor"]').should('be.visible')
    // 3. Edit Search Criteria and Add fields
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    // Address_ID, Textbox Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('ADDRESS_ID').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('TextBoxWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    //Land Market Value, Range-Slider Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('VAL_MRKT_LAND').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('RangeSliderWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    //Improvement Percent, Textbox Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('IMPRV_PCT').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('TextBoxWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.wait(4000)
    // 4. Click OK
    cy.get('[aria-labelledby=ui-dialog-title-filterDiv_editor] .ui-button-text').contains('OK').click()
    cy.wait(15000)
    // 5. Verify the Search works and was created
    cy.get('.right_arrow').should('be.visible').click()
    cy.get('.filterSpan span').contains('ADDRESS ID').should('exist')
    cy.get('.filterSpan span').contains('LAND MARKET VALUE').should('exist')
    cy.get('.filterSpan span').contains('IMPROVEMENT PERCENT').should('exist')
    cy.get('#applyFilter_dt').click()
    cy.wait(10000)
    // 6. Click on Reset to Default
    cy.get('#filterDiv_selectionButton').click()
    cy.get('.bt-content > #filterDiv_menu > [commandname="ResetDefault"] > span').click()
  })
  it('Edit Filter Option, Edit Property Search and Rename the Filter. Then delete the fields that were added.', function(){
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv_menu').should('be.visible')
    cy.get('.bt-content #filterDiv_menu [commandname=EditFilter]').click()
    cy.get('[aria-labelledby="ui-dialog-title-filterDiv_editor"]').should('be.visible')
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    // Address_ID, Textbox Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('ADDRESS_ID').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('TextBoxWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    //Land Market Value, Range-Slider Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('VAL_MRKT_LAND').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('RangeSliderWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    //Improvement Percent, Textbox Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('IMPRV_PCT').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('TextBoxWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    //Rename the Search and Click Ok
    var random_str = require("randomstring");
    cy.get('#filterDiv_editor_saveAsDiv input').type(random_str.generate(3))
    cy.get('[aria-labelledby=ui-dialog-title-filterDiv_editor] .ui-button-text').contains('OK').click()
    cy.wait(15000)
    //Verify the New Search works Still by clicking Apply Filter
    cy.get('.right_arrow').should('be.visible').click()
    //cy.get('#applyFilter_dt').click()
    cy.wait(10000)
    //Delete the fields added.
    cy.get('#filterDiv_selectionButton').should('be.visible').click()
    cy.get('#filterDiv_menu').should('be.visible')
    cy.get('.bt-content #filterDiv_menu [commandname=EditFilter]').click()
    for (var i =0; i<3; i++){
      cy.get('#filterDiv_formList .WidgetEditorBody .RemoveWidget a').last().click()
    }
    cy.get('[aria-labelledby=ui-dialog-title-filterDiv_editor] .ui-button-text').contains('OK').click()
  })
  it('Create a new filter from legends modal, and add some new criteria.', function(){
    cy.get('#legendIcon').click()
    cy.get('#legend_legendList [title="Options"]').first().click()
    cy.wait(3000)
    cy.get('.bMCommand .insertText').contains('Create New Filter').first().click()
    var randName = require('randomstring');
    cy.get('#NewFilterNameRow input').type(randName.generate(7))
    cy.get('.ui-button-text').contains('OK').click()

    //Add Search Criteria
    // Address_ID, Textbox Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('ADDRESS_ID').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('TextBoxWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    //Land Market Value, Range-Slider Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('VAL_MRKT_LAND').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('RangeSliderWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.get('.ui-button-text').contains('Add Search Criteria').click()
    //Improvement Percent, Textbox Widget
    cy.get('#filterDiv_addSearchCrit .FieldSelector').select('IMPRV_PCT').then(($byField) => {
      cy.get('#filterDiv_addSearchCrit .WidgetSelector').first().select('TextBoxWidget')
      cy.get('.ui-dialog-buttonset .ui-button-text').contains('Add').click()
    })
    cy.get('[aria-labelledby=ui-dialog-title-filterDiv_editor] .ui-button-text').contains('OK').click()
    cy.wait(10000)
  })
})
