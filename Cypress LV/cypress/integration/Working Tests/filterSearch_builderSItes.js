Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Builder Sites Filter', function() {
  //Open LV
  before(function(){
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)
  })
  //Click on the Search Filter
  beforeEach(function() {
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').should('be.visible').select('Builder Sites')
    cy.get('#filterFormDiv_filterSearchSelect').should('have.value', 'Builder Sites')
    cy.wait(4000)
  });
  it('Click the Select All and apply', function(){
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('Select All').click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click()
    cy.wait(4000)
    cy.get('#filterFormDiv_filterFormContainer').should('not.be.visible')
  });
  it('Click Deselect All and apply', function(){
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('Deselect All').click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click()
    cy.wait(4000)
    cy.get('#filterFormDiv_filterFormContainer').should('not.be.visible')
    cy.get('#applyFilter_dt').click()
  });
  it('Input text in the first Builder Name input box and click ok', function(){
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(156) > input[type="text"]:nth-child(3)')
    .clear().type('John')
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click()
  })
})
