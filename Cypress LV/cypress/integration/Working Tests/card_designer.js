Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

//Reset the Card Designer. Test Teardown.
var resetToDefault= function(address){
  //Enter a Parcel to search
  cy.searchAddress(address)
  cy.waitForParcel();
  cy.get('#summary').contains('More').click()
  //Open Card Designer
  cy.get('#summary').contains('Design Cards').click()
  cy.get('[data-surfedit=card_group]').should('be.visible')
  cy.get('#reset').click()
  cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > .ui-button > .ui-button-text').last().click({force:true})
}

describe('Card Designer Feature Tests', function() {
  //Define random data to use
  const randomLabel ='Val$%&***&()'
  const randomDesc ='&**&*&&*78821'
  const address = '273 N CALAVERAS ST, FRESNO, CA'
  //Initial start of the test
  beforeEach(function() {
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Fresno').click().wait(8000)
    resetToDefault(address);
  });
  it.only('Add Field: Create a New Field and delete.', function() {
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Field Link
    cy.get('[id-surfedit="addTableField"]').first().click()
    cy.get('#addFieldForm').should('be.visible')
    //Select a Field and click Add then close the module.
    cy.get('#selectableFields').select('VALUE TRANSFER')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"]').within(($addFieldModal) => {
      cy.get('.ui-button-text').contains('Add').click()
      cy.get('span').contains('close').click()
    })
    //Verify the Field was added, then click to modify.
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('be.visible').click()
    cy.get('.command_name').contains('Modify').click()
    //Input special characters into the fields.
    //Label Input Box
    cy.get('.editAttributeLabel').contains('Label: ').next().then(($label) => {
      cy.wrap($label).clear().type(randomLabel)
    })
    //Description Input Box
    cy.get('.editAttributeLabel').contains('Description: ').next().then(($description) => {
      cy.wrap($description).clear().type(randomDesc)
    })
    //Select a Formatter
    //Formatter
    cy.get('.editAttributeLabel').contains('Formatter: ').next().then(($formatter) =>{
      cy.wrap($formatter).select('###,###')
    })
    //Click Ok button to save and verify label is in effect.
    cy.get('.editAttributeSubmit').contains('OK').click()
    cy.get('[data-surfedit=tableField_label]').contains(randomLabel)
    //Click Save to USER folder.
    cy.get('#saveAndClose').click().wait(5000)
    cy.get('#transformerSaveOk .ui-button-text').click().wait(5000)
    //Delete the newly saved field and save.
    cy.get('#searchButton').click()
    cy.waitForParcel();
    cy.get('#summary').contains('More').click()
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    var clickDelete = function(id){
      cy.get(id).click()
    };
    cy.get('[data-surfedit=tableField_label]').contains(randomLabel).scrollIntoView().click().then(($field) => {
      var id_val = $field.attr('data-dropdown');
      id_val = '#' + id_val.replace('_infoBox', '_delete');
      clickDelete(id_val);
    })
    cy.get('[data-surfedit=tableField_label]').contains(randomLabel).should('not.be.visible')
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk .ui-button-text').click().wait(5000)
  });
  it('Add Field: Create a New Field and change formatter. Delete created field afterwards.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    cy.waitForParcel();
    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(8000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Field Link
    cy.get('[id-surfedit="addTableField"]').first().click()
    cy.get('#addFieldForm').should('be.visible')
    //Select a Field and click Add then close the module
    cy.get('#selectableFields').select('VALUE TRANSFER')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"]').within(($addFieldModal) => {
      cy.get('.ui-button-text').contains('Add').click()
      cy.get('span').contains('close').click()
    })
    //Verify the Field was added and then click to modify
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('be.visible').click()
    cy.get('.command_name').contains('Modify').click()
    //Input special characters into the fields.
    //Label Input Box
    cy.get('.editAttributeLabel').contains('Label: ').next().then(($label) => {
      cy.wrap($label).clear().type(randomLabel)
    })
    //Description Input Box
    cy.get('.editAttributeLabel').contains('Description: ').next().then(($description) => {
      cy.wrap($description).clear().type(randomDesc)
    })
    //Select a Formatter
    cy.get('.editAttributeLabel').contains('Formatter: ').next().then(($formatter) =>{
      cy.wrap($formatter).select('###,###.00')
    })
    //Click the Ok button to save and verify the label is in effect
    cy.get('.editAttributeSubmit').contains('OK').click()
    cy.get('[data-surfedit=tableField_label]').contains(randomLabel)
    //Click Save to USER folder
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk .ui-button-text').click().wait(5000)
    //Delete the newly saved field and save
    cy.get('#searchInputBox').clear().type(address)
    cy.get('#searchButton').click()
    cy.waitForParcel()
    cy.get('#summary').contains('More').click()
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(6000)
    cy.get('#addCard > .command_name').should('be.visible')
    var clickDelete = function(id){
      cy.get(id).click()
    };
    cy.get('[data-surfedit=tableField_label]').contains(randomLabel).scrollIntoView().click().then(($field) => {
      var id_val = $field.attr('data-dropdown');
      id_val = '#' + id_val.replace('_infoBox', '_delete');
      clickDelete(id_val);
    })
    cy.get('[data-surfedit=tableField_label]').contains(randomLabel).should('not.be.visible')
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk .ui-button-text').click().wait(5000)
  });
  it('Add Command: Add a new command and save', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(8000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Manage Documents')
    cy.get('.editAttributeSubmit').click()
    cy.get('.command_name').contains('Manage Documents').scrollIntoView().should('be.visible')
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(5000)
    cy.get('#summary').contains('Manage Documents').should('be.visible')
  })
  it('Select the first card group and move the card up or down.', function(){
    const selectedField ='Year Built'
    //Enters a parcel to Search
    cy.searchAddress(address)
    cy.waitForParcel()
    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(8000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('#addList > .command_name').should('be.visible')
    //Move the fields up or down.
    cy.get('[id-surfedit=tableField_group1]').within(($cardGroup) =>{
      //Functions to move the field up/down
      var moveUp = function(id) {
        id = '#' + id;
        id = id.replace('_infoBox','_up')
        cy.get(id).click()
      };
      var moveDown = function(id) {
        id = '#' + id;
        id = id.replace('_infoBox','_down')
        cy.get(id).click()
      }
      cy.get('[data-surfedit=tableField_label]').contains(selectedField).then(($label_selected) => {
        var id_val = $label_selected.attr('data-dropdown')
        moveUp(id_val)
        cy.wait(3000)
        moveDown(id_val)
      })
    })
    cy.get('[title="Close"]').click({multiple:true})
  })
  it('Add Command: Create a new command (Copy Geometry) with all fields checked.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    cy.get('#summary').contains('More').click().wait(8000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Dmp.Toolkit.Commands.CopyGeometryCommand')
    cy.get('.editAttributeSubmit').click()
    var clickCommand = function(s){
      cy.get('#summary').contains(s).click()
    };
    //Create a new name for the command and add settings.
    var randomstring = require("randomstring");
    randomstring = "Copy Geometry " + randomstring.generate(3);
    cy.get('.ui-dialog-title').contains('Copy Geometry Command').should('be.visible').then(($copyGeometry) => {
      //Command Label name and checkboxes.
      cy.get('.editAttributeLabel').contains('Command Label').next().clear().type(randomstring)
      cy.get('.editAttributeLabel').contains('Copy Fields').prev().check()
      cy.get('.editAttributeLabel').contains('Hide Layer Picker').prev().check()
      cy.get('.editAttributeLabel').contains('Suppress Edit Form').prev().check()
      //Select the layer to save to.
      cy.get('.editAttributeInput').contains('Browse').last().click()
      cy.wait(5000)
      cy.get('#mruDropDown_8').select('DRAWINGS',{force:true})
      cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
        var layerSelected = "Markup";
        var id = $layerModal.attr('id');
        id = id.replace('ui-dialog-title-', '')
        var list_selection_id = '#' + id + '_availableList'
        var ok_button = '#' + id + '_okButton'
        cy.get(list_selection_id).select(layerSelected)
        cy.get(ok_button).click()
      })
      //Submit
      cy.get('.editAttributeSubmit').click()
    })
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    cy.get('#summary').contains('More').click().wait(5000)
    //Click the newly created command.
    cy.get('#summary').contains(randomstring).click()
    cy.get('.editAttributeInput').contains('Browse').click()
    cy.wait(3000)
    cy.get('#mruDropDown_8').select('SHARE',{force:true})
    //Select the Layer
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Project Sites";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    cy.get('.editAttributeSubmit').contains('OK').click()
    cy.server()
    cy.route('POST','/Transaction.aspx').as('getInfo')
    //Verify the Command works.
    cy.get('#summary').contains(randomstring).click()
    //---Wait for the Transaction Request to appear.
    cy.wait('@getInfo').then((xhr) => {
      cy.log('Success')
    })
  })
  it('Add Command: Create a new command (Copy Geometry) with all fields except "Suppress Edit Form" checked.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    cy.get('#summary').contains('More').click().wait(8000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Dmp.Toolkit.Commands.CopyGeometryCommand')
    cy.get('.editAttributeSubmit').click()
    var clickCommand = function(s){
      cy.get('#summary').contains(s).click()
    };
    //Create a new name for the command and add settings.
    var randomstring = require("randomstring");
    randomstring = "Copy Geometry " + randomstring.generate(3);
    cy.get('.ui-dialog-title').contains('Copy Geometry Command').should('be.visible').then(($copyGeometry) => {
      //Command Label name and checkboxes.
      cy.get('.editAttributeLabel').contains('Command Label').next().clear().type(randomstring)
      cy.get('.editAttributeLabel').contains('Copy Fields').prev().check()
      cy.get('.editAttributeLabel').contains('Hide Layer Picker').prev().check()
      //Select the layer to save to.
      cy.get('.editAttributeInput').contains('Browse').last().click()
      cy.wait(5000)
      cy.get('#mruDropDown_8').select('DRAWINGS',{force:true})
      cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
        var layerSelected = "Markup";
        var id = $layerModal.attr('id');
        id = id.replace('ui-dialog-title-', '')
        var list_selection_id = '#' + id + '_availableList'
        var ok_button = '#' + id + '_okButton'
        cy.get(list_selection_id).select(layerSelected)
        cy.get(ok_button).click()
      })
      //Submit
      cy.get('.editAttributeSubmit').click()
    })
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    cy.get('#summary').contains('More').click().wait(5000)
    //Click the newly created command.
    cy.get('#summary').contains(randomstring).click()
    cy.get('.editAttributeInput').contains('Browse').click()
    cy.wait(3000)
    cy.get('#mruDropDown_8').select('SHARE',{force:true})
    //Select the Layer
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Project Sites";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    cy.get('.editAttributeSubmit').contains('OK').click()
    //Verify the Command works.
    cy.get('#summary').contains(randomstring).click()
  })
  it('Add Command: Create a new command (Copy Geomtery) with no fields checked.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    cy.get('#summary').contains('More').click().wait(8000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Dmp.Toolkit.Commands.CopyGeometryCommand')
    cy.get('.editAttributeSubmit').click()
    var clickCommand = function(s){
      cy.get('#summary').contains(s).click()
    };
    //Create a new name for the command and add settings.
    var randomstring = require("randomstring");
    randomstring = "Copy Geometry " + randomstring.generate(3);
    cy.get('.ui-dialog-title').contains('Copy Geometry Command').should('be.visible').then(($copyGeometry) => {
      //Submit
      cy.get('.editAttributeSubmit').click()
    })
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    cy.get('#summary').contains('More').click().wait(5000)
    //Click the newly created command.
    cy.get('#summary').contains(randomstring).click()
    cy.get('.editAttributeInput').contains('Browse').click()
    cy.wait(3000)
    cy.get('#mruDropDown_8').select('SHARE',{force:true})
    //Select the Layer
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Project Sites";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    cy.get('.editAttributeSubmit').contains('OK').click()
    //Verify the Command works.
    cy.get('#summary').contains(randomstring).click()
  })
})

describe('Misc Card Designer tests', function(){
  const randomLabel ='Val$%&***&()'
  const randomDesc ='&**&*&&*78821'
  const address = '273 N CALAVERAS ST, FRESNO, CA'
  //Initial start of the test
  beforeEach(function() {
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Fresno').click().wait(8000)
    resetToDefault(address);
  });
  it('Create Card: Create a Card with Table Template: NOT RUNNING', function() {
    cy.log('test')
    cy.searchAddress(address)
    cy.waitForParcel()
    //Open More command
    cy.get('#summary').contains('More').click()
    //Click Design Card command.
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(8000)
    cy.get('#addCard > .command_name').should('be.visible')
    //Add card and input data.
    cy.get('.command_name').contains('Add Card').click()
    cy.get('#addCard').should('be.visible')
    var randomstring = require("randomstring")
    var randStr = randomstring.generate(7)
    cy.get('#cardLabel').clear().type(randStr)
    cy.get('#selectCardTemplate').select('Table').should('have.value', 'Table')
    cy.get('[aria-labelledby="ui-dialog-title-addCardForm"] .ui-button-text').contains('OK').click()
    cy.wait(4000)
    cy.get('[data-surfedit="card_header"]').last().then(() => {
      cy.get('h6').contains(randStr)
    })
    //Delete newly created card.
    cy.get('#summary section .is-cardless').last().then(() => {
      cy.get('.fa-trash').click()
    })
    cy.get('.SE_unedit').contains('Delete').click()
    //Save the settings to USER Folder.
    cy.get('#saveAndClose').click()
    cy.wait(3000)
    cy.get('#transformerSaveOk .ui-button-text').click()
    cy.wait(5000)
  })
  it('Add Field: Add new fields and do not save changes. Verify the fields were NOT saved', function(){
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Field Link
    cy.get('[id-surfedit="addTableField"]').first().click()
    cy.get('#addFieldForm').should('be.visible')
    //Select Fields and click Add then close the module.
    cy.get('#selectableFields').select('VALUE TRANSFER')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"]').within(() => {
      cy.get('.ui-button-text').contains('Add').click()
    })
    cy.get('#selectableFields').select('BUYER NAME')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"]').within(() => {
      cy.get('.ui-button-text').contains('Add').click()
    })
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"] .ui-icon-closethick').contains('close').click() //Close dialog box.
    //Verify the Field was added.
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('be.visible')
    cy.get('[data-surfedit=tableField_label]').contains('BUYER NAME').should('be.visible')
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Verify the fields were not saved.
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('not.be.visible')
    cy.get('[data-surfedit=tableField_label]').contains('BUYER NAME').should('not.be.visible')
  })
  it('Add Field: Create a New Field and modify the field. Close card designer and verify changes were NOT saved.', function(){
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Field Link
    cy.get('[id-surfedit="addTableField"]').first().click()
    cy.get('#addFieldForm').should('be.visible')
    //Select Fields and click Add
    cy.get('#selectableFields').select('VALUE TRANSFER')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"]').within(() => {
      cy.get('.ui-button-text').contains('Add').click()
    })
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"] .ui-icon-closethick').contains('close').click() //Close dialog box.
    //Verify the Field was added,then click to modify
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('be.visible').click()
    cy.get('.command_name').contains('Modify').click()
    //Input special characters into the fields.
    //Label Input Box
    cy.get('.editAttributeLabel').contains('Label: ').next().then(($label) => {
      cy.wrap($label).clear().type(randomLabel)
    })
    //Description Input Box
    cy.get('.editAttributeLabel').contains('Description: ').next().then(($description) => {
      cy.wrap($description).clear().type(randomDesc)
    })
    //Select a Formatter
    cy.get('.editAttributeLabel').contains('Formatter: ').next().then(($formatter) =>{
      cy.wrap($formatter).select('###,###.00')
    })
    //Click the Ok button to save and verify the label is in effect
    cy.get('.editAttributeSubmit').contains('OK').click()
    cy.get('[data-surfedit=tableField_label]').contains(randomLabel)
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Verify the fields were not saved.
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('not.be.visible')
  })
  it('Delete Field: Delete a default field accidentally and close the card. Ensure changes were NOT saved.', function(){
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Delete the field
    cy.get('[data-surfedit=tableField_label]').contains('Site Address').should('be.visible')
    var clickDelete = function(id){
      cy.get(id).click()
    };
    cy.get('[data-surfedit=tableField_label]').contains('Site Address').scrollIntoView().click().then(($field) => {
      var id_val = $field.attr('data-dropdown');
      id_val = '#' + id_val.replace('_infoBox', '_delete');
      clickDelete(id_val);
    })
    cy.get('[data-surfedit=tableField_label]').contains('Site Address').should('not.be.visible')
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('[data-surfedit=tableField_label]').contains('Site Address').should('be.visible')
  })
  it('Add Command: Close card designer and verify changes were NOT saved.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(8000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Manage Documents')
    cy.get('.editAttributeSubmit').click()
    cy.get('.command_name').contains('Manage Documents').scrollIntoView().should('be.visible')
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('[data-surfedit=tableField_label]').contains('Manage Documents').should('not.be.visible')
  })
  it('Add Command: Add a new command and modify the command. Close the card and ensure changes were NOT saved.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(8000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Manage Documents')
    cy.get('.editAttributeSubmit').click()
    cy.get('.command_name').contains('Manage Documents').scrollIntoView().should('be.visible').click()
    cy.get('.command_name').contains('Modify').click({force:true}).then(() => {
      cy.get('.editAttributeLabel').contains('Label').next().clear().type('Test')
      cy.get('.editAttributeLabel').contains('Description').next().clear().type('Testing description area.')
      cy.get('.editAttributeSubmit').click()
    })
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('[data-surfedit=tableField_label]').contains('Manage Documents').should('not.be.visible')
  })
  it('Delete Command: Delete a default command field accidentally and close the card. Ensure changes were NOT saved.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(8000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Delete default command
    var clickDelete = function(id){
      cy.get(id).click()
    };
    cy.get('[data-surfedit="command_item"]').contains('Site Profile Report').scrollIntoView().click().then(($command) => {
      var id_val = $command.attr('data-dropdown');
      id_val = '#' + id_val.replace('_infoBox', '_delete');
      clickDelete(id_val);
    })
    cy.get('.command_name').contains('Site Profile Report').should('not.be.visible')
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('.command_name').contains('Site Profile Report').scrollIntoView().should('be.visible')
  })
  it('Help: Verify help icon displays Card Designer info', function(){
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('#surfaceEditorHelp').click()
    cy.get('[aria-labelledby="ui-dialog-title-helpDialog"]').should('be.visible')
    cy.get('th').contains('Card Designer').should('be.visible')
  })
})

describe('Card Designer Layer Relationship Tests', function(){
  //Define random data to use
  const randomLabel ='Val$%&***&()'
  const randomDesc ='&**&*&&*78821'
  const address = '11206 HARVARD DR'
  //Initial start of the test
  beforeEach(function() {
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(20000)
    resetToDefault(address);
  });
  it('Layer Relationship: Create a Layer Relationship from a layer.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR
    cy.waitForParcel()
    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(8000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add List Command
    cy.get('#addList').first().click()
    cy.get('#ui-dialog-title-addListForm').should('be.visible')
    cy.get('#selectLink').select('New Layer Relationship').wait(3000)
    //Select a layer dialog.
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Manholes";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    //Layer Relationship Modal
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"]').then(($layerRelationship) => {
      var relationshipName = randomstring.generate(7);
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(5000)
      //Verify layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })
    //Input Header Name
    var randomstring = require("randomstring");
    var headerTitle = randomstring.generate(7)
    cy.get('#cardLabel').type(headerTitle)
    //Click Ok in the Add List Modal
    cy.wait(3000)
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"] button').contains('OK').click()
    //Verify the List was created
    cy.wait(3000)
    cy.get('[data-surfedit="card_group"] .card-section .title').contains(headerTitle).should('exist')
  })
  it('Layer Relationship: Create a Layer Relationship in user loaded layer(Manholes) and add datatype fields. Modify the fields and verify the format of the fields are correct for a parcel in the layer', function(){
    // 1. Enter a parcel to search in user loaded layer.
    var address1 = '6385 WINSTON TRCE, MCDONOUGH, GA 30252';
    var address2 = '5055 YELLOW PINE DR, MCDONOUGH, GA 30252';
    cy.searchAddress(address1)
    // Wait for XHR
    cy.waitForParcel()
    // 2. Open Card Designer and click Add List command.
    cy.get('#summary').contains('More').click().wait(8000)
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('#addList').first().click()
    cy.get('#ui-dialog-title-addListForm').should('be.visible')
    // 3. Create a new layer relationship and select the Manholes layer.
    cy.get('#selectLink').select('New Layer Relationship').wait(3000)
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Manholes";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    // 4. Create the Layer Relationship by selecting and inputting different fields.
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"]').then(($layerRelationship) => {
      var relationshipName = randomstring.generate(7);
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      cy.get('#Buffer').type('500')
      cy.get('#Unit').select('Feet')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(4000)
      //Verify layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })
    // 5. Input the header name on the add to list section.
    var randomstring = require("randomstring");
    var headerTitle = randomstring.generate(7)
    cy.get('#cardLabel').type(headerTitle)
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"] button').contains('OK').click()
    // 6. Verify the list was created in card designer.
    cy.wait(3000)
    cy.get('[data-surfedit="card_group"] .card-section .title').contains(headerTitle).should('exist')
    // 7. Click add field on the list and add datatypes.
    cy.get('[id-surfedit="addTableField"] a').last().click()
    cy.get('#addFieldForm').should('be.visible')
    //Date
    cy.get('#selectableFields').select('Gnss Heigh')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Integer
    cy.get('#selectableFields').select('Gps Week')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Text
    cy.get('#selectableFields').select('Gps Second')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    // Time
    cy.get('#selectableFields').select('Gps Date')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    // Time
    cy.get('#selectableFields').select('Last Updat')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    cy.get('.new_form > .ui-dialog-titlebar > .ui-dialog-titlebar-close').contains('close').click()
    // 8. Verify the fields were added to the list.
    cy.get('[data-surfedit="tableField_label"]').contains('Gnss Heigh').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Week').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Second').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Date').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Last Updat').scrollIntoView().should('be.visible')
    // 9. Modify the fields
    //Gnss Heigh
    cy.get('[data-surfedit="tableField_label"]').contains('Gnss Heigh').last().click().then(($gnssHeigh) => {
      var id = $gnssHeigh.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('###,###.00',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Gps Week
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Week').last().click().then(($gpsWeek) => {
      var id = $gpsWeek.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('###,###',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Gps Second
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Second').last().click().then(($gpsSecond) => {
      var id = $gpsSecond.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('$###,###',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Gps Date
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Date').last().click().then(($gpsDate) => {
      var id = $gpsDate.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('M/DD/YY',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Last Updat
    cy.get('[data-surfedit="tableField_label"]').contains('Last Updat').last().click().then(($lastUpdate) => {
      var id = $lastUpdate.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('M/DD/YY HH:mm a',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    // 10. Save the Card
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    // 11. Search a different address in the layer and verify the datatypes are correct.
    cy.searchAddress(address2)
    cy.wait(10000)
    // Wait for XHR
    cy.waitForParcel()
    // 12. Verify the header with dataypes are correct using regex
    cy.get('.title').contains(headerTitle).scrollIntoView().should('be.visible').then(($header) => {
      var id = $header.attr('id');
      id = '#' + id.replace('CardLabel','');
      //Verify 1st Field (Gps Heigh)/ ###,###.00
      cy.get(id+ ' [data-surfedit="tableField_label"]').contains('Gnss Heigh').next().then(($gnssHeigh) => {
        var v ="" + $gnssHeigh.text();
        expect(v).to.match(/(^\d{3})\,(\d{3})\.(\d{2}$)/)
      })
      //Verify 2nd Field (GPS Week)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Gps Week').next().then(($gpsWeek) => {
        var v = "" + $gpsWeek.text();
        expect(v).to.match(/(^\d{3})\,(\d{3}$)/)
      })
      //Verify 3rd field (GPS Second)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Gps Second').next().then(($gpsSecond) => {
        var v ="" + $gpsSecond.text();
        expect(v).to.match(/(^\$)(\d{3})\,(\d{3}$)/)
      })
      //verify the 4th field (GPS Date)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Gps Date').next().then(($gpsDate) => {
        var v ="" + $gpsDate.text();
        expect(v).to.match(/(^\d{1,2})\/(\d{1,2})\/(\d{1,2}$)/)
      })
      //verify the 5th field (GPS Date)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Last Updat').next().then(($lastUpdate) => {
        var v ="" + $lastUpdate.text();
        expect(v).to.match(/(^\d{1,2})\/(\d{1,2})\/(\d{1,2})\s(\d{1,2})\:(\d{1,2})\s([a|p])([m]$)/)
      })
    })
  })
  it('Layer Relationship: Create a Layer Relationship in user loaded layer(Manholes) and add different datatype fields. Modify the fields and verify the format of the fields are correct for a parcel in the layer', function(){
    // 1. Enter a parcel to search in user loaded layer.
    var address1 = '100 CITY PARK DR, MCDONOUGH, GA 30252';
    var address2 = '8190 CHAPMAN TER, MCDONOUGH, GA 30252';
    cy.searchAddress(address1)
    // Wait for XHR
    cy.waitForParcel()
    // 2. Open Card Designer and click Add List command.
    cy.get('#summary').contains('More').click().wait(8000)
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('#addList').first().click()
    cy.get('#ui-dialog-title-addListForm').should('be.visible')
    // 3. Create a new layer relationship and select the Manholes layer.
    cy.get('#selectLink').select('New Layer Relationship').wait(3000)
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Manholes";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    // 4. Create the Layer Relationship by selecting and inputting different fields.
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"]').then(($layerRelationship) => {
      var relationshipName = randomstring.generate(7);
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      cy.get('#Buffer').type('500')
      cy.get('#Unit').select('Feet')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(4000)
      //Verify layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })
    // 5. Input the header name on the add to list section.
    var randomstring = require("randomstring");
    var headerTitle = randomstring.generate(7)
    cy.get('#cardLabel').type(headerTitle)
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"] button').contains('OK').click()
    // 6. Verify the list was created in card designer.
    cy.wait(3000)
    cy.get('[data-surfedit="card_group"] .card-section .title').contains(headerTitle).should('exist')
    // 7. Click add field on the list and add datatypes.
    cy.get('[id-surfedit="addTableField"] a').last().click()
    cy.get('#addFieldForm').should('be.visible')
    //Date
    cy.get('#selectableFields').select('Latitude')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Integer
    cy.get('#selectableFields').select('Longitude')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Text
    cy.get('#selectableFields').select('Location')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    // 8. Verify the fields were added to the list.
    cy.get('[data-surfedit="tableField_label"]').contains('Latitude').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Longitude').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Location').scrollIntoView().should('be.visible')
    // 9. Modify the fields
    //Latitude
    cy.get('[data-surfedit="tableField_label"]').contains('Latitude').last().click({force:true}).then(($latitude) => {
      var id = $latitude.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('###,###.00')
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Longitude
    cy.get('[data-surfedit="tableField_label"]').contains('Longitude').last().click().then(($longitude) => {
      var id = $longitude.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('###,###',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Location
    cy.get('[data-surfedit="tableField_label"]').contains('Location').last().click().then(($location) => {
      var id = $location.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('$###,###',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    // 10. Save the Card
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    // 11. Search a different address in the layer and verify the datatypes are correct.
    cy.searchAddress(address2)
    // Wait for XHR
    cy.waitForParcel()
    // 12. Verify the header with dataypes are correct using regex
    cy.get('.title').contains(headerTitle).scrollIntoView().should('be.visible').then(($header) => {
      var id = $header.attr('id');
      id = '#' + id.replace('CardLabel','');
      //Verify 1st Field (Gps Heigh)/ ###,###.00
      cy.get(id+ ' [data-surfedit="tableField_label"]').contains('Latitude').next().then(($gnssHeigh) => {
        var v ="" + $gnssHeigh.text();
        expect(v).to.match(/(^\d{3})\,(\d{3})\.(\d{2}$)/)
      })
      //Verify 2nd Field (GPS Week)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Longitude').next().then(($gpsWeek) => {
        var v = "" + $gpsWeek.text();
        expect(v).to.match(/(^\d{3})\,(\d{3}$)/)
      })
      //Verify 3rd field (GPS Second)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Location').next().then(($gpsSecond) => {
        var v ="" + $gpsSecond.text();
        expect(v).to.match(/(^\$)(\d{3})\,(\d{3}$)/)
      })
    })
  })
  it('Layer Relationship: Linking a layer relationship with multi layer relationship. ER: 2', function(){
    //Values pre determined.
    var address1 = '3591 CERRITOS AVE, LOS ALAMITOS, CA 90720';
    var number_of_cards = 2;
    var randomstring = require("randomstring");
    var headerTitle = randomstring.generate(7);
    cy.searchAddress(address1)
    // Wait for XHR
    cy.waitForParcel()
    // 2. Open Card Designer and click Add List command.
    cy.get('#summary').contains('More').click().wait(8000)
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('#addList').first().click()
    cy.get('#ui-dialog-title-addListForm').should('be.visible')
    // 3. Create a new layer relationship with given filepath.
    cy.get('#selectLink').select('New Layer Relationship').wait(3000)
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      cy.get('#mruText_8').clear().type('ss.demog.easi{enter}')
      var layerSelected = "Demographics";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    // 4. Create the Layer Relationship by selecting and inputting different fields.
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"]').then(($layerRelationship) => {
      var relationshipName = randomstring.generate(7);
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(4000)
      //Verify layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })
    //5. Create the Multi Layer Relationship
    cy.get('#selectLink').select('New Multi Layer Relationship').wait(3000)
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      cy.get('#mruText_8').clear().type('ss.admin.mp.publicschools{enter}')
      var layerSelected = "PublicSchools";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    // 6. Create the Multi Layer Relationship by selecting and inputting different fields.
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"]').then(($multLayerRelationship) => {
      var relationshipName = randomstring.generate(7);
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(4000)
      // 7.Verify Multi layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })
    // 8. Input the header name on the add to list section.
    cy.get('#cardLabel').type(headerTitle)
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"] button').contains('OK').click()
    // 9. Verify the list was created in card designer.
    cy.wait(3000)
    cy.get('[data-surfedit="card_group"] .card-section .title').contains(headerTitle).should('exist')
    // 10. Click add field on the list and add datatypes.
    cy.get('[id-surfedit="addTableField"] a').last().click()
    cy.get('#addFieldForm').should('be.visible')
    //Date
    cy.get('#selectableFields').select('District')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Integer
    cy.get('#selectableFields').select('Ed Level')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Text
    cy.get('#selectableFields').select('Sch Name')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    // Time
    cy.get('#selectableFields').select('Sch Type')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    cy.get('.new_form > .ui-dialog-titlebar > .ui-dialog-titlebar-close').contains('close').click()
    // 11. Verify the fields were added to the list.
    cy.get('[data-surfedit="tableField_label"]').contains('District').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Ed Level').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Sch Name').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Sch Type').scrollIntoView().should('be.visible')
    // 12. Save the Card
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click().wait(4000)
    //13. Open the Legends modal and enable demographics and schools
    cy.get('#legendIcon').click()
    cy.get('.layerTitle').contains('Public Schools').scrollIntoView().click()
    cy.get('.layerTitle').contains('Demographics').scrollIntoView().click()
    cy.wait(8000)
    // 14. Search a different address in the layer and verify the card list appears in the parcel.
    cy.searchAddress(address1)
    cy.waitForParcel()
    cy.get('.title').contains(headerTitle).scrollIntoView().should('be.visible').then(($header) => {
      var id = $header.attr('id');
      id = '#' + id.replace('CardLabel','');
      cy.get(id+ ' [data-surfedit="tableField_label"]').contains('District').should('be.visible')
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Ed Level').should('be.visible')
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Sch Name').should('be.visible')
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Sch Type').should('be.visible')
      cy.get(id + ' li[data-surfedit="card_template"]').should('have.length', '2')
    })
    //15. Open the Legends modal and disable demographics and schools layers.
    cy.get('#legendIcon').click()
    cy.get('.layerTitle').contains('Public Schools').scrollIntoView().click()
    cy.get('.layerTitle').contains('Demographics').scrollIntoView().click()
    cy.wait(8000)
  })
})
