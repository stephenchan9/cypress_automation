Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Load Contacts from spreadsheet', function() {
  before(function() {
    cy.visit('http://localhost:53633/lib/3.0/App/LandVision/CRE/CREST2.aspx?login=schan&account=automationcre',{ timeout:40000 }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Load Contacts: Import Contacts').click().wait(8000)
  })

  afterEach(function(){

  })

  it('CRE: Load contacts from spreadsheet after a search.', function(){
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Property Search').should('have.value','Property Search')
    cy.get('.filterItemText').contains('Characteristics').click()
    //Input the years in the box.
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type="text"]:nth-child(3)')
    .clear().type('2000')
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type="text"]:nth-child(5)')
    .clear().type('2018')
    //Last Market Sale 12 months
    cy.get('.filterItemText').contains('Last Market Sale').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(6) > div:nth-child(6) > input[type="radio"]:nth-child(9)').check()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(5000)
    cy.get('#filterFormDiv_filterSearchSelectContainer').should('not.be.visible').wait(10000)

    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(20000)
      cy.get('#PropertySearch_0_tab').should('be.visible').then(($tabCount) => {
        expect($tabCount.text()).to.include(count)
      })
    })
    //Get the results and save to USER folder.
    cy.get('[title="Save Results List"]').click()
    cy.get('#mruDropDown_5').select('USER', {force: true})
    cy.get('.mruFileInput').clear().type('SavedResultsTest')
    cy.get('#gridSaveAsOK').click().wait(5000)
    //Verify the title was saved accurately.
    cy.get('#PropertySearch_0_tab').should('be.visible').then(($tab_title) => {
      expect($tab_title.text()).to.have.string('SavedResultsTest')
    })
    //Upload the spreadsheet with contacts
    cy.get('#legendIcon').click()
    cy.get('input[value="More Layers"]').click()
    cy.get('#mruDropDown_1').select('USER', {force:true})
    cy.get('#legend_browseLayers_dataloaderSelect').select('Spreadsheet', {force:true})
    cy.upload_file('.upload_file_box', 'TestAutomation.csv');
    var randomstring = require("randomstring");
    var layerTitle = randomstring.generate(7)
    cy.get('#createLayerLayerName').type(layerTitle)
    cy.get('#createLayerCreate').click()
    cy.get('#legend_browseLayers_okButton').click()
    cy.wait(3000) // Give time for the layer to appear in list.
    //Verify the Layer was loaded sucessfully.
    cy.get('[title="Table Loader Job"]').should('be.visible')
    //Delete the Uploaded Layer.
    cy.get('input[value="More Layers"]').click()
    cy.get('#legend_browseLayers_availableList').select(layerTitle)
    cy.get('.ui-button-text').contains('Delete Layer').click()
    cy.wait(2000)
    cy.get('option').contains(layerTitle).should('not.be.visible')
    cy.get('#legend_browseLayers_okButton').click()
    cy.get('#legendIcon').click()
  })
  it('CRE: Load contacts from spreadsheet and add layer to list.', function(){
  })
})

describe('My Site', function() {
  before(function() {
    cy.visit('http://localhost:53633/lib/3.0/App/LandVision/CRE/CREST2.aspx?login=schan&account=automationcre',{ timeout:40000 }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Load Contacts: Import Contacts').click().wait(8000)
  })

  afterEach(function(){
    if(this.currentTest.state === 'failed') {
      Cypress.runner.stop()
    }
    else{
      cy.wait(3000)
      //Delete the Newly Created MySite.
      cy.get('.title').contains('MySites').click({force:true})
      cy.get('.command_name').contains('View MySite').first().click()
      cy.get('#FeaturePanel .SE_unedit > .command_name').click()
      cy.get('.command_name').contains('Delete').click()
    }
  })

  it('Create a MySite', function(){
    var address = '15 CHESTER IRVINE, CA 92603';
    //Enter a Address to search
    cy.searchAddress(address)
    //---Wait for the parcel to load. Account is crew so can't use wait for parcel.
    cy.wait(30000)
    //Click command create MySite
    cy.get('.SE_unedit > .command_name').click()
    cy.get('.command_name').contains('Create MySite').click()
    //Input information into MySite Form.
    var random_str = require("randomstring");
    cy.get('.editAttributeLabel').contains('Name').next().clear().type(random_str.generate(6)) //Name
    cy.get('.editAttributeLabel').contains('Location Details').next().clear().type(random_str.generate(30)) //Location Details
    cy.get('.editAttributeLabel').contains('Selling Status').next().select('Listed')
    //Characteristics
    cy.get('.editAttributeLabel').contains('Acreage').next().clear().type('.3') //Acreage
    cy.get('.editAttributeLabel').contains('Lot Square Footage').next().clear().type('5000') //Lot Square Footage
    cy.get('.editAttributeLabel').contains('Land Use').next().clear().type('Vacant') //Location Details
    cy.get('.editAttributeLabel').contains('Zoning').next().clear().type(random_str.generate(15)) //Location Details
    //Condition
    cy.get('.groupId_Condition .editAttributeLabel').contains('Status').next().select('Entitled')
    cy.get('.editAttributeLabel').contains('Entitlement Details').next().clear().type(random_str.generate(15))
    cy.get('.editAttributeLabel').contains('Site Condition').next().clear().type(random_str.generate(20))
    //Visibility
    cy.get('.editAttributeLabel').contains('Road Frontage').next().clear().type(random_str.generate(20))
    //Add Road Frontage functionality
    //cy.get('ui-button-text').contains('Add Road Frontage').click()
    cy.get('.editAttributeLabel').contains('Traffic Count').next().clear().type(random_str.generate(20))
    //Add Traffic Count functionality
    //cy.get('ui-button-text').contains('Add Traffic Count').click()
    //Additional Info
    cy.get('.editAttributeLabel').contains('Web Link').next().clear().type(random_str.generate(20))
    //Add Web Link functionality
    //cy.get('ui-button-text').contains('Add Web Link').click()
    //My Contacts
    cy.get('[style="position: relative; display: inline-block;"] > .ui-button > .ui-button-icon-primary').click().then(($newContact) => {
      cy.wait(3000)
      cy.get('.ui-menu-item > .ui-corner-all').contains('New...').trigger('mouseover')
      cy.get('.ui-menu-item > .ui-corner-all').contains('New...').click()
      cy.get('.groupId_Contact_Information .editAttributeLabel').contains('First Name').next().clear().type(random_str.generate(6)) //First Name
      cy.get('.groupId_Contact_Information .editAttributeLabel').contains('Last Name').next().clear().type(random_str.generate(6)) //Last Name
      cy.get('.editAttributeLabel').contains('Cell Number').next().clear().type('847239498') //Name
      cy.get('.ui-button-text').contains('Save').click() //Click Save
    })
    //My Notes
    cy.get('.editAttributeLabel').contains('Notes').next().clear().type(random_str.generate(30))
    //Add Notes functionality
    //cy.get('ui-button-text').contains('Add Notes').click()
    cy.get('.editAttributeSubmit').click() //Click OK button

  })

})
