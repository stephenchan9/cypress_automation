Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})
describe('Create Site Profile Report', function(){
  before(function(){
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)
  })

  afterEach(function(){
    if (this.currentTest.state === 'failed') { Cypress.runner.stop() }
    //cy.get('#timeViewToolbar_CloseIcon').click()
  })

  it('Click Time View and select a different View.', function(){
    var address = '179 FIELDWOOD, IRVINE, CA 92618'
    cy.searchAddress(address)
    //---Wait for the parcel to load
    cy.waitForParcel()
    cy.get('#timeViewMenu').click()
    cy.get('#timeViewToolbar').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('[name="timeview"]').then(($btns) => {
      var count = $btns.length -1;
      var rand = Math.floor((Math.random() * count)+1); //Select a rabdom number between 1 and the count
      if (rand == 1){ //Condition so "Default" is not selected.
        rand += 1;
      }
      var selector = ':eq(' + rand + ')'
      cy.get('[name="timeview"]' + selector).check()
      //Verify the selected Date matches the Date on the bottom of modal.
      cy.get('[name="timeview"]' + selector).next().then(($label) => {
        var text = $label.text().replace(/[^0-9-]/g, '') //Removes extra characters to match.
        cy.get('#timeViewToolbar_Label').should('have.text', text)
      })
    })
  })

  it('Click Time View and Refresh the List after panning', function(){
    var address = '345 LANG AVE, LA PUENTE, CA 91744'
    cy.searchAddress(address)
    //---Wait for the parcel to load
    cy.waitForParcel()
    cy.get('#timeViewMenu').click()
    cy.get('#timeViewToolbar').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('[name="timeview"]').then(($btns) => {
      var count = $btns.length - 1;
      cy.log(count)
      var rand = Math.floor((Math.random() * count)+1); //Select a rabdom number between 1 and the count
      if (rand == 1){ //Condition so "Default" is not selected.
        rand += 1;
      }
      var selector = ':eq(' + rand + ')'
      cy.get('[name="timeview"]' + selector).check()
      //Verify the selected Date matches the Date on the bottom of modal.
      cy.get('[name="timeview"]' + selector).next().then(($label) => {
        var text = $label.text().replace(/[^0-9-]/g, '') //Removes extra characters to match.
        cy.get('#timeViewToolbar_Label').should('have.text', text)
      })
    })
    //Pan the Map
    var x_coord= -117.937
    var y_coord =33.93398
    var zoom =15
    cy.panMap(x_coord, y_coord, zoom)
    //Time View toolbar remains open.
    cy.get('#timeViewToolbar').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    //Refresh the List with new dates.
    cy.get('#timeViewToolbar_Refresh').click()
  })
})
