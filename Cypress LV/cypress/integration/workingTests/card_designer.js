//Reset the Card Designer. Test Teardown.
var resetToDefault= function(address){
  //Enter a Parcel to search
  cy.searchAddress(address)
  ;
  cy.get('#summary').contains('More').click()
  //Open Card Designer
  cy.get('#summary').contains('Design Cards').click()
  cy.get('[data-surfedit=card_group]').should('be.visible')
  cy.get('#reset').click()
  cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > .ui-button > .ui-button-text').last().click({force:true})
  cy.wait(5000) //Wait for reset to defult notifications to dissappear.
}

describe('Card Designer Feature Tests', function() {
  //Define random data to use
  const randomLabel ='Val$%&***&()'
  const randomDesc ='&**&*&&*78821'
  const address = '11206 HARVARD DR'
  //Initial start of the test
  before(function(){
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(30000)
  })
  beforeEach(function(){
    resetToDefault(address);
  })
  afterEach(function(){
    if (this.currentTest.state === 'failed') {
      cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(35000)
    }
  })

  it('Add Field: Create a New Field', function() {
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Field Link
    cy.get('[id-surfedit="addTableField"]').first().click()
    cy.get('#addFieldForm').should('be.visible')
    //Select a Field and click Add then close the module.
    cy.get('#selectableFields').select('VALUE TRANSFER')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"]').within(($addFieldModal) => {
      cy.get('.ui-button-text').contains('Add').click()
      cy.get('span').contains('close').click()
    })
    //Verify the Field was added, then click to modify.
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('be.visible').click()
    cy.get('.command_name').contains('Modify').click()
    //Input special characters into the fields.
    //Label Input Box
    cy.get('.editAttributeLabel').contains('Label: ').next().then(($label) => {
      cy.wrap($label).clear().type(randomLabel)
    })
    //Description Input Box
    cy.get('.editAttributeLabel').contains('Description: ').next().then(($description) => {
      cy.wrap($description).clear().type(randomDesc)
    })
    //Select a Formatter
    //Formatter
    cy.get('.editAttributeLabel').contains('Formatter: ').next().then(($formatter) =>{
      cy.wrap($formatter).select('###,###')
    })
    //Click Ok button to save and verify label is in effect.
    cy.get('.editAttributeSubmit').contains('OK').click()
    cy.get('[data-surfedit=tableField_label]').contains(randomLabel)
    //Click Save to USER folder.
    cy.get('#saveAndClose').click().wait(5000)
    cy.get('#transformerSaveOk .ui-button-text').click().wait(5000)
  });
  it('Add Field: Create a New Field and change formatter. Delete created field afterwards.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(5000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Field Link
    cy.get('[id-surfedit="addTableField"]').first().click()
    cy.get('#addFieldForm').should('be.visible')
    //Select a Field and click Add then close the module
    cy.get('#selectableFields').select('VALUE TRANSFER')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"]').within(($addFieldModal) => {
      cy.get('.ui-button-text').contains('Add').click()
      cy.get('span').contains('close').click()
    })
    //Verify the Field was added and then click to modify
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('be.visible').click()
    cy.get('.command_name').contains('Modify').click()
    //Input special characters into the fields.
    //Label Input Box
    cy.get('.editAttributeLabel').contains('Label: ').next().then(($label) => {
      cy.wrap($label).clear().type(randomLabel)
    })
    //Description Input Box
    cy.get('.editAttributeLabel').contains('Description: ').next().then(($description) => {
      cy.wrap($description).clear().type(randomDesc)
    })
    //Select a Formatter
    cy.get('.editAttributeLabel').contains('Formatter: ').next().then(($formatter) =>{
      cy.wrap($formatter).select('###,###.00')
    })
    //Click the Ok button to save and verify the label is in effect
    cy.get('.editAttributeSubmit').contains('OK').click()
    cy.get('[data-surfedit=tableField_label]').contains(randomLabel)
    //Click Save to USER folder
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk .ui-button-text').click().wait(5000)
  });
  it('Add Command: Add a new command and save', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(5000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Manage Documents')
    cy.get('.editAttributeSubmit').click()
    cy.get('.command_name').contains('Manage Documents').scrollIntoView().should('be.visible')
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(5000)
    cy.get('#summary').contains('Manage Documents').should('be.visible')
  })
  it('Select the first card group and move the card up or down.', function(){
    const selectedField ='Year Built'
    //Enters a parcel to Search
    cy.searchAddress(address)

    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(5000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('#addList > .command_name').should('be.visible')
    //Move the fields up or down.
    cy.get('[id-surfedit=tableField_group1]').within(($cardGroup) =>{
      //Functions to move the field up/down
      var moveUp = function(id) {
        id = '#' + id;
        id = id.replace('_infoBox','_up')
        cy.get(id).click()
      };
      var moveDown = function(id) {
        id = '#' + id;
        id = id.replace('_infoBox','_down')
        cy.get(id).click()
      }
      cy.get('[data-surfedit=tableField_label]').contains(selectedField).then(($label_selected) => {
        var id_val = $label_selected.attr('data-dropdown')
        moveUp(id_val)
        cy.wait(3000)
        moveDown(id_val)
      })
    })
    cy.get('[title="Close"]').click({multiple:true})
  })
  it('Add Command: Create a new command (Copy Geometry) with all fields checked.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    cy.get('#summary').contains('More').click().wait(5000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Dmp.Toolkit.Commands.CopyGeometryCommand')
    cy.get('.editAttributeSubmit').click()
    var clickCommand = function(s){
      cy.get('#summary').contains(s).click()
    };
    //Create a new name for the command and add settings.
    var randomstring = "Copy Geometry " + Math.random().toString(36).substring(7);
    cy.get('.ui-dialog-title').contains('Copy Geometry Command').should('be.visible').then(($copyGeometry) => {
      //Command Label name and checkboxes.
      cy.get('.editAttributeLabel').contains('Command Label').next().clear().type(randomstring)
      cy.get('.editAttributeLabel').contains('Copy Fields').prev().check()
      cy.get('.editAttributeLabel').contains('Hide Layer Picker').prev().check()
      cy.get('.editAttributeLabel').contains('Suppress Edit Form').prev().check()
      //Select the layer to save to.
      cy.get('.editAttributeInput').contains('Browse').last().click()
      cy.wait(5000)
      cy.get('#mruDropDown_8').select('DRAWINGS',{force:true})
      cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
        var layerSelected = "Markup";
        var id = $layerModal.attr('id');
        id = id.replace('ui-dialog-title-', '')
        var list_selection_id = '#' + id + '_availableList'
        var ok_button = '#' + id + '_okButton'
        cy.get(list_selection_id).select(layerSelected).first()
        cy.get(ok_button).click()
      })
      //Submit
      cy.get('.editAttributeSubmit').click()
    })
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    cy.get('#summary').contains('More').click().wait(5000)
    //Click the newly created command.
    cy.get('#summary').contains(randomstring).click()
    cy.get('.editAttributeInput').contains('Browse').click()
    cy.wait(3000)
    cy.get('#mruDropDown_8').select('SHARE',{force:true})
    //Select the Layer
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Project Sites";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    cy.get('.editAttributeSubmit').contains('OK').click()
    cy.server()
    cy.route('POST','/Transaction.aspx').as('getInfo')
    //Verify the Command works.
    cy.get('#summary').contains(randomstring).click()
    //---Wait for the Transaction Request to appear.
    cy.wait('@getInfo').then((xhr) => {
      cy.log('Success')
    })
  })
  it('Add Command: Create a new command (Copy Geometry) with all fields except "Suppress Edit Form" checked.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    cy.get('#summary').contains('More').click().wait(5000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Dmp.Toolkit.Commands.CopyGeometryCommand')
    cy.get('.editAttributeSubmit').click()
    var clickCommand = function(s){
      cy.get('#summary').contains(s).click()
    };
    //Create a new name for the command and add settings.
    var randomstring = "Copy Geometry " + Math.random().toString(36).substring(7);
    cy.get('.ui-dialog-title').contains('Copy Geometry Command').should('be.visible').then(($copyGeometry) => {
      //Command Label name and checkboxes.
      cy.get('.editAttributeLabel').contains('Command Label').next().clear().type(randomstring)
      cy.get('.editAttributeLabel').contains('Copy Fields').prev().check()
      cy.get('.editAttributeLabel').contains('Hide Layer Picker').prev().check()
      //Select the layer to save to.
      cy.get('.editAttributeInput').contains('Browse').last().click()
      cy.wait(5000)
      cy.get('#mruDropDown_8').select('DRAWINGS',{force:true})
      cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
        var layerSelected = "Markup";
        var id = $layerModal.attr('id');
        id = id.replace('ui-dialog-title-', '')
        var list_selection_id = '#' + id + '_availableList'
        var ok_button = '#' + id + '_okButton'
        cy.get(list_selection_id).select(layerSelected)
        cy.get(ok_button).click()
      })
      //Submit
      cy.get('.editAttributeSubmit').click()
    })
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    cy.get('#summary').contains('More').click().wait(5000)
    //Click the newly created command.
    cy.get('#summary').contains(randomstring).click()
    cy.get('.editAttributeInput').contains('Browse').click()
    cy.wait(3000)
    cy.get('#mruDropDown_8').select('SHARE',{force:true})
    //Select the Layer
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Project Sites";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    cy.get('.editAttributeSubmit').contains('OK').click()
    //Verify the Command works.
    cy.get('#summary').contains(randomstring).click()
    cy.wait(3000)
    //Close the Copy Geometry Dialog.
    cy.get('[value="OK"]').last().click({force:true})
    cy.wait(4000)
    cy.get('[value="OK"]').last().click({force:true})
    //Close the parcel detail.
    cy.get('.js-close_card').click()
  })
  it('Add Command: Create a new command (Copy Geometry) with no fields checked.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    cy.get('#summary').contains('More').click().wait(5000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Dmp.Toolkit.Commands.CopyGeometryCommand')
    cy.get('.editAttributeSubmit').click()
    var clickCommand = function(s){
      cy.get('#summary').contains(s).click()
    };
    //Create a new name for the command and add settings.
    var randomstring = "Copy Geometry " + Math.random().toString(36).substring(7);
    cy.get('.ui-dialog-title').contains('Copy Geometry Command').should('be.visible').then(($copyGeometry) => {
      cy.get('.editAttributeLabel').contains('Command Label').next().clear().type(randomstring)
      //Submit
      cy.get('.editAttributeSubmit').click()
    })
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    cy.get('#summary').contains('More').click().wait(5000)
    //Click the newly created command.
    cy.get('#summary').contains(randomstring).click()
    cy.get('.editAttributeInput').contains('Browse').click()
    cy.wait(3000)
    //Select the Layer
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Project Sites";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList';
      var folder_selection = '#' + id + ' .mruDropdown';
      var ok_button = '#' + id + '_okButton'
      cy.get(folder_selection).select('SHARE', {force:true})
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    //Verify the Command works.
    cy.get('.editAttributeSubmit').contains('OK').click()
    cy.wait(2000)
    //Close the Copy Geometry Dialog.
    cy.get('[value="OK"]').last().click({force:true})
    cy.wait(4000)
    //Close the parcel detail.
    cy.get('.js-close_card').click()

  })
  it('Linking Layer: Conditional Data: Create conditional test for hazard data.', function(){
    var address = '3535 SPORTS ARENA BLVD, SAN DIEGO, CA 92110';

    cy.searchAddress(address)

    cy.get('.command_name').contains('More').click()
    cy.get('.command_name').contains('Design Cards').click()
    //Add a field to the hazards card.
    //Create a Hazards2 card
    cy.wait(4000)
    cy.get('#addCard').click()
    cy.get('#cardLabel').clear().type('Hazard2')
    cy.get('[aria-labelledby="ui-dialog-title-addCardForm"] .ui-button-text').contains('OK').click()
    cy.get('[data-surfedit="card_item"]').contains('Hazard2').scrollIntoView().should('be.visible')
    //Add Field
    cy.get('[data-surfedit="card_item"] [id-surfedit="addTableField"]').last().click()
    //Select a layer "SS.HAZARDS.HAZARDSMETADATA"
    cy.get('#ui-dialog-title-addFieldForm').should('be.visible')
    cy.get('#selectLink').select('New Layer Relationship').wait(3000)
    //Select a layer dialog.
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      cy.get('#mruText_8').clear().type('SS.HAZARDS.HAZARDSMETADATA{enter}')
      var layerSelected = "MetadataJUP";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    //Layer Relationship Modal: HazardsMetadata
    cy.get('.ui-dialog').contains('Layer Relationship').then(($layerRelationship) => {
      var relationshipName = 'HazardsMetadata';
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(5000)
      //Verify layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })
    //Select the "Type" field
    cy.get('#fieldList #selectableFields0').select('Type')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"] .ui-button-text').contains('Select').click()
    cy.wait(3000)
    //Create another Layer Relationship
    //Select a layer "SS.HAZARDS.DAMINUNDATIONZONES"
    cy.get('#ui-dialog-title-addFieldForm').should('be.visible')
    cy.get('#selectLink').select('New Layer Relationship').wait(3000)
    //Select a layer dialog.
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      cy.get('#mruText_8').clear().type('SS.HAZARDS.DAMINUNDATIONZONES{enter}')
      cy.get('#mruText_8').type('{enter}')
      var layerSelected = "DamInundationZonesJUP";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    //Layer Relationship Modal: DamInundation
    cy.get('.ui-dialog-title').then(($layerRelationship) => {
      var relationshipName = 'DamInundation';
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(5000)
      //Verify layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })

    //Add Custom Script Field and Select "Zone" field
    cy.get('#selectableFields').select('CUSTOM SCRIPT')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"] .ui-button-text').contains('Add').click()
    cy.get('#fieldList #selectableFields0').select('Zone')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"] .ui-button-text').contains('Select').click()
    cy.wait(3000)
    //Close the dialog and modify the CUSTOM SCRIPT field.
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"] .ui-dialog-titlebar-close').click()
    cy.get('[data-surfedit="tableField_label"]').contains('CUSTOM_SCRIPT').last().click()
    cy.get('.command_name').contains('Modify').click()
    //Modify the field
    cy.get('.editAttributeLabel').contains('Label: ').next().clear().type('Dam Inundation Zones').then(() => {
      var str = `return (function() {
        if (record.toJSON() && record.toJSON().DamInundation && record.toJSON().DamInundation.length && record.toJSON().DamInundation.length > 0) {
          return record.toJSON().DamInundation[0].record.ZONE;
        } else if (record.toJSON().HazardsMetadata) {
          var HazardsMetadata = record.toJSON().HazardsMetadata;
          var output = "";
          for (var i = 0; i < HazardsMetadata.length; i++) {
            var HazardsMetadataLoop = HazardsMetadata[i].record;
            if (HazardsMetadataLoop.TYPE && HazardsMetadataLoop.TYPE == "DamInundationZones") {
              return "Not in hazard";
            }
          }
          return "Data not available";
        } else {
          return "Data not available";
        }
      })();`;
      cy.get('.editAttributeLabel').contains('Custom Formatter: ').next().clear().type(str)
      var hzd = '[data-value=' + 'HazardsMetadata]';
      var dam = '[data-value=' + 'DamInundation]';
      cy.get(hzd).check()
      cy.get(dam).check()
    })
    cy.get('.editAttributeSubmit').contains('OK').click()
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()

    //verify the hazard info is correct.
    cy.searchAddress(address)

    cy.get('#summary .title').contains('Hazard2').scrollIntoView()
    cy.get('[data-surfedit="tableField_label"]').contains('Dam Inundation Zones').should('be.visible')
    cy.wait(3000)
    cy.get('.js-data').last().should('contain', 'IN')
  })
})

describe('Misc Card Designer tests', function(){
  const randomLabel ='Val$%&***&()'
  const randomDesc ='&**&*&&*78821'
  const address = '11206 HARVARD DR'
  //Initial start of the test
  before(function(){
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(30000)
  })
  afterEach(function(){
    if (this.currentTest.state === 'failed') {
      cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(35000)
    }
  })

  it('Create Card: Create a Card with Table Template', function() {
    resetToDefault(address);
    cy.searchAddress(address)

    //Open More command
    cy.get('#summary').contains('More').click()
    //Click Design Card command.
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(8000)
    cy.get('#addCard > .command_name').should('be.visible')
    //Add card and input data.
    cy.get('.command_name').contains('Add Card').click()
    cy.get('#addCard').should('be.visible')
    var randStr = Math.random().toString(36).substring(7);
    cy.get('#cardLabel').clear().type(randStr)
    cy.get('#selectCardTemplate').select('Table').should('have.value', 'Table')
    cy.get('[aria-labelledby="ui-dialog-title-addCardForm"] .ui-button-text').contains('OK').click()
    cy.wait(4000)
    cy.get('[data-surfedit="card_header"]').last().then(() => {
      cy.get('h6').contains(randStr)
    })
    //Delete newly created card.
    cy.get('#summary section .is-cardless').last().then(() => {
      cy.get('#summary section .is-cardless .fa-trash').last().click()
    })
    cy.get('.SE_unedit').contains('Delete').click()
    //Save the settings to USER Folder.
    cy.get('#saveAndClose').click()
    cy.wait(3000)
    cy.get('#transformerSaveOk .ui-button-text').click()
    cy.wait(5000)
  })
  it('Add Field: Add new fields and do not save changes. Verify the fields were NOT saved', function(){
    resetToDefault(address);
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Field Link
    cy.get('[id-surfedit="addTableField"]').first().click()
    cy.get('#addFieldForm').should('be.visible')
    //Select Fields and click Add then close the module.
    cy.get('#selectableFields').select('VALUE TRANSFER')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"]').within(() => {
      cy.get('.ui-button-text').contains('Add').click()
    })
    cy.get('#selectableFields').select('BUYER NAME')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"]').within(() => {
      cy.get('.ui-button-text').contains('Add').click()
    })
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"] .ui-icon-closethick').contains('close').click() //Close dialog box.
    //Verify the Field was added.
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('be.visible')
    cy.get('[data-surfedit=tableField_label]').contains('BUYER NAME').should('be.visible')
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Verify the fields were not saved.
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('not.be.visible')
    cy.get('[data-surfedit=tableField_label]').contains('BUYER NAME').should('not.be.visible')
  })
  it('Add Field: Create a New Field and modify the field. Close card designer and verify changes were NOT saved.', function(){
    resetToDefault(address);
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Field Link
    cy.get('[id-surfedit="addTableField"]').first().click()
    cy.get('#addFieldForm').should('be.visible')
    //Select Fields and click Add
    cy.get('#selectableFields').select('VALUE TRANSFER')
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"]').within(() => {
      cy.get('.ui-button-text').contains('Add').click()
    })
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"] .ui-icon-closethick').contains('close').click() //Close dialog box.
    //Verify the Field was added,then click to modify
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('be.visible').click()
    cy.get('.command_name').contains('Modify').click()
    //Input special characters into the fields.
    //Label Input Box
    cy.get('.editAttributeLabel').contains('Label: ').next().then(($label) => {
      cy.wrap($label).clear().type(randomLabel)
    })
    //Description Input Box
    cy.get('.editAttributeLabel').contains('Description: ').next().then(($description) => {
      cy.wrap($description).clear().type(randomDesc)
    })
    //Select a Formatter
    cy.get('.editAttributeLabel').contains('Formatter: ').next().then(($formatter) =>{
      cy.wrap($formatter).select('###,###.00')
    })
    //Click the Ok button to save and verify the label is in effect
    cy.get('.editAttributeSubmit').contains('OK').click()
    cy.get('[data-surfedit=tableField_label]').contains(randomLabel)
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Verify the fields were not saved.
    cy.get('[data-surfedit=tableField_label]').contains('VALUE TRANSFER').should('not.be.visible')
  })
  it('Delete Field: Delete a default field accidentally and close the card. Ensure changes were NOT saved.', function(){
    resetToDefault(address);
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Delete the field
    cy.get('[data-surfedit=tableField_label]').contains('Site Address').should('be.visible')
    var clickDelete = function(id){
      cy.get(id).click()
    };
    cy.get('[data-surfedit=tableField_label]').contains('Site Address').scrollIntoView().click().then(($field) => {
      var id_val = $field.attr('data-dropdown');
      id_val = '#' + id_val.replace('_infoBox', '_delete');
      clickDelete(id_val);
    })
    cy.get('[data-surfedit=tableField_label]').contains('Site Address').should('not.be.visible')
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('[data-surfedit=tableField_label]').contains('Site Address').should('be.visible')
  })
  it('Add Command: Close card designer and verify changes were NOT saved.', function(){
    resetToDefault(address);
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(5000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Manage Documents')
    cy.get('.editAttributeSubmit').click()
    cy.get('.command_name').contains('Manage Documents').scrollIntoView().should('be.visible')
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('[data-surfedit=tableField_label]').contains('Manage Documents').should('not.be.visible')
  })
  it('Add Command: Add a new command and modify the command. Close the card and ensure changes were NOT saved.', function(){
    resetToDefault(address);
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(5000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add Command
    cy.get('.js-hide-section [id-surfedit="addCommand"]').scrollIntoView().click()
    cy.get('.editAttributeLabel').contains('Choose a command').next().select('Manage Documents')
    cy.get('.editAttributeSubmit').click()
    cy.get('.command_name').contains('Manage Documents').scrollIntoView().should('be.visible').click()
    cy.get('.command_name').contains('Modify').click({force:true}).then(() => {
      cy.get('.editAttributeLabel').contains('Label').next().clear().type('Test')
      cy.get('.editAttributeLabel').contains('Description').next().clear().type('Testing description area.')
      cy.get('.editAttributeSubmit').click()
    })
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('[data-surfedit=tableField_label]').contains('Manage Documents').should('not.be.visible')
  })
  it('Delete Command: Delete a default command field accidentally and close the card. Ensure changes were NOT saved.', function(){
    resetToDefault(address);
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(5000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Delete default command
    var clickDelete = function(id){
      cy.get(id).click()
    };
    cy.get('[data-surfedit="command_item"]').contains('Site Profile Report').scrollIntoView().click().then(($command) => {
      var id_val = $command.attr('data-dropdown');
      id_val = '#' + id_val.replace('_infoBox', '_delete');
      clickDelete(id_val);
    })
    cy.get('.command_name').contains('Site Profile Report').should('not.be.visible')
    cy.get('#SEContainer .js-close_card').click() //Close the card
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.wait(5000)
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('.command_name').contains('Site Profile Report').scrollIntoView().should('be.visible')
  })
  it('Help: Verify help icon displays Card Designer info', function(){
    resetToDefault(address);
    //Enter a Parcel to search
    cy.searchAddress(address)
    //Wait for XHR
    
    //Open the More dropdown
    cy.get('#summary').contains('More').click()
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('[title="Card Designer Help"]').click()
    cy.get('[aria-labelledby="ui-dialog-title-helpDialog"]').should('be.visible')
    cy.get('th').contains('Card Designer').should('be.visible')
    //Close the Help Dialog
    cy.get('[aria-labelledby="ui-dialog-title-helpDialog"] .ui-dialog-titlebar-close').click()
    cy.get('.js-close_card').last().click()
  })
})

describe('Card Designer Layer Relationship Tests', function(){
  //Define random data to use
  const randomLabel ='Val$%&***&()'
  const randomDesc ='&**&*&&*78821'
  const address = '11206 HARVARD DR'
  //Initial start of the test
  before(function(){
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(30000)
  })
  beforeEach(function(){
    resetToDefault(address);
  })
  afterEach(function(){
    if (this.currentTest.state === 'failed') {
      cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(35000)
    }
  })

  it('Layer Relationship: Create a Layer Relationship from a layer.', function(){
    //Enters a parcel to Search
    cy.searchAddress(address)
    //Wait for XHR

    //Open the More Dropdown
    cy.get('#summary').contains('More').click().wait(5000)
    //Open Card Designer
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    //Click Add List Command
    cy.get('#addList').first().click()
    cy.get('#ui-dialog-title-addListForm').should('be.visible')
    cy.get('#selectLink').select('New Layer Relationship').wait(3000)
    //Select a layer dialog.
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Manholes";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    //Layer Relationship Modal
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"]').then(($layerRelationship) => {
      var relationshipName = Math.random().toString(36).substring(7);
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(5000)
      //Verify layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })
    //Input Header Name
    var headerTitle = Math.random().toString(36).substring(7);
    cy.get('#cardLabel').type(headerTitle)
    //Click Ok in the Add List Modal
    cy.wait(3000)
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"] button').contains('OK').click()
    //Verify the List was created
    cy.wait(3000)
    cy.get('[data-surfedit="card_group"] .card-section .title').contains(headerTitle).should('exist')
    cy.get('.js-close_card').last().click({force:true})
  })
  //Gives an error ue to current bug with format.
  it('Layer Relationship: Create a Layer Relationship in user loaded layer(Manholes) and add datatype fields. Modify the fields and verify the format of the fields are correct for a parcel in the layer', function(){
    // 1. Enter a parcel to search in user loaded layer.
    var address1 = '6385 WINSTON TRCE, MCDONOUGH, GA 30252';
    var address2 = '5055 YELLOW PINE DR, MCDONOUGH, GA 30252';
    cy.searchAddress(address1)
    // Wait for XHR

    // 2. Open Card Designer and click Add List command.
    cy.get('#summary').contains('More').click().wait(5000)
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('#addList').first().click()
    cy.get('#ui-dialog-title-addListForm').should('be.visible')
    // 3. Create a new layer relationship and select the Manholes layer.
    cy.get('#selectLink').select('New Layer Relationship').wait(3000)
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Manholes";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    // 4. Create the Layer Relationship by selecting and inputting different fields.
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"]').then(($layerRelationship) => {
      var relationshipName = Math.random().toString(36).substring(7);
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      cy.get('#Buffer').type('500')
      cy.get('#Unit').select('Feet')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(4000)
      //Verify layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })
    // 5. Input the header name on the add to list section.
    var headerTitle = Math.random().toString(36).substring(7);
    cy.get('#cardLabel').type(headerTitle)
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"] button').contains('OK').click()
    // 6. Verify the list was created in card designer.
    cy.wait(3000)
    cy.get('[data-surfedit="card_group"] .card-section .title').contains(headerTitle).should('exist')
    // 7. Click add field on the list and add datatypes.
    cy.get('[id-surfedit="addTableField"] a').last().click()
    cy.get('#addFieldForm').should('be.visible')
    //Date
    cy.get('#selectableFields').select('Gnss Heigh')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Integer
    cy.get('#selectableFields').select('Gps Week')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Text
    cy.get('#selectableFields').select('Gps Second')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    // Time
    cy.get('#selectableFields').select('Gps Date')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    // Time
    cy.get('#selectableFields').select('Last Updat')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    cy.get('.new_form > .ui-dialog-titlebar > .ui-dialog-titlebar-close').contains('close').click()
    // 8. Verify the fields were added to the list.
    cy.get('[data-surfedit="tableField_label"]').contains('Gnss Heigh').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Week').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Second').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Date').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Last Updat').scrollIntoView().should('be.visible')
    // 9. Modify the fields
    //Gnss Heigh
    cy.get('[data-surfedit="tableField_label"]').contains('Gnss Heigh').last().click().then(($gnssHeigh) => {
      var id = $gnssHeigh.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('###,###.00',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Gps Week
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Week').last().click().then(($gpsWeek) => {
      var id = $gpsWeek.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('###,###',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Gps Second
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Second').last().click().then(($gpsSecond) => {
      var id = $gpsSecond.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('$###,###',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Gps Date
    cy.get('[data-surfedit="tableField_label"]').contains('Gps Date').last().click().then(($gpsDate) => {
      var id = $gpsDate.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('M/DD/YY',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Last Updat
    cy.get('[data-surfedit="tableField_label"]').contains('Last Updat').last().click().then(($lastUpdate) => {
      var id = $lastUpdate.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('M/DD/YY HH:mm a',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    // 10. Save the Card
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    // 11. Search a different address in the layer and verify the datatypes are correct.
    cy.searchAddress(address2)
    cy.wait(6000)
    // Wait for XHR

    // 12. Verify the header with dataypes are correct using regex
    cy.get('.title').contains(headerTitle).scrollIntoView().should('be.visible').then(($header) => {
      var id = $header.attr('id');
      id = '#' + id.replace('CardLabel','');
      //Verify 1st Field (Gps Heigh)/ ###,###.00
      cy.get(id+ ' [data-surfedit="tableField_label"]').contains('Gnss Heigh').next().then(($gnssHeigh) => {
        var v ="" + $gnssHeigh.text();
        expect(v).to.match(/(^\d{3})\,(\d{3})\.(\d{2}$)/)
      })
      //Verify 2nd Field (GPS Week)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Gps Week').next().then(($gpsWeek) => {
        var v = "" + $gpsWeek.text();
        expect(v).to.match(/(^\d{3})\,(\d{3}$)/)
      })
      //Verify 3rd field (GPS Second)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Gps Second').next().then(($gpsSecond) => {
        var v ="" + $gpsSecond.text();
        expect(v).to.match(/(^\$)(\d{3})\,(\d{3}$)/)
      })
      //verify the 4th field (GPS Date)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Gps Date').next().then(($gpsDate) => {
        var v ="" + $gpsDate.text();
        expect(v).to.match(/(^\d{1,2})\/(\d{1,2})\/(\d{1,2}$)/)
      })
      //verify the 5th field (GPS Date)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Last Updat').next().then(($lastUpdate) => {
        var v ="" + $lastUpdate.text();
        expect(v).to.match(/(^\d{1,2})\/(\d{1,2})\/(\d{1,2})\s(\d{1,2})\:(\d{1,2})\s([a|p])([m]$)/)
      })
    })
    cy.get('.js-close_card').last().click({force:true})
  })
  //Gives an error ue to current bug with format. The selected format is not appearing due to the shapefile not having correct format.
  it('Layer Relationship: Create a Layer Relationship in user loaded layer(Manholes) and add different datatype fields. Modify the fields and verify the format of the fields are correct for a parcel in the layer', function(){
    // 1. Enter a parcel to search in user loaded layer.
    var address1 = '100 CITY PARK DR, MCDONOUGH, GA 30252';
    var address2 = '8190 CHAPMAN TER, MCDONOUGH, GA 30252';
    cy.searchAddress(address1)
    // Wait for XHR

    // 2. Open Card Designer and click Add List command.
    cy.get('#summary').contains('More').click().wait(5000)
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('#addList').first().click()
    cy.get('#ui-dialog-title-addListForm').should('be.visible')
    // 3. Create a new layer relationship and select the Manholes layer.
    cy.get('#selectLink').select('New Layer Relationship').wait(3000)
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      var layerSelected = "Manholes";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    // 4. Create the Layer Relationship by selecting and inputting different fields.
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"]').then(($layerRelationship) => {
      var relationshipName = Math.random().toString(36).substring(7);
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      cy.get('#Buffer').type('500')
      cy.get('#Unit').select('Feet')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(4000)
      //Verify layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })
    // 5. Input the header name on the add to list section.
    var headerTitle = Math.random().toString(36).substring(7);
    cy.get('#cardLabel').type(headerTitle)
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"] button').contains('OK').click()
    // 6. Verify the list was created in card designer.
    cy.wait(3000)
    cy.get('[data-surfedit="card_group"] .card-section .title').contains(headerTitle).should('exist')
    // 7. Click add field on the list and add datatypes.
    cy.get('[id-surfedit="addTableField"] a').last().click()
    cy.get('#addFieldForm').should('be.visible')
    //Date
    cy.get('#selectableFields').select('Latitude')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Integer
    cy.get('#selectableFields').select('Longitude')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Text
    cy.get('#selectableFields').select('Location')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Close the dialog.
    cy.get('[aria-labelledby="ui-dialog-title-addFieldForm"] .ui-dialog-titlebar-close').click()
    // 8. Verify the fields were added to the list.
    cy.get('[data-surfedit="tableField_label"]').contains('Latitude').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Longitude').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Location').scrollIntoView().should('be.visible')
    // 9. Modify the fields
    //Latitude
    cy.get('[data-surfedit="tableField_label"]').contains('Latitude').last().click({force:true}).then(($latitude) => {
      var id = $latitude.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('###,###.00')
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Longitude
    cy.get('[data-surfedit="tableField_label"]').contains('Longitude').last().click().then(($longitude) => {
      var id = $longitude.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('###,###',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    //Location
    cy.get('[data-surfedit="tableField_label"]').contains('Location').last().click().then(($location) => {
      var id = $location.attr('data-dropdown');
      id = '#' + id.replace('_infoBox', '');
      cy.get(id).click().then(() =>{
        cy.get(id+ '_modify').click({force:true})
        cy.wait(3000)
        cy.get('.editAttributeLabel').contains('Formatter: ').next().select('$###,###',{force:true})
        cy.get('.editAttributeSubmit').contains('OK').click()
      })
    })
    // 10. Save the Card
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    // 11. Search a different address in the layer and verify the datatypes are correct.
    cy.searchAddress(address2)
    // Wait for XHR

    // 12. Verify the header with dataypes are correct using regex
    cy.get('.title').contains(headerTitle).scrollIntoView().should('be.visible').then(($header) => {
      var id = $header.attr('id');
      id = '#' + id.replace('CardLabel','');
      //Verify 1st Field (Gps Heigh)/ ###,###.00
      cy.get(id+ ' [data-surfedit="tableField_label"]').contains('Latitude').next().then(($gnssHeigh) => {
        var v ="" + $gnssHeigh.text();
        expect(v).to.match(/(^\d{3})\,(\d{3})\.(\d{2}$)/)
      })
      //Verify 2nd Field (GPS Week)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Longitude').next().then(($gpsWeek) => {
        var v = "" + $gpsWeek.text();
        expect(v).to.match(/(^\d{3})\,(\d{3}$)/)
      })
      //Verify 3rd field (GPS Second)
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Location').next().then(($gpsSecond) => {
        var v ="" + $gpsSecond.text();
        expect(v).to.match(/(^\$)(\d{3})\,(\d{3}$)/)
      })
    })
    cy.get('.js-close_card').last().click({force:true})
  })
  it('Layer Relationship: Linking a layer relationship with multi layer relationship. ER: 2', function(){
    //Values pre determined.
    var address1 = '3591 CERRITOS AVE, LOS ALAMITOS, CA 90720';
    var number_of_cards = 2;
    var headerTitle = Math.random().toString(36).substring(7);
    cy.searchAddress(address1)
    // Wait for XHR

    // 2. Open Card Designer and click Add List command.
    cy.get('#summary').contains('More').click().wait(5000)
    cy.get('#summary').contains('Design Cards').click()
    cy.get('[data-surfedit=card_group]').should('be.visible')
    cy.get('#addList').first().click()
    cy.get('#ui-dialog-title-addListForm').should('be.visible')
    // 3. Create a new layer relationship with given filepath.
    cy.get('#selectLink').select('New Layer Relationship').wait(3000)
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      cy.get('#mruText_8').clear().type('ss.demog.easi{enter}')
      var layerSelected = "Demographics";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    // 4. Create the Layer Relationship by selecting and inputting different fields.
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"]').then(($layerRelationship) => {
      var relationshipName = Math.random().toString(36).substring(7);
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(4000)
      //Verify layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })
    //5. Create the Multi Layer Relationship
    cy.get('#selectLink').select('New Multi Layer Relationship').wait(3000)
    cy.get('.ui-dialog-title').contains('Choose Layer').then(($layerModal) =>{
      cy.get('#mruText_8').clear().type('ss.admin.mp.publicschools{enter}')
      var layerSelected = "PublicSchools";
      var id = $layerModal.attr('id');
      id = id.replace('ui-dialog-title-', '')
      var list_selection_id = '#' + id + '_availableList'
      var ok_button = '#' + id + '_okButton'
      cy.get(list_selection_id).select(layerSelected)
      cy.get(ok_button).click()
    })
    // 6. Create the Multi Layer Relationship by selecting and inputting different fields.
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"]').then(($multLayerRelationship) => {
      var relationshipName = Math.random().toString(36).substring(7);
      cy.get('#linkName').type(relationshipName)
      cy.get('#linkType').select('Spatially')
      //Selector Issue. May fail.
      cy.get('body').then(($body) => {
        if ($body.find('div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').is('::visible')){
          cy.get('body > div:nth-child(123) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
        else{
          cy.get('body > div:nth-child(124) > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1) > span').click()
        }
      })
      cy.wait(4000)
      // 7.Verify Multi layer relationship
      cy.get('#selectLink').contains(relationshipName).should('exist')
    })
    // 8. Input the header name on the add to list section.
    cy.get('#cardLabel').type(headerTitle)
    cy.get('[aria-labelledby="ui-dialog-title-addListForm"] button').contains('OK').click()
    // 9. Verify the list was created in card designer.
    cy.wait(3000)
    cy.get('[data-surfedit="card_group"] .card-section .title').contains(headerTitle).should('exist')
    // 10. Click add field on the list and add datatypes.
    cy.get('[id-surfedit="addTableField"] a').last().click()
    cy.get('#addFieldForm').should('be.visible')
    //Date
    cy.get('#selectableFields').select('District')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Integer
    cy.get('#selectableFields').select('Ed Level')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    //Text
    cy.get('#selectableFields').select('Sch Name')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    // Time
    cy.get('#selectableFields').select('Sch Type')
    cy.get('.new_form > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text').contains('Add').click()
    cy.get('.new_form > .ui-dialog-titlebar > .ui-dialog-titlebar-close').contains('close').click()
    // 11. Verify the fields were added to the list.
    cy.get('[data-surfedit="tableField_label"]').contains('District').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Ed Level').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Sch Name').scrollIntoView().should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Sch Type').scrollIntoView().should('be.visible')
    // 12. Save the Card
    cy.get('#saveAndClose').click()
    cy.get('#transformerSaveOk').click()
    cy.wait(4000)
    // 13. Search a different address in the layer and verify the card list appears in the parcel.
    cy.searchAddress(address1)

    cy.get('.title').contains(headerTitle).scrollIntoView().should('be.visible').then(($header) => {
      var id = $header.attr('id');
      id = '#' + id.replace('CardLabel','');
      cy.get(id+ ' [data-surfedit="tableField_label"]').contains('District').should('be.visible')
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Ed Level').should('be.visible')
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Sch Name').should('be.visible')
      cy.get(id + ' [data-surfedit="tableField_label"]').contains('Sch Type').should('be.visible')
      cy.get(id + ' li[data-surfedit="card_template"]').should('have.length', '2')
    })
    cy.get('.js-close_card').last().click({force:true})
  })
})
