Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Search Bar/Locate Tests', function() {
  //Initial start of the test
  before(function() {
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(10000)
  });
  afterEach(function(){
    if (this.currentTest.state === 'failed') {
      cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(10000)
    }
  })

  it('Search by Address', function() {
    var address = '273 N CALAVERAS ST, FRESNO, CA'
    //Enter a Address to search
    cy.searchAddress(address)
    cy.wait(10000)
    cy.get('#identify_results_section').should('be.visible')
  });
  it('Search by APN', function() {
    var apn = '650-663-30';
    //Enter a APN to search
    cy.searchAddress(apn)
    cy.wait(10000)
    cy.get('#identify_results_section').should('be.visible')
  })
  it('Search by Lat/Long', function() {
    var coordinates = '36.11158, -115.17558';
    //Enter a APN to search
    cy.searchAddress(coordinates)
    cy.wait(10000)
    cy.get('#identify_results_section .title_group .title').contains(coordinates).should('be.visible')
  })
  it('Search by Street Intersection', function(){
    var intersection = 'Long Beach Blvd & E Pacific Coast Hwy, Long Beach, CA 90813'
    //Enter a Intersection to search
    cy.searchAddress(coordinates)
    cy.wait(10000)
    cy.get('#identify_results_section .title_group .title').contains(intersection).should('be.visible')
  })
})
