Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('POI search', function(){
  beforeEach(function(){
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)
  })
  afterEach(function(){
    if (this.currentTest.state === 'failed') {
      cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE',{ timeout:40000 }).wait(20000)
      cy.get('#bookmarkIcon').click()
      cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)
    }
  })
  it('Do a POI search with all major restaurants selected. Add to List. Press Print command in the results list. (8.5x11)', function(){
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Points of Interest Search').should('have.value','Points of Interest Search')
    cy.get('.filterItemText').contains('Major Brands').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > button:nth-child(1)').click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(8000)
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(20000) //Takes a long wait to add to list.
      cy.get('#gridLayerCommands [title="Options"]').click()
      cy.get('.balloon-items .insertText').contains('8.5 x 11 Print').should('be.visible').click()
    })
  })
  it('Do a POI search and create Trade Area', function(){
    //Search POI
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Points of Interest Search').should('have.value','Points of Interest Search')
    cy.get('.filterItemText').contains('Major Brands').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(6) > div > button:nth-child(1) > span')
    .click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(8000)
    cy.get('#searchInputBox').type('2457 PARK AVE, TUSTIN, CA 92782')
    cy.get('#searchButton').click()

    cy.wait(6000)
    cy.get('.js-close_card').click()
    //Add to List
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(20000) //Takes a long wait to add to list.
    })
    //Click Create Trade Area command in the results list.
    cy.get('#gridLayerCommands [title="Options"]').click()
    cy.get('.balloon-items .insertText').contains('Create Trade Area').should('be.visible').click().wait(4000)
    //Create a trade area with new Project Name.
    cy.get('.ui-dialog-title').contains('Trade Area Settings').then(($title) => {
      var unique_id = $title.attr('id');
      var random_str = Math.random().toString(36).substring(7); //Create a random string.
      unique_id = '#' + unique_id.replace('ui-dialog-title-', '');
      cy.get(unique_id + ' select').first().select('New', {force:true})
      cy.get('#newFolderInput').type(random_str)
      cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text').last().click()
      //Select Ring Type: Radius
      cy.get(unique_id).then(() => {
        var ring_type = unique_id.replace('root','RingType_Select')
        cy.log(ring_type)
        cy.get(ring_type).select('Radius')
      })
      //Select Ring Use: Candidate Sites
      cy.get(unique_id).then(() => {
        var ring_use = unique_id.replace('root','RingUse_Select')
        cy.log(ring_use)
        cy.get(ring_use).select('Candidate Sites')
      })
      //Select Max Ring Size: 15 minutes
      cy.get(unique_id).then(() => {
        var ring_size = unique_id.replace('root','RingSize_Select')
        cy.log(ring_size)
        cy.get(ring_size).select('3 Miles')
      })
      cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text').last().contains('OK').click()
      cy.wait(11000) //Wait for the layer to take into effect.
      cy.screenshot('tradeAreaOn')

      //Delete the Trade Area Sites/Rings
      cy.get('#legendIcon').click().wait(2000)
      cy.get('[value="More Layers"]').click()
      cy.get('#legend_browseLayers_activeList').select('Trade Area Sites')
      cy.get('#legend_browseLayers_removeButton').click()
      cy.get('#legend_browseLayers_activeList').select('Trade Area Rings')
      cy.get('#legend_browseLayers_removeButton').click()
      cy.wait(2000)
      cy.get('#legend_browseLayers_okButton').click()
      cy.get('#legend_legendCloseIcon').click()
    })
  })
})
