Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Draw Icon Toolbar', function() {
  before(function() {
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE', {
      timeout: 40000
    }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)
  })
  afterEach(function(){
    if (this.currentTest.state === 'failed') {
      cy.reload({timeout:40000}).wait(20000)
    }
    else{
      cy.get('#drawIcon').click()
      cy.get('.insertText').contains('Clear Drawings').click()
      cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text').contains('Yes').click()
    }
  })
  it('Check Target Layer is on by default.', function() {
    cy.get('#drawIcon').click()
    cy.get('.balloon-header').contains('Markup').should('be.visible')
    cy.get('#drawIcon').click()
  })
  it('Verify Turn Measure On/Off functionality works.', function() {
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Turn Measure On').should('be.visible').click()
    cy.get('.insertText').contains('Turn Measure Off').should('be.visible').click()
    cy.get('.insertText').contains('Turn Measure On').should('be.visible')
    cy.get('#drawIcon').click()
  })
  it('Draw Polygon', function() {
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Polygon').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    cy.wait(3000)
    cy.screenshot('before')
    //Draw the Polygon.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').click(1350, 650, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()
    //End draw
    cy.get('#geoStyler_accept').click().wait(2000)
    cy.wait(3000)
    cy.screenshot('after')

    cy.compareScreenshots('before' ,'after', 'drawIcon_toolbar.js', '.1')
  })
  it('Draw Polygon and Click Copy Geometry with no fields inputted.', function() {
    cy.server()
    cy.route('POST', '/Transaction.aspx').as('getInfo')
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Polygon').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Polygon.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').click(1350, 650, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()
    //End draw
    cy.get('#geoStyler_viewInfo').click()
    cy.get('.SE_unedit .command_name').contains('More').click()
    cy.get('.command_name').contains('Copy Geometry').click()
    cy.wait(5000)
    //Click Ok button
    cy.get('.editAttributeSubmit').last().click()
    //Copy Geometry Notes Modal
    cy.get('.editAttributeSubmit').last().click({
      force: true
    })
    //---Wait for the Transaction Request to appear.
    cy.wait('@getInfo').then((xhr) => {
      cy.log('Success')
    })
  })
  it('Draw Line Command', function() {
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Line').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Polygon.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()
    //End draw
    cy.get('#geoStyler_accept').click().wait(5000)
  })
  it('Draw Circle Command', function() {
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Circle').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Circle.
    cy.get('#labelCanvasId').click(800, 850, {
      force: true
    })
    //cy.get('#labelCanvasId').click(1050,750, {force:true})
    //cy.get('#labelCanvasId').dblclick()
    //End draw
    cy.get('#geoStyler_accept').click().wait(5000)
  })
  it.skip('Draw Rectangle Command:IN PROGRESS', function() {
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Rectangle').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Rectangle.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    //cy.get('#labelCanvasId').click(1050,750, {force:true})
    //End draw
    cy.get('#geoStyler_accept').click().wait(5000)
  })
})

describe('Other Tests', function() {
  before(function() {
    cy.visit('https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE', {
      timeout: 40000
    }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)
  })
  afterEach(function(){
    if (this.currentTest.state === 'failed') {
      cy.reload({timeout:40000}).wait(20000)
    }
  })
  it('Pan the map', function() {
    cy.window().then((win) => {
      expect(win.Dmp).to.exist;
      //var map = win.Dmp.Map.getMap();
      var center_of_map = win.Dmp.Map.getCenter(win.map);
      cy.log(center_of_map)
      //Lat y, Long x
      win.Dmp.Map.setView(win.map, {
        center: {
          x: -117.9375,
          y: 33.93398
        },
        zoom: 15
      });
      cy.wait(2000)
    })
  })
  //Need to find a different way.
  it.only('Access Tooltip for a parcel (Show Location Information):IN PROGRESS', function() {
    cy.get('#labelCanvasId').trigger('mouseover', {
      clientX: 33.42827,
      clientY: -84.13005
    })

    cy.get('#labelCanvasId').first().trigger('contextmenu').then(()=>{
      cy.wait(3000)
      //Click the menu item using JS
      cy.window().then((win)=>{
        win.document.querySelector('#GoogleEarthMenu [title="Show Location Information"]').click()
      })
    })
  })

})
