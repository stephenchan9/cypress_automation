describe('Heat Map: Results List', function() {
  //Initial start of the test
  beforeEach(function() {
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE', {
      timeout: 40000
    }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)
  });

  it('1.1: Heat Map: Turn on the heatmap for results after searching.', function() {
    //Perform a search.
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Points of Interest Search').should('have.value', 'Points of Interest Search')
    cy.get('.filterItemText').contains('Major Brands').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > button:nth-child(1)').click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(8000)
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(15000) //Takes a long wait to add to list.
      cy.get('#showHeatmap').should('be.visible').click() //Heat Map is turned on.
      cy.get('#DashboardDiv').screenshot('resultsList1.1/heatMapOn')
      cy.get('#hideHeatmap').should('be.visible').click() //Heat Map is turned off.
      cy.get('#DashboardDiv').screenshot('resultsList1.1/heatMapOff')
      cy.get('#gridLayerCommands [title="Options"]').click()
      cy.get('.balloon-items .insertText').contains('Remove List').should('be.visible').click()
    })
  })
  it('1.2: Heat Map + Labels: Search for results and turn on heatmap and Labels.', function() {
    //Perform a search.
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Points of Interest Search').should('have.value', 'Points of Interest Search')
    cy.get('.filterItemText').contains('Major Brands').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > button:nth-child(1)').click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(8000)
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(20000) //Takes a long wait to add to list.
      cy.get('#showHeatmap').should('be.visible').click() //Heat Map is turned on.
      cy.get('#hideLabels').should('be.visible') //Labels already on
      cy.get('#DashboardDiv').screenshot('resultsList1.2/heatAndLabelOn')
      cy.get('#hideHeatmap').should('be.visible').click() //Heat Map is turned off.
      cy.get('#DashboardDiv').screenshot('resultsList1.2/heatOffAndLabelOn')
      cy.get('#gridLayerCommands [title="Options"]').click()
      cy.get('.balloon-items .insertText').contains('Remove List').should('be.visible').click()
    })
  })
  it('1.3: Heat Map: Remove List and Create another. Verify heat map is still toggled.', function() {
    //Perform a search.
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Points of Interest Search').should('have.value', 'Points of Interest Search')
    cy.get('.filterItemText').contains('Major Brands').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > button:nth-child(1)').click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(8000)
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(15000) //Takes a long wait to add to list.
      cy.get('#showHeatmap').should('be.visible').click() //Heat Map is turned on.
      cy.get('#gridLayerCommands [title="Options"]').click()
      cy.get('.balloon-items .insertText').contains('Remove List').should('be.visible').click()
      cy.wait(3000)
    })
    //Perform another Search and verify heat map is still turned on.
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Fresno').click().wait(8000)
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Points of Interest Search').should('have.value', 'Points of Interest Search')
    cy.get('.filterItemText').contains('Major Brands').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > button:nth-child(1)').click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(8000)
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(15000) //Takes a long wait to add to list.
      cy.get('#hideHeatmap').should('be.visible') //Heat Map is still on.
      cy.get('#DashboardDiv').screenshot('resultsList1.3/heatStillOn')
      cy.get('#gridLayerCommands [title="Options"]').click()
      cy.get('.balloon-items .insertText').contains('Remove List').should('be.visible').click()
      cy.wait(3000)
    })

  })
  it('1.4: Heat Map: Edit heat map style with different values', function() {
    //Perform a search.
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Points of Interest Search').should('have.value', 'Points of Interest Search')
    cy.get('.filterItemText').contains('Major Brands').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > button:nth-child(1)').click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(8000)
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(15000) //Takes a long wait to add to list.
      cy.get('#showHeatmap').should('be.visible').click() //Heat Map is turned on.
      cy.get('#gridLayerCommands [title="Options"]').click()
    })
    cy.get('.balloon-items .insertText').contains('Edit Heatmap Style').should('be.visible').click()
    cy.wait(3000)
    cy.get('[aria-labelledby="ui-dialog-title-quickPicker2"]').should('be.visible').then(() => {
      //Intensity
      cy.get('#quickPicker2_intensityPicker_display .ui-slider').as('range')
        .invoke('attr', 'data-value', 255).invoke('attr', 'title', 255)
        .trigger('change').should('have.attr', 'data-value', '255')
      //Opacity
      cy.get('#quickPicker2_heatMapOpacityPicker').click()
      cy.get('#quickPicker2_heatMapOpacityPicker_menu [data-value="50"]').click()
      //size
      cy.get('#quickPicker2_heatPointSizePicker_display').click()
      cy.get('#quickPicker2_heatPointSizePicker_menu [data-value="60"]').click()
      //Close the styler
      cy.get('[aria-labelledby="ui-dialog-title-quickPicker2"] .ui-dialog-titlebar-close').click()
    })
    //Delete the List
    cy.get('#gridLayerCommands [title="Options"]').click()
    cy.get('.balloon-items .insertText').contains('Remove List').should('be.visible').click()
  })
  it('1.5: Heat Map Labels: Edit the label style with different values', function() {
    //Perform a search.
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Points of Interest Search').should('have.value', 'Points of Interest Search')
    cy.get('.filterItemText').contains('Major Brands').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > button:nth-child(1)').click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(8000)
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(15000) //Takes a long wait to add to list.
      cy.get('#showHeatmap').should('be.visible').click() //Heat Map is turned on.
      cy.get('#gridLayerCommands [title="Options"]').click()
    })
    cy.get('.balloon-items .insertText').contains('Edit Label Style').should('be.visible').click()
    cy.wait(3000)
    cy.get('[aria-labelledby="ui-dialog-title-quickPicker2"]').should('be.visible').then(() => {
      //Fill Color
      cy.get('#quickPicker2_colorPicker').click()
      cy.get('#quickPicker2_colorPicker_menu [style=" background-color:#33cccc"]').click() //Teal
      //Line Color
      cy.get('#quickPicker2_borderColorPicker').click()
      it()
      cy.get('#quickPicker2_borderColorPicker_menu [style=" background-color:#000000"]').click()
      //size
      cy.get('#quickPicker2_fontSizePicker').click()
      cy.get('#quickPicker2_fontSizePicker_menu [data-value="80"]').click()
      //Close the styler
      cy.get('[aria-labelledby="ui-dialog-title-quickPicker2"] .ui-dialog-titlebar-close').click()
    })
    //Delete the list
    cy.get('#gridLayerCommands [title="Options"]').click()
    cy.get('.balloon-items .insertText').contains('Remove List').should('be.visible').click()
  })
  it('1.6: Heat Map: Add More records and verify more heat maps are created', function() {
    //Perform a search.
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Points of Interest Search').should('have.value', 'Points of Interest Search')
    cy.get('.filterItemText').contains('Major Brands').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > button:nth-child(1)').click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(8000)
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(15000) //Takes a long wait to add to list.
      cy.get('#showHeatmap').should('be.visible').click() //Heat Map is turned on.
    })
    //Perform another Search and verify heat map is still turned on.
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Fresno').click().wait(8000)
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(15000) //Takes a long wait to add to list.
      cy.get('#hideHeatmap').should('be.visible') //Heat Map is still on.
      cy.get('[title="Hide Results List"]').click()
      cy.get('#DashboardDiv').screenshot('resultsList1.6/heatWithMore')
    })
  })
  //Getting 404 error.
  it('1.7: Heat Map: Upload a Browse Lists and create a heat map from results.', function() {
    cy.get('#resultsListIcon').click()
    cy.get('[title="Open Results List"]').click()
    cy.get('.insertText').contains('Browse Lists').click()
    cy.wait(4000)
    cy.get('select[class=browseTableSelect]').first().select('SavedResultsList')
    cy.get('#showHeatmap').should('be.visible').click()
    cy.get('.browseOK').first().click()
    cy.wait(15000)
    cy.get('#mygrid_container #SavedResultsList_0_tab').should('be.visible')
    cy.get('#hideHeatmap').should('be.visible') //Heat Map is already on
    cy.get('#DashboardDiv').screenshot('resultsList1.7/savedResultsHeatMapOn')
  })
  it('1.8: Heat Map: Toggle heat map for multiple lists.', function() {
    //Perform a search.
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Points of Interest Search').should('have.value', 'Points of Interest Search')
    cy.get('.filterItemText').contains('Major Brands').click()
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > button:nth-child(1)').click()
    cy.get('#filterFormDiv_filterFormContainer .ui-button-text').contains('OK').click().wait(8000)
    cy.get('#viewStatus_viewStatus_count').then(($countOfList) => {
      cy.wait(5000)
      const count = $countOfList.text()
      cy.expect(count).to.not.eq('0')
      cy.expect(count).to.not.eq(' --')
      cy.get('.viewStatusCommand').contains('Add To List').click().wait(15000) //Takes a long wait to add to list.
      cy.get('#showHeatmap').should('be.visible').click() //Heat Map is turned on.
    })
    //Load another list and switch between tabs.
    cy.get('[title="Open Results List"]').click()
    cy.get('.insertText').contains('Browse Lists').click()
    cy.wait(4000)
    cy.get('select[class=browseTableSelect]').first().select('SavedResultsList')
    cy.get('.browseOK').first().click()
    cy.wait(8000)
    cy.get('#mygrid_container #SavedResultsList_1_tab').should('be.visible')
    cy.get('#hideHeatmap').should('be.visible') //Heat Map is already on
    cy.get('#DashboardDiv').screenshot('resultsList1.8/multipleListsHeatMapOn1')
    //Switch to the previous tab and verify the heat map is still on.
    cy.get('[id="Pointsof Interest_0_tab"]').click().then(() => {
      cy.get('#hideHeatmap').should('be.visible') //Heat Map is already on
      cy.get('#DashboardDiv').screenshot('resultsList1.8/savedResultsHeatMapOn2')
    })
  })
})

describe('Heat Map: Layers', function() {
  //Initial start of the test
  beforeEach(function() {
    cy.visit('/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE', {
      timeout: 40000
    }).wait(20000)
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Irvine - Years Between').click().wait(8000)
  });

  it('1.9: Heat Map Parcels: Create heat map from layer', function() {
    cy.get('#legendIcon').click()
    cy.get('.layerTitle').contains('Parcels').then(($parcelsLayer) => {
      var data_id = $parcelsLayer.attr('data-layer');
      data_id = '[data-layer=' + data_id + '] [title="Options"]';
      cy.get(data_id).click()
      cy.wait(3000)
      cy.get('.insertText').contains('Create Heatmap').click({
        force: true
      }) //Turn on the heatmap for parcels.
    })
    //Verify the Heat map parcel was created
    cy.get('[layerid="UTF_Parcels_heatmap"]').should('be.visible')
    cy.get('#DashboardDiv').screenshot('heatMap1.9/on')
    cy.get('[layerid="UTF_Parcels_heatmap"] [title="Options"]').scrollIntoView().should('be.visible').last().click().then(($heatLayerOption) => {
      cy.get('.bt-content .insertText').contains('Remove Layer').click() //Remove Layer
      cy.get('#DashboardDiv').screenshot('heatMap1.9/off')
    })
    cy.get('#legend_legendCloseIcon').click()
  })
  it('1.10: Heat Map Parcels: Test all drop down options in Heat Map options after creating a heat map from layer.', function() {
    //Create the Heat Map First
    cy.get('#legendIcon').click()
    cy.get('.layerTitle').contains('Parcels').then(($parcelsLayer) => {
      var data_id = $parcelsLayer.attr('data-layer');
      data_id = '[data-layer=' + data_id + '] [title="Options"]';
      cy.get(data_id).click()
      cy.wait(3000)
      cy.get('.insertText').contains('Create Heatmap').click({
        force: true
      }) //Turn on the heatmap for parcels.
    })
    //Verify the Heat map parcel was created
    cy.wait(3000)
    //Start the Commands Test.
    cy.get('[layerid="UTF_Parcels_heatmap"] [title="Options"]').should('be.visible').scrollIntoView().should('be.visible').last().click().then(($heatLayerOption) => {
      cy.get('.bt-content .insertText').contains('Create New Filter').click() //Create New Filter
      cy.get('.ui-dialog-title').contains('New Filter').should('be.visible').then(() => {
        cy.get('.ui-dialog-titlebar-close .ui-icon-closethick').last().click() //Close dialog box.
      })
      cy.wrap($heatLayerOption).click() //Open the Options
      cy.get('.bt-content .insertText').contains('Edit Style').click() //Edit Style
      cy.get('#ui-dialog-title-quickPicker2').should('be.visible').then(() => {
        cy.get('[aria-labelledby="ui-dialog-title-quickPicker2"] .ui-dialog-titlebar-close').last().click()//CLose the dialog box
      })
      cy.wrap($heatLayerOption).click() //Open the Options
      cy.get('.bt-content .insertText').contains('Layer Properties').click() //Layer Properties.
      cy.get('.ui-dialog-title').contains('Layer Settings').should('be.visible').then(($id) => {
        cy.wrap($id).next().click() //Close the dialog box.
      })
      cy.wrap($heatLayerOption).click()
      cy.get('.bt-content .insertText').contains('Zoom To Bounds').click() //Zoom to Bounds
      cy.wait(3000)
      cy.wrap($heatLayerOption).click()
      cy.get('.bt-content .insertText').contains('Remove Layer').click() //Remove Layer
      cy.get('#DashboardDiv').screenshot('heatMap1.10/off')
    })
  })
})
