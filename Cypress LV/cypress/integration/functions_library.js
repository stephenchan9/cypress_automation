//Click on the Search filter
 var clickFilter = function(){
   cy.get('#filterIcon').click()
   cy.get('#filterArea').should('be.visible')
 };

//Click on the Remove List command in the Results List options
var removeList = function(){
   cy.get('#gridLayerCommands [title="Options"]').click()
   cy.get('.balloon-items .insertText').contains('Remove List').should('be.visible').click()
   cy.wait(3000)
};

//Click on the Edit Style command in the Results List options.
var editStyle = function(){
   cy.get('#gridLayerCommands [title="Options"]').click()
   cy.get('.balloon-items .insertText').contains('Edit Style').should('be.visible').click()
   cy.wait(3000)
}

var editHeatMapStyle = function(){
   cy.get('#gridLayerCommands [title="Options"]').click()
   cy.get('.balloon-items .insertText').contains('Edit Heat Map Style').should('be.visible').click()
   cy.wait(3000)
}

it('Pan the map', function(){
  cy.window().then((win) => {
    expect(win.Dmp).to.exist;
    //var map = win.Dmp.Map.getMap();
    var center_of_map = win.Dmp.Map.getCenter(win.map);
    cy.log(center_of_map)
    //Lat y, Long x
    win.Dmp.Map.setView(win.map, { center: { x: -117.9375, y: 33.93398 }, zoom: 15 });
    cy.wait(2000)
  })
})

//Wait for XHR Response, Can only be done for CRE, CRE has different responses/requests.
cy.server()
cy.route('POST','/getByKey.aspx').as('getParcel')
cy.wait('@getParcel').then((xhr) => {
  cy.wait(5000) //Give some time for the modal to load.
  cy.get('#identify_results_section').then(($modal) => {
    var display = $modal.css('display');
    cy.expect(display).to.equal('block')
  })
})

//Wait for Transaction XHR Response
//Transaction
cy.server()
cy.route('POST','/Transaction.aspx').as('getInfo')
cy.wait('@getInfo').then((xhr) => {
  cy.log('Success')
})
