// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
const {
  addMatchImageSnapshotPlugin,
} = require('cypress-image-snapshot/plugin');

const browserify = require('@cypress/browserify-preprocessor');

module.exports = (on, config) => {
  const options = browserify.defaultOptions;
  options.browserifyOptions.bundleExternal = false;
  // options.browserifyOptions.basedir = __dirname;
  // options.browserifyOptions.paths = ["./node_modules/", path.resolve(__dirname, "../../Bundles/")];
  // options.browserifyOptions.noParse = ["C:\\Users\\MWalker\\Workspace\\SStream-Client\\lib\\3.0\\Bundles\\Core.js"];
  on("file:preprocessor", browserify(options));

  on('task', {
    log(message) {
      console.log(message)
      return null
    }
  })
};

module.exports = (on) => {
  addMatchImageSnapshotPlugin(on);
  on('before:browser:launch', (browser = {}, args) => {
    if (browser.name === 'chrome') {
      args = args.filter((arg) => {
        return arg !== '--disable-blink-features=RootLayerScrolling'
      })

      return args
    }
  })
}
