describe('Buffer Notification', function() {
  //Initial start of the test
  before(function() {
    cy.visit('http://localhost:44344/lib/3.0/build/TestPages/GovClarity_New_Debug.aspx?login=autouser1&account=automationGC', {
      timeout: 40000
    }).wait(20000)
  });
  afterEach(function() {
    if (this.currentTest.state === 'failed') {
      cy.visit('http://localhost:44344/lib/3.0/build/TestPages/GovClarity_New_Debug.aspx?login=autouser1&account=automationGC', {
        timeout: 40000
      }).wait(20000)
    }
  });

  it('Buffer Notification 1.1: Create a buffer list and verify.', function() {
    var address = '217 VIA MALAGA, SAN CLEMENTE, CA 92673';
    //Transaction
    cy.server()
    cy.route('POST', '/Transaction.aspx').as('getInfo')
    cy.searchAddress(address)
    cy.wait(15000)
    //Verify parcel appears.
    cy.get('#balloonDivId').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('.command_name').contains('Buffer / Notification').click()
    cy.get('[value="Create List"]').click()
    //verify Transaction Request appears.
    cy.wait('@getInfo').then((xhr) => {
      cy.log('Success')
    })
    //Verify the List appears and delete list.
    cy.wait(10000)
    cy.get('#null_0_tab').should('be.visible').then(() => {
      cy.get('#gridLayerCommands [title="Options"]').click()
      cy.get('.balloon-items .insertText').contains('Remove List').should('be.visible').click()
    })
    cy.get('#DashboardDiv')
    //Open legend and delete buffer.
    cy.get('#legendIcon').click()
    cy.get('[title="Options"]').first().click()
    cy.wait(3000)
    cy.get('.insertText').contains('Remove Layer').click()
    cy.get('#legend_legendCloseIcon').click()
  })
  it('Buffer Notification 1.2: Show Buffer', function() {
    var address = '217 VIA MALAGA, SAN CLEMENTE, CA 92673';
    //Transaction
    cy.server()
    cy.route('POST', '/Transaction.aspx').as('getInfo')
    cy.searchAddress(address)
    cy.wait(15000)
    //Verify parcel appears.
    cy.get('#balloonDivId').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('.command_name').contains('Buffer / Notification').click()
    cy.get('a[class="editAttributeLabel"]').contains('Show Buffer').click()
    cy.get('a[class="editAttributeLabel"]').contains('Clear Buffer').should('have.attr', 'href')
    cy.get('a[class="editAttributeLabel"]').contains('Clear Buffer').click()
    cy.get('a[class="editAttributeLabel"]').contains('Clear Buffer').should('not.have.attr', 'href')
    //Close the Buffer dialog
    cy.get('.ui-dialog-title').last().contains('Buffer / Notification').should('be.visible').then(($modal) => {
      var id = $modal.attr('id');
      var close_button_selector = '[aria-labelledby=' + id + ']' + ' .ui-dialog-titlebar-close';
      cy.get(close_button_selector).click()
    })
  })
  it('Buffer Notification 1.3: Help', function() {
    var address = '217 VIA MALAGA, SAN CLEMENTE, CA 92673';
    //Transaction
    cy.server()
    cy.route('POST', '/Transaction.aspx').as('getInfo')
    cy.searchAddress(address)
    cy.wait(15000)
    //Verify parcel appears.
    cy.get('#balloonDivId').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('.command_name').contains('Buffer / Notification').click()
    cy.get('[title="Buffer / Notification Help"]').click()
    cy.get('#helpDialog').should('be.visible')
    //Close the help dialog.
    cy.get('[aria-labelledby="ui-dialog-title-helpDialog"] > .ui-dialog-titlebar > .ui-dialog-titlebar-close > .ui-icon').click()
    //Close the dialog.
    cy.get('.ui-dialog-titlebar > .ui-dialog-titlebar-close > .ui-icon').last().click()
  })
  it('Buffer Notification 1.4: Create buffer and add parcel to list.', function() {
    var address = '217 VIA MALAGA, SAN CLEMENTE, CA 92673';
    //Transaction
    cy.server()
    cy.route('POST', '/Transaction.aspx').as('getInfo')
    cy.searchAddress(address)
    cy.wait(15000)
    //Verify parcel appears.
    cy.get('#balloonDivId').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('.command_name').contains('Buffer / Notification').click()
    cy.get('[value="Create List"]').click({
      force: true
    })
    cy.wait(12000)
    cy.get('#mygrid_container a span').invoke('text').as('text1')
    //Verify Transaction Request appears.
    cy.wait('@getInfo').then((xhr) => {
      cy.log('Success')
    })
    //Search Address again
    cy.searchAddress(address)
    cy.wait(15000)
    //Add to list. Verify the Parcel was added.
    cy.get('.command_name').contains('Add to List').click()
    cy.wait(9000)
    cy.get('#mygrid_container a span').invoke('text').as('text2').then(() => {
      cy.log('@text1')
      expect(this.text1).to.not.equal(this.text2)
      var short_address = '217 VIA MALAGA'; //Use for verification
      cy.get('#mygrid_container .objbox').contains(short_address).should('exist')
    })
    //Delete the List
    cy.get('#gridLayerCommands [title="Options"]').click()
    cy.get('.balloon-items .insertText').contains('Remove List').should('be.visible').click()
    cy.get('#DashboardDiv')
    //Open legend and delete buffer.
    cy.get('#legendIcon').click()
    cy.get('[title="Options"]').first().click()
    cy.wait(3000)
    cy.get('.insertText').contains('Remove Layer').click()
    cy.get('#legend_legendCloseIcon').click()
  })
  //Context Menu items not clickable.
  it.skip('Buffer 1.5: Open commands menu in Context menu.', function() {
    var address = '217 VIA MALAGA, SAN CLEMENTE, CA 92673';
    cy.get('#labelCanvasId').trigger('contextmenu', 1000, 850)
    cy.wait(4000)
    cy.window().then((win) => {
      win.document.querySelectorAll("#GoogleEarthMenu > div:nth-child(1) > div")[0].click();
    })
  })
  it('Buffer 1.6: Update Buffer Size', function() {
    var address = '31461 PASEO CAMPEON SAN JUAN CAPISTRANO, CA 92675';
    cy.searchAddress(address)
    cy.wait(15000)
    //Verify parcel appears.
    cy.get('#balloonDivId').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('.command_name').contains('Buffer / Notification').click()
    cy.get('.editAttributeLabel').contains('Show Buffer').click().wait(4000)
    cy.screenshot('1.6buffer/buffer300')
    cy.get('#BufferDistance').clear().type('500')
    cy.get('.editAttributeLabel').contains('Show Buffer').click().wait(4000)
    cy.screenshot('1.6buffer/buffer500')
    //Delete the Buffer
    cy.get('.editAttributeLabel').contains('Clear Buffer').click()
    cy.get('div.ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix > a').last().click()
  })
  it('Buffer 1.7: Toggle buffer layer on off', function() {
    cy.get('#legendIcon').click()
    cy.get('.layerTitle').contains('Buffer').then(($buffer) => {
      var id = $buffer.attr('data-layer');
      cy.get('#' + id).should('be.checked').uncheck()
      cy.wait(3000)
      cy.get('#' + id).should('not.be.checked').check().should('be.checked')
    })
    //Close the Icon
    cy.get('#legend_legendCloseIcon').click()
  })
  it('Buffer 1.8: Create a buffer for a different parcel. Verify old buffer is cleared.', function() {
    var address = '31461 PASEO CAMPEON SAN JUAN CAPISTRANO, CA 92675';
    var address2 = '27682 PASEO BARONA SAN JUAN CAPISTRANO, CA 92675';
    cy.searchAddress(address)
    cy.wait(15000)
    //Verify parcel appears.
    cy.get('#balloonDivId').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('.command_name').contains('Buffer / Notification').click()
    cy.get('#notification-form .editAttributeLabel').contains('Show Buffer').click().wait(4000)
    //Close the Buffer dialog
    cy.get('.ui-dialog-title').last().contains('Buffer / Notification').should('be.visible').then(($modal) => {
      var id = $modal.attr('id');
      var close_button_selector = '[aria-labelledby=' + id + ']' + ' .ui-dialog-titlebar-close';
      cy.get(close_button_selector).click()
    })
    cy.screenshot('1.8buffer/firstAddress')
    cy.searchAddress(address2)
    cy.wait(15000)
    //Verify parcel appears.
    cy.get('#balloonDivId').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.wait(3000)
    cy.get('.command_name').contains('Buffer / Notification').click()
    cy.get('.editAttributeLabel').contains('Show Buffer').click().wait(4000)
    cy.screenshot('1.8buffer/secondAddress')
    //Delete the Buffer
    cy.get('.editAttributeLabel').contains('Clear Buffer').click()
    //Close the Buffer dialog
    cy.get('.ui-dialog-title').last().contains('Buffer / Notification').should('be.visible').then(($modal) => {
      var id = $modal.attr('id');
      var close_button_selector = '[aria-labelledby=' + id + ']' + ' .ui-dialog-titlebar-close';
      cy.get(close_button_selector).click()
    })
  })
  it('Buffer 1.9: Create a polygon and then verify the old buffer can be created on top of the markup layer.', function() {
    var address = '31461 PASEO CAMPEON SAN JUAN CAPISTRANO, CA 92675';
    cy.searchAddress(address)
    cy.wait(15000)
    //Verify parcel appears.
    cy.get('#balloonDivId').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('.js-close_card').click() //Close the modal
    //Draw the Polygon.
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Polygon').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').click(1350, 650, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()
    cy.get('#geoStyler_accept').click().wait(2000)
    //End Draw

    //Create the buffer zone over the markup layer.
    cy.searchAddress(address)
    cy.wait(5000)
    cy.get('.command_name').contains('Buffer / Notification').click()
    cy.wait(3000)
    cy.get('#notification-form .editAttributeLabel').contains('Show Buffer').click().wait(4000)
    //Close the Buffer dialog
    cy.get('.ui-dialog-title').last().contains('Buffer / Notification').should('be.visible').then(($modal) => {
      var id = $modal.attr('id');
      var close_button_selector = '[aria-labelledby=' + id + ']' + ' .ui-dialog-titlebar-close';
      cy.get(close_button_selector).click()
    })
    cy.screenshot('1.9buffer/bufferOverMarkup')
    //Clear the Buffer
    cy.searchAddress(address)
    cy.wait(5000)
    cy.get('.command_name').contains('Buffer / Notification').click()
    cy.wait(3000)
    //Close the Buffer dialog
    cy.get('.ui-dialog-title').last().contains('Buffer / Notification').should('be.visible').then(($modal) => {
      var id = $modal.attr('id');
      var close_button_selector = '[aria-labelledby=' + id + ']' + ' .ui-dialog-titlebar-close';
      cy.get(close_button_selector).click()
    })
    //Clear the Drawings
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Clear Drawings').click()
    cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text').contains('Yes').click()
  })

  it('Buffer 1.10: Create a condo buffer.', function() {
    var apn = 'APN:668-172-22';
    //Transaction
    cy.server()
    cy.route('POST', '/Transaction.aspx').as('getInfo')
    cy.searchAddress(apn)
    cy.wait(15000)
    //Verify parcel appears.
    cy.get('#balloonDivId').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('[value="Create List"]').click()
    //verify Transaction Request appears.
    cy.wait('@getInfo').then((xhr) => {
      cy.log('Success')
    })
    //Verify the List appears
    cy.wait(10000)
    cy.get('#null_0_tab').should('be.visible').then(() => {
      var short_apn = apn.replace('APN:', '')
      cy.get('#mygrid_container .objbox').contains(short_address).should('not.exist')
    })
    //Delete the List
    cy.get('#gridLayerCommands [title="Options"]').click()
    cy.get('.balloon-items .insertText').contains('Remove List').should('be.visible').click()
    cy.get('#DashboardDiv')
    //Open legend and delete buffer.
    cy.get('#legendIcon').click()
    cy.get('[title="Options"]').first().click()
    cy.wait(3000)
    cy.get('.insertText').contains('Remove Layer').click()
    cy.get('#legend_legendCloseIcon').click()
  })
  it('Buffer 1.11: Create a condo buffer with address labels.', function() {
    var apn = 'APN:668-172-22';
    //Transaction
    cy.server()
    cy.route('POST', '/Transaction.aspx').as('getInfo')
    cy.searchAddress(apn)
    cy.wait(15000)
    //Verify parcel appears.
    cy.get('#balloonDivId').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('#BufferDistance').clear().type('500')
    cy.get('[value="Create List"]').click()
    //verify Transaction Request appears.
    cy.wait('@getInfo').then((xhr) => {
      cy.log('Success')
    })
    //Verify the List appears
    cy.wait(10000)
    cy.get('#null_0_tab').should('be.visible').then(() => {
      var short_apn = apn.replace('APN:', '')
      cy.get('#mygrid_container .objbox').contains(short_address).should('not.exist')
      cy.get('#gridLayerCommands [title="Options"]').click()
      cy.get('.insertText').contains('Address Labels').click()
    })
    cy.wait(5000)
    cy.get('.ui-dialog-title').last().contains('Address Labels').should('be.visible').then(($modal) => {
      cy.get('[class=mailInputContainer] [class=mailInput] ').eq(1).check() //Occupant
      cy.get('[name="UseApn"]').eq(0).check() //Yes
      cy.get('[class="editAttributeSubmit]').click()
    })
    cy.wait(5000)
    cy.screenshot('buffer1.11/condoBufferWithLabels')
    //Delete the List
    cy.get('#gridLayerCommands [title="Options"]').click()
    cy.get('.balloon-items .insertText').contains('Remove List').should('be.visible').click()
    cy.get('#DashboardDiv')
    //Open legend and delete buffer.
    cy.get('#legendIcon').click()
    cy.get('[title="Options"]').first().click()
    cy.wait(3000)
    cy.get('.insertText').contains('Remove Layer').click()
    cy.get('#legend_legendCloseIcon').click()
  })
  it('Buffer 1.12: Create a condo buffer and export to CSV.', function() {
    var apn = 'APN:668-172-22';
    //Transaction
    cy.server()
    cy.route('POST', '/Transaction.aspx').as('getInfo')
    cy.searchAddress(apn)
    cy.wait(15000)
    //Verify parcel appears.
    cy.get('#balloonDivId').then(($modal) => {
      var display = $modal.css('display');
      cy.expect(display).to.equal('block')
    })
    cy.get('#BufferDistance').clear().type('500')
    cy.get('[value="Create List"]').click()
    //verify Transaction Request appears.
    cy.wait('@getInfo').then((xhr) => {
      cy.log('Success')
    })
    //Verify the List appears
    cy.wait(10000)
    cy.get('#null_0_tab').should('be.visible').then(() => {
      var short_apn = apn.replace('APN:', '')
      cy.get('#mygrid_container .objbox').contains(short_address).should('not.exist')
      cy.get('#gridLayerCommands [title="Options"]').click()
      cy.get('.insertText').contains('Address Labels').click()
    })
    cy.wait(5000)
    cy.get('.ui-dialog-title').last().contains('Address Labels').should('be.visible').then(($modal) => {
      cy.get('[class=mailInputContainer] [class=mailInput] ').eq(1).check() //Occupant
      cy.get('[name="UseApn"]').eq(0).check() //Yes
      cy.get('[class="editAttributeSubmit]').click()
    })
    cy.wait(5000)
    cy.screenshot('buffer1.11/condoBufferWithLabels')
  })
})

describe('Property Setback', function() {
  //Initial start of the test
  before(function() {
    cy.visit('http://localhost:53633/lib/3.0/build/TestPages/GovClarity_New_Debug.aspx?login=autouser1&account=automationGC', {
      timeout: 40000
    }).wait(20000)
  });
  afterEach(function() {
    if (this.currentTest.state === 'failed') {
      cy.visit('http://localhost:53633/lib/3.0/build/TestPages/GovClarity_New_Debug.aspx?login=autouser1&account=automationGC', {
        timeout: 40000
      }).wait(20000)
    }
  });

  it('Property Setback 1.1: Create a setback', function() {
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Dana Point').click()
    cy.wait(5000)
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Line').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Line.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()

    cy.get('#geoStyler_viewInfo').should('be.visible').click().then(() => {
      cy.get('#identify_results_section').should('be.visible').then(() => {
        cy.get('.command_name').contains('Setback').click()
        cy.wait(5000)
        cy.get('#labelCanvasId').click(200, 350, {
          force: true
        })
        cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"]').should('be.visible').then(() => {
          cy.get('#BufferDistance').clear().type('400')
          cy.get('.editAttributeLabel').contains('Show Setback').click().wait(3000)
          cy.screenshot('setback1.1/setbackOn')
          cy.get('.editAttributeLabel').contains('Clear Setback').click().wait(3000)
          cy.screenshot('setback1.1/setbackOff')
          //Close the dialog
          cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"] .ui-dialog-titlebar-close').click()
        })
      })
      //Clear Drawings
      cy.get('#drawIcon').click()
      cy.get('.insertText').contains('Clear Drawings').click()
      cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text').contains('Yes').click().then(() => {
        cy.get('[title="Clear Layer"]').should('be.visible')
      })
    })
  })
  it('Property Setback 1.2: Open the Setback Help', function() {
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Dana Point').click()
    cy.wait(5000)
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Line').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Line.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()

    cy.get('#geoStyler_viewInfo').should('be.visible').click().then(() => {
      cy.get('#identify_results_section').should('be.visible').then(() => {
        cy.get('.command_name').contains('Setback').click()
        cy.wait(5000)
        cy.get('#labelCanvasId').click(200, 350, {
          force: true
        })
        cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"]').should('be.visible').then(() => {
          cy.get('[title="Setback Help"]').click() //Help icon. Verify the help text is accurate
          cy.get('#helpDialog').should('be.visible').then(($helpDialog) => {
            cy.get('#helpDialog td').first().should('contain', 'Use the Property Setback tool to display a Setback on the map with a few easy steps.')
            cy.screenshot('setback1.2/help')
            cy.get('[aria-labelledby="ui-dialog-title-helpDialog"] .ui-dialog-titlebar-close').click() //Close help dialog
          })
          cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"] .ui-dialog-titlebar-close').click() //Close the dialog
        })
      })
      //Clear Drawings
      cy.get('#drawIcon').click()
      cy.get('.insertText').contains('Clear Drawings').click()
      cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text').contains('Yes').click().then(() => {
        cy.get('[title="Clear Layer"]').should('be.visible')
      })
    })
  })
  it('Property Setback 1.3: Create a setback and verify layer was created.', function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Dana Point').click()
    cy.wait(5000)
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Line').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Line.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()

    cy.get('#geoStyler_viewInfo').should('be.visible').click().then(() => {
      cy.get('#identify_results_section').should('be.visible').then(() => {
        cy.get('.command_name').contains('Setback').click()
        cy.wait(5000)
        cy.get('#labelCanvasId').click(200, 350, {
          force: true
        })
        cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"]').should('be.visible').then(() => {
          cy.get('#BufferDistance').clear().type('400')
          cy.get('.editAttributeLabel').contains('Show Setback').click().wait(3000)
          cy.screenshot('setback1.1/setbackOn')
          //Close the dialog
          cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"] .ui-dialog-titlebar-close').click()
        })
      })
    })
    //Clear the Layer from the legend
    cy.get('#legendIcon').click()
    cy.get('.layerTitle').contains('Setback').then(($setback) => {
      var id = $setback.attr('data-layer');
      cy.get('[data-layer=' + id+ ']' + ' [title="Options"]').click()
      cy.wait(2000)
      cy.get('.insertText').contains('Clear Layer').click()
      cy.get('.ui-button-text').contains('Yes').click()
    })
    //Close the Legend
    cy.get('#legend_legendCloseIcon').click()
    //Clear Drawings
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Clear Drawings').click()
    cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text').contains('Yes').click().then(() => {
      cy.get('[title="Clear Layer"]').should('be.visible') //Clear layer notification.
    })
  })
  it('Property Setback 1.4: Input an invalid distance (special characters) for setback.', function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Dana Point').click()
    cy.wait(5000)
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Line').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Line.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()
    cy.get('#geoStyler_viewInfo').should('be.visible').click().then(() => {
      cy.get('#identify_results_section').should('be.visible').then(() => {
        cy.get('.command_name').contains('Setback').click()
        cy.wait(5000)
        cy.get('#labelCanvasId').click(200, 350, {
          force: true
        })
        cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"]').should('be.visible').then(() => {
          cy.get('#BufferDistance').clear().type('*&^')
          cy.get('.editAttributeLabel').contains('Show Setback').click().wait(3000)
          //Verify the Error notification message appears
          cy.get('#jGrowl .jGrowl-notification .jGrowl-message').should('be.visible').then(($notification) => {
            var text = $notification.text();
            cy.expect(text).to.eq('Invalid input, please input a number')
          })
          //Close the dialog
          cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"] .ui-dialog-titlebar-close').click()
        })
      })
    })
  })
  it('Property Setback 1.5: Input an invalid distance (0 or negative) for setback.', function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Dana Point').click()
    cy.wait(5000)
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Line').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Line.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()
    cy.get('#geoStyler_viewInfo').should('be.visible').click().then(() => {
      cy.get('#identify_results_section').should('be.visible').then(() => {
        cy.get('.command_name').contains('Setback').click()
        cy.wait(5000)
        cy.get('#labelCanvasId').click(200, 350, {
          force: true
        })
        cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"]').should('be.visible').then(() => {
          cy.get('#BufferDistance').clear().type('0')
          cy.get('.editAttributeLabel').contains('Show Setback').click().wait(3000)
          //Verify the Error notification message appears
          cy.get('#jGrowl .jGrowl-notification .jGrowl-message').should('be.visible').then(($notification) => {
            var text = $notification.text();
            cy.expect(text).to.eq('Please input a distance larger than zero')
          })
          //Close the dialog
          cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"] .ui-dialog-titlebar-close').click()
        })
      })
    })
  })
  it('Property Setback 1.6: Create and replace an already created setback', function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Dana Point').click()
    cy.wait(5000)
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Line').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Line.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()
    cy.get('#geoStyler_viewInfo').should('be.visible').click().then(() => {
      cy.get('#identify_results_section').should('be.visible').then(() => {
        cy.get('.command_name').contains('Setback').click()
        cy.wait(5000)
        cy.get('#labelCanvasId').click(200, 350, {
          force: true
        })
        cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"]').should('be.visible').then(() => {
          cy.get('#BufferDistance').clear().type('400')
          cy.get('.editAttributeLabel').contains('Show Setback').click().wait(3000)
          cy.screenshot('setback1.6/firstSetback')
          //Close the dialog
          cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"] .ui-dialog-titlebar-close').click()
        })
      })
    })
    //Draw new setback line.
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Line').should('be.visible').click()
    cy.get('#labelCanvasId').click(400, 550, {
      force: true
    })
    cy.get('#labelCanvasId').click(850, 250, {
      force: true
    })
    cy.get('#labelCanvasId').click(900, 650, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()
    cy.get('#geoStyler_viewInfo').should('be.visible').click().then(() => {
      cy.get('#identify_results_section').should('be.visible').then(() => {
        cy.get('.command_name').contains('Setback').click()
        cy.wait(5000)
        cy.get('#labelCanvasId').click(600, 850, {
          force: true
        })
        cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"]').should('be.visible').then(() => {
          cy.get('#BufferDistance').clear().type('400')
          cy.get('.editAttributeLabel').contains('Show Setback').click().wait(3000)
          cy.screenshot('setback1.6/secondSetback')
          //Close the dialog
          cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"] .ui-dialog-titlebar-close').click()
        })
      })
    })
    //Clear Drawings
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Clear Drawings').click()
    cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text').contains('Yes').click().then(() => {
      cy.get('[title="Clear Layer"]').should('be.visible')
    })
    //Clear the Layer from the legend
    cy.get('#legendIcon').click()
    cy.get('.layerTitle').contains('Setback').then(($setback) => {
      var id = $setback.attr('data-layer');
      cy.get('[data-layer=' + id+ ']' + ' [title="Options"]').click()
      cy.wait(2000)
      cy.get('.insertText').contains('Clear Layer').click()
      cy.get('.ui-button-text').contains('Yes').click()
    })
    //Close the Legend
    cy.get('#legend_legendCloseIcon').click()
  })
  it('Property Setback 1.7: Create a setback and update the style from legend.',function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Dana Point').click()
    cy.wait(5000)
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Line').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Line.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()
    cy.get('#geoStyler_viewInfo').should('be.visible').click().then(() => {
      cy.get('#identify_results_section').should('be.visible').then(() => {
        cy.get('.command_name').contains('Setback').click()
        cy.wait(5000)
        cy.get('#labelCanvasId').click(200, 350, {
          force: true
        })
        cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"]').should('be.visible').then(() => {
          cy.get('#BufferDistance').clear().type('400')
          cy.get('.editAttributeLabel').contains('Show Setback').click().wait(3000)
          //Close the dialog
          cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"] .ui-dialog-titlebar-close').click()
        })
      })
    })
    cy.get('#legendIcon').click()
    //Edit the Setback from the legend
    cy.get('.layerTitle').contains('Setback').then(($setback) => {
      var id = $setback.attr('data-layer');
      cy.get('[data-layer=' + id+ ']' + ' [title="Options"]').click()
      cy.wait(2000)
      cy.get('.insertText').contains('Edit Style').click().wait(4000)
      cy.get('#quickPicker2_colorPicker').click().wait(3000).then(()=>{
        cy.get('#quickPicker2_colorPicker [style=" background-color:#3366ff"]').click() //Blue fill color
      })
      //Close the style dialog
      cy.get('[aria-labelledby="ui-dialog-title-quickPicker2"] .ui-dialog-titlebar-close').click()
    })
    //Clear the Layer from the legend
    cy.get('.layerTitle').contains('Setback').then(($setback) => {
      var id = $setback.attr('data-layer');
      cy.get('[data-layer=' + id+ ']' + ' [title="Options"]').click()
      cy.wait(2000)
      cy.get('.insertText').contains('Clear Layer').click()
      cy.get('.ui-button-text').contains('Yes').click()
    })
    //Clear Drawings
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Clear Drawings').click()
    cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text').contains('Yes').click().then(() => {
      cy.get('[title="Clear Layer"]').should('be.visible')
    })
    cy.get('#legend_legendCloseIcon').click()
  })
  it('Property Setback 1.8: Create a setback and print. Verify the setback appears in the print preview.', function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Dana Point').click()
    cy.wait(5000)
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Line').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Line.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()
    cy.get('#geoStyler_viewInfo').should('be.visible').click().then(() => {
      cy.get('#identify_results_section').should('be.visible').then(() => {
        cy.get('.command_name').contains('Setback').click()
        cy.wait(5000)
        cy.get('#labelCanvasId').click(200, 350, {
          force: true
        })
        cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"]').should('be.visible').then(() => {
          cy.get('#BufferDistance').clear().type('400')
          cy.get('.editAttributeLabel').contains('Show Setback').click().wait(3000)
          //Close the dialog
          cy.get('[aria-labelledby="ui-dialog-title-SetbackDialog"] .ui-dialog-titlebar-close').click()
        })
      })
    })
    cy.get('#printIcon').click()
    cy.get('.insertText').contains('8.5 x 11').click()
    cy.wait(3000)
  })
})

//Remove List currently not working.
describe('Search', function(){
  //Initial start of the test
  before(function() {
    cy.visit('http://localhost:53633/lib/3.0/build/TestPages/GovClarity_New_Debug.aspx?login=autouser1&account=automationGC', {
      timeout: 40000
    }).wait(20000)
  });

  it('Search 1.1: Perform a Property Search (Limit to Map View)', function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Dana Point').click()
    cy.wait(5000)
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Property Search')
    cy.wait(3000)
    cy.get('#filterFormDiv_MapViewLimit').check() //Limit to City
    cy.get('#filterFormDiv_BufferSize').clear().type('200')
    //Open the Property Type Dropdown.
    cy.get('.filterItemText').contains('Property Type').click()
    cy.get('.ui-button-text').contains('Deselect All').last().click() //Deselect all checkboxes.
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > input[type="checkbox"]:nth-child(4)').check() //Multifamily
    cy.get('#filterFormDiv_filterFormContainer > button:nth-child(3)').click()
    cy.wait(20000)
    cy.get('[id="SearchResults: Property Search_1_tab"]').should('be.visible')
    //Delete the List
    cy.get('#mygrid_container [title="Options"]').click()
    cy.get('.insertText').contains('Remove List').click()
    cy.wait(5000)
    cy.get('[id="SearchResults: Property Search_1_tab"]').should('not.be.visible')
  })
  it('Search 1.2: Perform a Owner Name Search (Limit to City)', function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Dana Point').click()
    cy.wait(5000)
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Owner Name Search')
    cy.wait(3000)
    cy.get('#filterFormDiv_MapViewLimit').check() //Limit to City
    cy.get('#filterFormDiv_BufferSize').clear().type('200')
    //Open the Owner Name Dropdown Search Tab.
    cy.get('.filterItemText').contains('Owner Name').click()
    cy.get('text').contains('Owner Name').next().select('contains').next().clear().type('Jim')
    cy.get('#filterFormDiv_filterFormContainer > button:nth-child(3)').click()
    cy.wait(20000)
    cy.get('[id="SearchResults: Owner Name Search _0_tab"]').should('be.visible')
    //Delete the List
    cy.get('#mygrid_container [title="Options"]').click()
    cy.get('.insertText').contains('Remove List').click()
    cy.wait(5000)
    cy.get('[id="SearchResults: Owner Name Search _0_tab"]').should('not.be.visible')
  })
  it('Search 1.3: Verify the Help context menu', function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Dana Point').click()
    cy.wait(5000)
    cy.get('#filterIcon').click()
    cy.get('[title="FilterBar Help"]').click()
    cy.get('#helpDialog').should('be.visible')
    cy.get('#helpDialog td').first().should('contain', 'The Filter Menu options:  Edit, delete, remove or apply a filter to a layer and create an advanced filter')
    cy.get('[aria-labelledby="ui-dialog-title-helpDialog"] .ui-icon-closethick').click() //CLose help Dialog
    cy.get('#fancybox-close').click() //CLose the search.
  })
  it('Search 1.4: Zoom into city boundary. Draw a polygon and limit search to selected feature.', function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Polygon Test').click()
    cy.wait(5000)
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Draw Polygon').should('be.visible').click()
    cy.get('#MapPanel #geoStyler').should('be.visible').wait(4000)
    //Draw the Polygon.
    cy.get('#labelCanvasId').click(1000, 850, {
      force: true
    })
    cy.get('#labelCanvasId').click(1250, 750, {
      force: true
    })
    cy.get('#labelCanvasId').click(1150, 550, {
      force: true
    })
    cy.get('#labelCanvasId').click(1350, 650, {
      force: true
    })
    cy.get('#labelCanvasId').dblclick()
    //End draw
    cy.get('#filterIcon').click()
    cy.wait(3000)
    cy.get('#filterFormDiv_FeatureLimit').check({force:true}) //Limit to Selected Feature
    cy.get('#filterFormDiv_BufferSize').clear().type('2000')
    cy.get('#filterFormDiv_filterFormContainer > button:nth-child(3)').click()
    cy.wait(20000)
    //Clear the drawing
    cy.get('#drawIcon').click()
    cy.get('.insertText').contains('Clear Drawings').click()
    cy.get('.ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text').contains('Yes').click()

  })
  it('Search 1.5: Apply as View Filter with Property Search',function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Dana Point').click()
    cy.wait(5000)
    cy.get('#filterIcon').click()
    cy.get('#filterFormDiv_filterSearchSelect').select('Property Search')
    cy.wait(3000)
    cy.get('#filterFormDiv_DoFilter').check() //Apply as View Filter
    //Open the Property Type Dropdown.
    cy.get('.filterItemText').contains('Property Type').click()
    cy.get('#filterFormDiv_DoFilter').check()
    cy.get('.ui-button-text').contains('Deselect All').last().click() //Deselect all checkboxes.
    cy.get('#filterFormDiv_filterFormContainer > div > div:nth-child(2) > input[type="checkbox"]:nth-child(4)').check() //Multifamily
    cy.get('#filterFormDiv_filterFormContainer > button:nth-child(3)').click()
    cy.wait(20000)
    cy.get('#viewStatus_viewStatus_count').should('be.visible').invoke('text').should('not.equal', '0')
  })

})

describe('Parcel Edit Override', function(){
  var randomstring = require("randomstring");
  //Initial start of the test
  before(function() {
    cy.visit('http://localhost:53633/lib/3.0/build/TestPages/GovClarity_New_Debug.aspx?login=autouser1&account=automationGC', {
      timeout: 40000
    }).wait(20000)
  });

  it('Parcel Override: Search Parcel and edit data associated with parcel. Verify the following(Owner Information, Sale Information, Location Information)', function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Dana Point').click()
    cy.wait(5000)
    cy.searchAddress('31905 DEL OBISPO ST, SAN JUAN CAPISTRANO, CA 92675 ')
    cy.wait(20000)
    cy.get('a').contains('Edit data associated with this parcel').click()
    cy.get('[aria-labelledby="ui-dialog-title-peoDialog"]').should('be.visible').then((modal)=>{
      cy.get('.groupId_Owner_Information').first().scrollIntoView().should('be.visible') //Owner Information
      cy.get('.groupId_Sale_Information').first().scrollIntoView().should('be.visible') //Sales Information
      cy.get('.groupId_Location_Information').first().scrollIntoView().should('be.visible')//Location Information
      cy.get('[aria-labelledby="ui-dialog-title-peoDialog"] .ui-icon-closethick').click() //Close dialog
    })
    cy.get('.js-close_card').click()
  })
  it('Parcel Override: Edit a field in each of the following(Owner Information, Sale Information, Location Information)', function(){
    var owner_name = randomstring.generate(7);
    var legal_description = randomstring.generate(5);

    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Dana Point').click()
    cy.wait(5000)
    cy.searchAddress('31905 DEL OBISPO ST, SAN JUAN CAPISTRANO, CA 92675 ')
    cy.wait(20000)
    cy.get('a').contains('Edit data associated with this parcel').click()
    cy.get('[aria-labelledby="ui-dialog-title-peoDialog"]').should('be.visible').then(()=>{
      //Owner Information
      cy.get('.editAttributeLabel').contains('Owner Name').next().clear().type(owner_name)
      //Sales Information
      cy.get('.editAttributeLabel').contains('Sale Date').next().click()
      cy.get('.ui-datepicker-calendar td[onclick]').should('be.visible').then(($calendar)=>{
        var count = $calendar.length;
        var rand = Math.floor((Math.random() * count)+1);
        var selector = ':eq(' + rand + ')'
        cy.get('.ui-datepicker-calendar td[onclick]' + selector).click()
      })
      cy.get('.editAttributeLabel').contains('Legal Description').next().clear().type(legal_description)
      cy.get('.ui-button-text').contains('Save').click()
    })
    cy.wait(5000)
    //Verify the information was saved.
    cy.searchAddress('31905 DEL OBISPO ST, SAN JUAN CAPISTRANO, CA 92675 ')
    cy.wait(5000)
    cy.get('[id-surfedit="tableField_group1"] a').contains(owner_name).should('be.visible')
    cy.get('[id-surfedit="tableField_group1"] [data-surfedit="tableField_value"]').contains(owner_name).should('be.visible')
    cy.get('.js-close_card').click()
  })
  it('Parcel Override: Edit a field in each of the following(Owner Information, Sale Information - Sales Price, Location Information)', function(){
    var owner_name = randomstring.generate(7);
    var legal_description = randomstring.generate(5);
    var price = '5000';

    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Dana Point').click()
    cy.wait(5000)
    cy.searchAddress('31905 DEL OBISPO ST, SAN JUAN CAPISTRANO, CA 92675 ')
    cy.wait(20000)
    cy.get('a').contains('Edit data associated with this parcel').click()
    cy.get('[aria-labelledby="ui-dialog-title-peoDialog"]').should('be.visible').then(()=>{
      //Owner Information
      cy.get('.editAttributeLabel').contains('Owner Name').next().clear().type(owner_name)
      //Sales Information
      cy.get('.editAttributeLabel').contains('Sale Price').next().clear().type(price)
      //Location Information
      cy.get('.editAttributeLabel').contains('Legal Description').next().clear().type(legal_description)
      cy.get('.ui-button-text').contains('Save').click()
    })
    cy.wait(5000)
    //Verify the information was saved.
    cy.searchAddress('31905 DEL OBISPO ST, SAN JUAN CAPISTRANO, CA 92675 ')
    cy.wait(5000)
    cy.get('[id-surfedit="tableField_group1"] a').contains(owner_name).should('be.visible')
    cy.get('[id-surfedit="tableField_group1"] [data-surfedit="tableField_value"]').contains(owner_name).should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Last Sale Price').next().then(($price)=> {
      var p = $price.text();
      p = p.replace(/(\D)/g, '');
      expect(p).to.eq(price);
    })
    cy.get('.js-close_card').click()
  })
  it('Parcel Override: Search by APN and edit zipcode, sales price, and assessed value.', function(){
    var apn = 'APN: 66820105';
    var zipcode = '90589';
    var assessed_value = '500000';
    var price = '5000';

    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Dana Point').click()
    cy.wait(5000)
    cy.searchAddress(apn)
    cy.wait(20000)
    cy.get('a').contains('Edit data associated with this parcel').click()
    cy.get('[aria-labelledby="ui-dialog-title-peoDialog"]').should('be.visible').then(()=>{
      //Owner Information
      cy.get('.editAttributeLabel').contains('Owner Address Zip Code').next().clear().type(zipcode)
      //Sales Information
      cy.get('.editAttributeLabel').contains('Sale Price').next().clear().type(price)
      //Location Information
      cy.get('.editAttributeLabel').contains('Assessed Value').next().clear().type(assessed_value)
      cy.get('.ui-button-text').contains('Save').click()
    })
    cy.wait(5000)
    //Verify the information was saved.
    cy.searchAddress(apn)
    cy.wait(5000)
    cy.get('[id-surfedit="tableField_group1"] [data-surfedit="tableField_value"]').contains(zipcode).should('be.visible')
    cy.get('[data-surfedit="tableField_label"]').contains('Last Sale Price').next().then(($price)=> {
      var p = $price.text();
      p = p.replace(/(\D)/g, '');
      expect(p).to.eq(price);
    })
    cy.get('[data-surfedit="tableField_label"]').contains('Total Assd. Value').scrollIntoView().next().then(($value)=> {
      var v = $value.text();
      v = v.replace(/(\D)/g, '');
      expect(v).to.eq(assessed_value);
    })
    cy.get('.js-close_card').click()
  })
})

describe('Address Management', function(){
  var randomstring = require("randomstring");
  //Initial start of the test
  before(function() {
    cy.visit('http://localhost:53633/lib/3.0/build/TestPages/GovClarity_New_Debug.aspx?login=autouser1&account=automationGC', {
      timeout: 40000
    }).wait(20000)
  });

  it('Enter address and search', function() {
    var address = '217 VIA MALAGA, SAN CLEMENTE, CA 92673';
    cy.searchAddress(address)
  })
  it('Address Management 1.1: Validate Address Management appears', function(){
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Dana Point').click()
    cy.wait(5000)
    cy.searchAddress(apn)
    cy.wait(20000)
    cy.get('[data-surfedit="card_item"]').contains('Address Management').should('be.visible')
  })
  it('Address Management 1.2: Create a Single Custom Address', function(){
    var address = '24904 DANAMAPLE, DANA POINT, CA 92629';
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Dana Point').click()
    cy.wait(5000)
    cy.searchAddress(address)
    cy.wait(20000)
    cy.get('[data-surfedit="card_item"]').contains('Address Management').scrollIntoView().should('be.visible')
    cy.get('a').contains('Add a single custom address').click()
    cy.get('[aria-labelledby="ui-dialog-title-editLocationDialog"]').should('be.visible').then(()=>{
      cy.get('#apnInput').clear().type('673-300-85')
      cy.get('#addressNumberStartInput').clear().type('89636')
      cy.get('#streetNameInput').clear().type('Oxford')
      cy.get('#cityInput').clear().type('Los Angeles')
      cy.get('#stateInput').clear().type('CA')
      cy.get('#postalCodeInput').clear()
      cy.get('.ui-button-text').contains('Continue').last().click()
    })
    //Click on a random point.
    cy.get('#labelCanvasId').click(1350, 650, {
      force: true
    })
    cy.wait(3000)
    cy.get('[aria-labelledby="ui-dialog-title-editLocationAlertDialog"] #editLocationAlertDialog').should('be.visible')
    cy.get('[aria-labelledby="ui-dialog-title-editLocationAlertDialog"] .ui-button-text').last().contains('Ok').click()
    cy.get('.js-close_card').click()
  })
  it.only('Address Management 1.3: Select Publish Custom Addresses', function(){
    var address = '24904 DANAMAPLE, DANA POINT, CA 92629';
    cy.get('#bookmarkIcon').click()
    cy.get('.insertText').contains('Search - Dana Point').click()
    cy.wait(5000)
    cy.searchAddress(address)
    cy.get('#advancedIcon').click()
    cy.get('.insertText').contains('Publish Custom Addresses').click()
  })
})
