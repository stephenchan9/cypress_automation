describe("Builder Sites Filter", () => {
  // Open LV
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(20000);
    cy.get("#bookmarkIcon").click();
    cy.get(".insertText").contains("Irvine - Years Between").click().wait(8000);
  });
  // Click on the Search Filter
  beforeEach(() => {
    cy.get("#filterIcon").click();
    cy.get("#filterFormDiv_filterSearchSelect").should("be.visible").select("Builder Sites");
    cy.get("#filterFormDiv_filterSearchSelect").should("have.value", "Builder Sites");
    cy.wait(4000);
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.reload({ timeout: 40000 }).wait(20000);
    }
  });

  it("Click the Select All and apply", () => {
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("Select All").click();
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click();
    cy.wait(4000);
    cy.get("#filterFormDiv_filterFormContainer").should("not.be.visible");
  });
  it("Click Deselect All and apply", () => {
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("Deselect All").click();
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click();
    cy.wait(4000);
    cy.get("#filterFormDiv_filterFormContainer").should("not.be.visible");
  });
  it("Input text in the first Builder Name input box and click ok", () => {
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(156) > input[type=\"text\"]:nth-child(3)")
      .clear().type("John");
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click();
  });
});
