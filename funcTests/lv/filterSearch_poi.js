describe("POI search", () => {
  beforeEach(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(20000);
    cy.get("#bookmarkIcon").click();
    cy.get(".insertText").contains("Irvine - Years Between").click().wait(8000);
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(20000);
      cy.get("#bookmarkIcon").click();
      cy.get(".insertText").contains("Irvine - Years Between").click().wait(8000);
    }
  });
  it("Do a POI search with all major restaurants selected. Add to List. Press Print command in the results list. (8.5x11)", () => {
    cy.get("#filterIcon").click();
    cy.get("#filterFormDiv_filterSearchSelect").select("Points of Interest Search").should("have.value", "Points of Interest Search");
    cy.get(".filterItemText").contains("Major Brands").click();
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(2) > button:nth-child(1)").click();
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click().wait(8000);
    cy.get("#viewStatus_viewStatus_count").then(($countOfList) => {
      cy.wait(5000);
      const count = $countOfList.text();
      cy.expect(count).to.not.eq("0");
      cy.expect(count).to.not.eq(" --");
      cy.get(".viewStatusCommand").contains("Add To List").click().wait(20000); // Takes a long wait to add to list.
      cy.get("#gridLayerCommands [title=\"Options\"]").click();
      cy.get(".balloon-items .insertText").contains("8.5 x 11 Print").should("be.visible").click();
    });
  });
  it("Do a POI search and create Trade Area", () => {
    // Search POI
    cy.get("#filterIcon").click();
    cy.get("#filterFormDiv_filterSearchSelect").select("Points of Interest Search").should("have.value", "Points of Interest Search");
    cy.get(".filterItemText").contains("Major Brands").click();
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(2) > div:nth-child(6) > div:nth-child(6) > div > button:nth-child(1) > span")
      .click();
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click().wait(8000);
    cy.get("#searchInputBox").type("2457 PARK AVE, TUSTIN, CA 92782");
    cy.get("#searchButton").click();

    cy.wait(6000);
    cy.get(".js-close_card").click();
    // Add to List
    cy.get("#viewStatus_viewStatus_count").then(($countOfList) => {
      cy.wait(5000);
      const count = $countOfList.text();
      cy.expect(count).to.not.eq("0");
      cy.expect(count).to.not.eq(" --");
      cy.get(".viewStatusCommand").contains("Add To List").click().wait(20000); // Takes a long wait to add to list.
    });
    // Click Create Trade Area command in the results list.
    cy.get("#gridLayerCommands [title=\"Options\"]").click();
    cy.get(".balloon-items .insertText").contains("Create Trade Area").should("be.visible").click()
      .wait(4000);
    // Create a trade area with new Project Name.
    cy.get(".ui-dialog-title").contains("Trade Area Settings").then(($title) => {
      let uniqueId = $title.attr("id");
      const randomStr = Math.random().toString(36).substring(7); // Create a random string.
      uniqueId = `#${uniqueId.replace("ui-dialog-title-", "")}`;
      cy.get(`${uniqueId} select`).first().select("New", { force: true });
      cy.get("#newFolderInput").type(randomStr);
      cy.get(".ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text").last().click();
      // Select Ring Type: Radius
      cy.get(uniqueId).then(() => {
        const ringType = uniqueId.replace("root", "RingType_Select");
        cy.log(ringType);
        cy.get(ringType).select("Radius");
      });
      // Select Ring Use: Candidate Sites
      cy.get(uniqueId).then(() => {
        const ringUse = uniqueId.replace("root", "RingUse_Select");
        cy.log(ringUse);
        cy.get(ringUse).select("Candidate Sites");
      });
      // Select Max Ring Size: 15 minutes
      cy.get(uniqueId).then(() => {
        const ringSize = uniqueId.replace("root", "RingSize_Select");
        cy.log(ringSize);
        cy.get(ringSize).select("3 Miles");
      });
      cy.get(".ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text").last().contains("OK").click();
      cy.wait(11000); // Wait for the layer to take into effect.
      // cy.screenshot("tradeAreaOn");

      // Delete the Trade Area Sites/Rings
      cy.get("#legendIcon").click().wait(2000);
      cy.get("[value=\"More Layers\"]").click();
      cy.get("#legend_browseLayers_activeList").select("Trade Area Sites");
      cy.get("#legend_browseLayers_removeButton").click();
      cy.get("#legend_browseLayers_activeList").select("Trade Area Rings");
      cy.get("#legend_browseLayers_removeButton").click();
      cy.wait(2000);
      cy.get("#legend_browseLayers_okButton").click();
      cy.get("#legend_legendCloseIcon").click();
    });
  });
});
