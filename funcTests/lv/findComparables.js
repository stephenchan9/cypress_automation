describe("Find Comparables", () => {
  // Initial start of the test
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(30000);
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(30000);
    }
  });
  it("Find Comparables for Single Family Residence ", () => {
    const address = "14 WOODHOLLOW";
    // Enter a Address to search
    cy.searchAddress2(address);
    // ---End of Check
    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      cy.get("[placeholder=MIN]").clear().type("850000");
      cy.get("[placeholder=MAX]").clear().type("900000");
      // Input Lot Size (Acre)
      cy.get(`${id}LotAcre_Input`).clear().type(".05");
      // Input Assessed Value
      cy.get(`${id}AssessedVal_Input`).clear().type("15");
      // Select within Miles
      cy.get(`${id}Mile`).select("5");
      // Click Search Button
      cy.get(`${id}SearchButton`).click();
    });
    cy.wait(25000);// Wait for results
    cy.get(".js-close_card").click();
    // Verify the Compared results appear.
    cy.get("#mygrid_container li span").first().then(($results) => {
      cy.expect($results.text()).contains(`Comps ${address}`);
    });
    // Delete the List
    cy.get("#gridLayerCommands [title=\"Options\"]").click();
    cy.get(".insertText").contains("Remove List").click();
  });
  it("Find Comparables for Vacant Land (Residential)", () => {
    const address = "9999 W YALE LOOP";
    // Enter a Address to search
    cy.searchAddress2(address);

    // ---End of Check
    cy.get("#identify_results_section").then(($modal) => {
      const display = $modal.css("display");
      cy.expect(display).to.equal("block");
    });
    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      // Input Lot Size (Acre)
      cy.get(`${id}LotAcre_Input`).clear().type(".05");
      // Click on a Random Area
      cy.get("td").contains("Last Sale Price: ").click();
      // Click Search Button
      cy.get(`${id}SearchButton`).click();
    });
    cy.wait(25000);// Wait for the results to load
    cy.get(".js-close_card").click();
    // Verify the Compared results appear.
    cy.get("#mygrid_container li span").then(($results) => {
      cy.expect($results.text()).contains("Comps 9999 W YALE");
    });
    // Delete the List
    cy.get("#gridLayerCommands [title=\"Options\"]").click();
    cy.get(".insertText").contains("Remove List").click();
  });
  it("Find Comparables for Industrial", () => {
    const address = "16666 VON KARMAN AVE";
    // Enter a Address to search
    cy.searchAddress2(address);

    cy.get("#identify_results_section").then(($modal) => {
      const display = $modal.css("display");
      cy.expect(display).to.equal("block");
    });
    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      // Input Assessed Value
      cy.get(`${id}AssessedVal_Input`).clear().type(".3");
      // Select within Miles
      cy.get(`${id}Mile`).select("15");
      // Click Search Button
      cy.get(`${id}SearchButton`).click();
    });
    cy.wait(25000);// Wait for results
    cy.get(".js-close_card").click();
    // Verify the Compared results appear.
    cy.get("#mygrid_container li span").first().then(($results) => {
      cy.expect($results.text()).contains(`Comps ${address}`);
    });
    // Delete the List
    cy.get("#gridLayerCommands [title=\"Options\"]").click();
    cy.get(".insertText").contains("Remove List").click();
  });
  it("Find Comparables for Commercial", () => {
    const address = "3901 W METROPOLITAN DR";
    // Enter a Address to search
    cy.searchAddress2(address);
    // ---Check for the Parcel to appear.

    // ---End of Check
    cy.get("#identify_results_section").then(($modal) => {
      const display = $modal.css("display");
      cy.expect(display).to.equal("block");
    });
    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      // Input Lot Size (Acre)
      cy.get(`${id}LotAcre_Input`).clear().type("2");
      // Click Search Button
      cy.get(`${id}SearchButton`).click();
      cy.get(`${id}SearchButton`).click();
    });
    cy.wait(25000);
    cy.get(".js-close_card").click();
    // Verify the Compared results appear.
    cy.get("#mygrid_container li span").first().then(($results) => {
      cy.expect($results.text()).contains(`Comps ${address}`);
    });
    // Delete the List
    cy.get("#gridLayerCommands [title=\"Options\"]").click();
    cy.get(".insertText").contains("Remove List").click();
  });
});

describe("Find Comparables Workflow", () => {
  // Initial start of the test
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(20000);
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.reload({ timeout: 40000 }).wait(20000);
    }
  });

  it("Find Comparables and print", () => {
    const address = "18831 VON KARMAN";
    // Enter a Address to search
    cy.searchAddress2(address);
    // ---End of Check
    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      cy.get(`${id}SoldDate_Select`).select("24");
      cy.get(`${id}LotAcre_Input`).type("2");
      // Click Search Button 2 times.
      cy.get(`${id}SearchButton`).click();
      cy.get(`${id}SearchButton`).click();
    });
    cy.wait(25000);// Wait for results
    // Verify the Compared results appear.
    cy.get("#mygrid_container li span").first().then(($results) => {
      cy.expect($results.text()).contains(`Comps ${address}`);
    });
    cy.get("#gridLayerCommands [title=\"Options\"]").click();
    cy.get(".balloon-items .insertText").contains("8.5 x 11 Print").should("be.visible").click();
  });
  it("Find Comparables and Export Image to toolbar icon (JPEG).", () => {
    const address = "18831 VON KARMAN";
    // Enter a Address to search
    cy.searchAddress2(address);
    // ---End of Check
    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      cy.get(`${id}SoldDate_Select`).select("24");
      cy.get(`${id}LotAcre_Input`).type("2");
      // Click Search Button 2 times.
      cy.get(`${id}SearchButton`).click();
      cy.get(`${id}SearchButton`).click();
    });
    cy.wait(25000);// Wait for results
    // Verify the Compared results appear.
    cy.get("#mygrid_container li span").first().then(($results) => {
      cy.expect($results.text()).contains(`Comps ${address}`);
    });
    cy.get("#exportImageIcon").click();
    cy.get(".insertText").contains("Image (JPEG)").click();
  });
});

describe("New Tests", () => {
  // Initial start of the test
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(20000);
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(30000);
    }
  });

  it("Verify Find Comparables Values are visible", () => {
    const address = "13003 GOLLER AVE, NORWALK, CA 90650";
    // Enter a Address to search
    cy.searchAddress(address);
    // ---End of Check
    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    // Verify Elements are visible
    cy.get("td").contains("Owner: ").should("be.visible");// Owner
    cy.get("td").contains("Subject Site: ").should("be.visible");// Subject Site
    cy.get("td").contains("Land Use: ").should("be.visible");// Land Use
    cy.get("td").contains("Sold within last: ").should("be.visible");// Sold within last
    cy.get("td").contains("Sale Price: ").should("be.visible");// Sale Price
    cy.get("td").contains("APN#: ").should("be.visible");// APN
    cy.get("td").contains("Last Sale Date: ").should("be.visible");// Last Sale Date
    cy.get("td").contains("Last Sale Price: ").should("be.visible");// Last Sale SalePrice
    cy.get("td").contains("Building Area: ").should("be.visible");// Building Area
    cy.get("td").contains("Lot Size (Acre): ").should("be.visible");// Lot Size(Acre)
    cy.get("td").contains("Lot Size (Sq Ft): ").should("be.visible");// Lot Size(Sq Ft)
    cy.get("td").contains("# of Units: ").should("be.visible");// # of Units
    cy.get("td").contains("# of Stories: ").should("be.visible");// # of Stories
    cy.get("td").contains("Year Built: ").should("be.visible");// Year Built
    cy.get("td").contains("Assessed Value: ").should("be.visible");// Assessed Value
    cy.get("td").contains("Improvement %: ").should("be.visible");// Improvement %
    cy.get("[value=\"Mile\"]").should("be.visible");
    cy.get("[value=\"Zip\"]").should("be.visible");
    cy.get("[value=\"City\"]").should("be.visible");
    cy.get("[value=\"County\"]").should("be.visible");
    cy.get("td").contains("Additional Land Uses: ").should("be.visible");// Additional Land Uses.
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      cy.get(`${id}CancelButton`).click();
    });
  });
  it("Sold within last 24 months test", () => {
    const address = "18831 VON KARMAN";
    // Enter a Address to search
    cy.searchAddress(address);
    // ---End of Check
    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      cy.get(`${id}SoldDate_Select`).select("24");
      cy.get(`${id}LotAcre_Input`).type("2");
      // Click Search Button 2 times.
      cy.get(`${id}SearchButton`).click();
      cy.get(`${id}SearchButton`).click();
    });
    cy.wait(25000);// Wait for results
    // Verify the Compared results appear.
    cy.get("#mygrid_container li span").first().then(($results) => {
      cy.expect($results.text()).contains(`Comps ${address}`);
    });
  });
  it("Sold within last 12 months test with Sq. Ft. Search Within Irvine", () => {
    const address = "18831 VON KARMAN";
    // Enter a Address to search
    cy.searchAddress(address);
    // ---End of Check
    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      cy.get(`${id}SoldDate_Select`).select("24");
      cy.get(`${id}LotFt_Input`).type("100000");
      cy.get("[value=\"City\"]").check();
      // Click Search Button
      cy.get(`${id}SearchButton`).click();
    });
    cy.wait(25000);// Wait for results
    // Verify the Compared results appear.
    cy.get("#mygrid_container li span").first().then(($results) => {
      cy.expect($results.text()).contains(`Comps ${address}`);
    });
  });
  it("Sold within last 12 months test with sale price between 2 values. Verify no results found", () => {
    const address = "18831 VON KARMAN";
    // Enter a Address to search
    cy.searchAddress(address);

    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      cy.get(`${id}SoldDate_Select`).select("12");
      cy.get(`${id}SalePriceMin`).type("1000");
      cy.get(`${id}SalePriceMax`).type("10000000");
      cy.get("[value=\"Zip\"]").check();
      // Click Search Button twice
      cy.get(`${id}SearchButton`).click();
      cy.get(`${id}SearchButton`).click();
      cy.get(".ui-dialog-content").contains("Sorry, no results were found. Please try again using different criteria.").should("be.visible");
      cy.wait(2000);
      cy.get(".ui-icon-closethick").last().click({ force: true });
      cy.get(`${id}CancelButton`).click();
    });
    cy.get(".ui-icon-closethick").last().click({ force: true });
    cy.get(".js-close_card").click();
  });
  it("Sold within last 36 months. With an assessed value and search within county.", () => {
    const address = "18831 VON KARMAN";
    // Enter a Address to search
    cy.searchAddress(address);
    // ---End of Check
    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      cy.wait(2000);
      cy.get(`${id}SoldDate_Select`).select("36");
      cy.get(`${id}AssessedVal_Input`).type("5");
      cy.get("[value=\"County\"]").check();
      // Click Search Button
      cy.get(`${id}SearchButton`).click();
    });
    cy.wait(25000);// Wait for results
    // Verify the Compared results appear.
    cy.get("#mygrid_container li span").first().then(($results) => {
      cy.expect($results.text()).contains(`Comps ${address}`);
    });
  });
  it("Verify only numeric fields.", () => {
    const address = "18831 VON KARMAN";
    // Enter a Address to search
    cy.searchAddress(address);
    // ---End of Check
    cy.get("#summary .command_name").contains("Find Comparables").click().wait(3000);
    cy.get(".ui-dialog-title").contains("Find Comparables").should("be.visible");
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      cy.get(`${id}SalePriceMin`).type("*");
      cy.get(`${id}SalePriceMax`).type("*");
      cy.get(`${id}LotAcre_Input`).type("$");
      cy.get(`${id}LotFt_Input`).type("$");
      cy.get(`${id}AssessedVal_Input`).type("$");
      cy.get(`${id}Improv_Input`).type("$");
      cy.get("[type=\"submit\"]").click({ force: true });
      cy.get(`${id}SalePriceMin`).should("have.class", "ui-state-error");
      cy.get(`${id}SalePriceMax`).should("have.class", "ui-state-error");
      cy.get(`${id}LotAcre_Input`).should("have.class", "ui-state-error");
      cy.get(`${id}LotFt_Input`).should("have.class", "ui-state-error");
      cy.get(`${id}AssessedVal_Input`).should("have.class", "ui-state-error");
      cy.get(`${id}Improv_Input`).should("have.class", "ui-state-error");
    });
    // Close the modal.
    cy.get("[placeholder=MIN]").then(($inputBlock) => {
      const salePriceMinBox = $inputBlock.attr("id");
      const id = `#${salePriceMinBox.replace("SalePriceMin", "")}`;
      cy.get(`${id}CancelButton`).click();
    });
  });
});
