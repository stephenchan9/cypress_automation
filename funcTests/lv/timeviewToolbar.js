describe("Time View Toolbar", () => {
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(20000);
    cy.get("#bookmarkIcon").click();
    cy.get(".insertText").contains("Irvine - Years Between").click().wait(8000);
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(20000);
    }
  });

  it("Click Time View and select a different View.", () => {
    const address = "179 FIELDWOOD, IRVINE, CA 92618";
    cy.searchAddress(address);
    // ---Wait for the parcel to load

    cy.get("#timeViewMenu").click();
    cy.get("#timeViewToolbar").then(($modal) => {
      const display = $modal.css("display");
      cy.expect(display).to.equal("block");
    });
    cy.get("[name=\"timeview\"]").then(($btns) => {
      const count = $btns.length - 1;
      let rand = Math.floor((Math.random() * count) + 1); // Select a rabdom number between 1 and the count
      if (rand === 1) { // Condition so "Default" is not selected.
        rand += 1;
      }
      const selector = `:eq(${rand})`;
      cy.get(`[name="timeview"]${selector}`).check();
      // Verify the selected Date matches the Date on the bottom of modal.
      cy.get(`[name="timeview"]${selector}`).next().then(($label) => {
        const text = $label.text().replace(/[^0-9-]/g, ""); // Removes extra characters to match.
        cy.get("#timeViewToolbar_Label").should("have.text", text);
      });
    });
    cy.get("#timeViewToolbar_CloseIcon").click({ force: true });
  });
  it("Click Time View and Refresh the List after panning", () => {
    const address = "345 LANG AVE, LA PUENTE, CA 91744";
    cy.searchAddress(address);
    // ---Wait for the parcel to load

    cy.get("#timeViewMenu").click();
    cy.wait(4000);
    cy.get("#timeViewToolbar").then(($modal) => {
      const display = $modal.css("display");
      cy.expect(display).to.equal("block");
    });
    cy.get("[name=\"timeview\"]").then(($btns) => {
      const count = $btns.length - 1;
      cy.log(count);
      let rand = Math.floor((Math.random() * count) + 1); // Select a rabdom number between 1 and the count
      if (rand === 1) { // Condition so "Default" is not selected.
        rand += 1;
      }
      const selector = `:eq(${rand})`;
      cy.get(`[name="timeview"]${selector}`).check();
      // Verify the selected Date matches the Date on the bottom of modal.
      cy.get(`[name="timeview"]${selector}`).next().then(($label) => {
        const text = $label.text().replace(/[^0-9-]/g, ""); // Removes extra characters to match.
        cy.get("#timeViewToolbar_Label").should("have.text", text);
      });
    });
    // Pan the Map
    const x = -117.937;
    const y = 33.93398;
    const zoom = 15;
    cy.panMap(x, y, zoom);
    // Time View toolbar remains open.
    cy.get("#timeViewToolbar").then(($modal) => {
      const display = $modal.css("display");
      cy.expect(display).to.equal("block");
    });
    // Refresh the List with new dates.
    cy.get("#timeViewToolbar_Refresh").click();
    cy.get("#timeViewToolbar_CloseIcon").click({ force: true });
  });
});
