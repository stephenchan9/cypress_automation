describe("Load Layers from file", () => {
  // Initial start of the test
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(30000);
    cy.get("#bookmarkIcon").click();
    cy.get(".insertText").contains("Load Data- Tests").click().wait(8000);
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(30000);
    }
  });

  it.skip("Load shapefile from legend. IN PROGRESS", () => {
    cy.get("#legendIcon").click();
    cy.get("input[value=\"More Layers\"]").click();
    cy.get("#mruDropDown_1").select("USER", { force: true });
    cy.get("#legend_browseLayers_dataloaderSelect").select("Shapefile", { force: true });
    const layerTitle = Math.random().toString(36).substring(7);
    cy.wait(3000);
    // Shape Loader, iFrame access.
    cy.get("iframe").then(($iframe) => {
      const doc = $iframe.contents();
      doc.find("#inputForm #layerInput").type(layerTitle);
    });
    cy.upload_file("#fileInput", "ManHoles.zip");
  }); // Cypress can't access iFrame
  it("Load spreadsheet from legend.", () => {
    cy.get("#legendIcon").click();
    cy.get("input[value=\"More Layers\"]").click();
    cy.get("#legend_browseLayers .mruDropdown").first().select("USER", { force: true });
    cy.get("#legend_browseLayers_dataloaderSelect").select("Spreadsheet", { force: true });
    cy.upload_file(".upload_file_box", "TestAutomation.csv");
    const layerTitle = Math.random().toString(36).substring(7);
    cy.get("#createLayerLayerName").type(layerTitle);
    cy.get("#createLayerCreate").click();
    cy.get("#legend_browseLayers_okButton").click();
    cy.wait(3000); // Give time for the layer to appear in list.
    // Verify the Layer was loaded sucessfully.
    cy.get("[title=\"Table Loader Job\"]").should("be.visible");
    // Delete the Uploaded Layer.
    cy.get("input[value=\"More Layers\"]").click();
    cy.get("#legend_browseLayers_availableList").select(layerTitle);
    cy.get(".ui-button-text").contains("Delete Layer").click();
    cy.wait(2000);
    cy.get("option").contains(layerTitle).should("not.be.visible");
    cy.get("#legend_browseLayers_okButton").click();
    cy.get("#legendIcon").click();
  });
  // Cross Origin Iframe issues.
  it.skip("Site Overlay: Upload an image.", () => {
    cy.get("#drawIcon").click();
    cy.get(".bMCommand .insertText").contains("Overlay Site Plan").click();
    cy.get("[aria-labelledby=\"ui-dialog-title-imageOverlayPicker\"] .ui-button-text").contains("Upload").click();

    /*
    cy.get('#imageOverlayUploadForm').then(function ($iframe) {
      const $jbody = $iframe.contents().find('body');
      cy.log($jbody)
      const $body = $jbody[0];

      cy.wrap($body).get('#DATAFILE').should('exist').should('be.visible')
    })
    */
    cy.get("#imageOverlayUploadForm").should("be.visible").then(() => {
      cy.upload_file2("rick-and-morty-season-4.jpg", "#DATAFILE");
      cy.get("#submitButton").click();
      cy.get("div[id=imageOverlayPicker_Shared] td").last().click();
      cy.get("[aria-labelledby=\"ui-dialog-title-imageOverlayPicker\"] .ui-button-text").contains("OK").click();
      cy.get(".ui-dialog-title").contains("Set Control Points").should("be.visible");
    });
  });
});
