
describe("Property Search filter to click Select All and Deselect All", () => {
  // Open LV
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", {
      timeout: 40000,
    }).wait(10000);
  });
  // Click on the Search Filter and select Property Search
  beforeEach(() => {
    cy.get("#filterIcon").click();
    cy.get("#filterFormDiv_filterSearchSelect").should("be.visible").select("Property Search");
    cy.get("#filterFormDiv_filterSearchSelect").should("have.value", "Property Search").wait(4000);
    cy.get(".filterItemText").contains("Property Type").click();
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", {
        timeout: 40000,
      }).wait(10000);
    }
  });

  it("Click the Select All and apply", () => {
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("Select All").click();
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click().wait(4000);
    cy.get("#filterFormDiv_filterFormContainer").should("not.be.visible");
  });
  it("Click Deselect All and apply", () => {
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("Deselect All").click();
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click().wait(4000);
    cy.get("#filterFormDiv_filterFormContainer").should("not.be.visible");
  });
  it("Open all tabs in Property Search", () => {
    cy.get(".filterItemText").contains("Property Type").click();
    cy.get(".filterItemText").contains("Characteristics").click();
    cy.get(".filterItemText").contains("Last Market Sale").click();
    cy.get(".filterItemText").contains("Ownership").click();
    cy.get(".filterItemText").contains("Property Value").click();
    cy.get(".filterItemText").contains("Location").click();
  });
});


describe("Property Search", () => {
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", {
      timeout: 40000,
    }).wait(20000);
    cy.get("#bookmarkIcon").click();
    cy.get(".insertText").contains("Irvine - Years Between").click().wait(8000);
  });
  beforeEach(() => {
    cy.get("#filterIcon").click();
    cy.get("#filterFormDiv_filterSearchSelect").select("Property Search").should("have.value", "Property Search");
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", {
        timeout: 40000,
      }).wait(20000);
    }
  });

  // Search property and add to list.
  it("Do a Property Search with years built characteristic filter and last 12 months radio button selected. Add Results to list.", () => {
    cy.get(".filterItemText").contains("Characteristics").click();
    // Input the years in the box.
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type=\"text\"]:nth-child(3)")
      .clear().type("2000");
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type=\"text\"]:nth-child(5)")
      .clear().type("2018");
    // Last Market Sale 12 months
    cy.get(".filterItemText").contains("Last Market Sale").click();
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(6) > div:nth-child(6) > input[type=\"radio\"]:nth-child(9)").check();
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click().wait(5000);
    cy.get("#filterFormDiv_filterSearchSelectContainer").should("not.be.visible").wait(10000);

    cy.get("#viewStatus_viewStatus_count").then(($countOfList) => {
      cy.wait(5000);
      const count = $countOfList.text();
      cy.expect(count).to.not.eq("0");
      cy.expect(count).to.not.eq(" --");
      cy.get(".viewStatusCommand").contains("Add To List").click().wait(20000);
      cy.get("#mygrid_container li span").should("be.visible").then(($tabCount) => {
        expect($tabCount.text()).to.include(count);
      });
    });
    // Remove List
    cy.get("#gridLayerCommands [title=\"Options\"]").click();
    cy.get(".balloon-items .insertText").contains("Remove List").should("be.visible").click();
    cy.wait(3000);
  });
  it("Do a Property Search with years built characteristic filter and last 12 months radio button selected. Add Results to list and click Remove list", () => {
    cy.get(".filterItemText").contains("Characteristics").click();
    // Input the years in the box.
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type=\"text\"]:nth-child(3)")
      .clear().type("2000");
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type=\"text\"]:nth-child(5)")
      .clear().type("2018");
    // Last Market Sale 12 months
    cy.get(".filterItemText").contains("Last Market Sale").click();
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(6) > div:nth-child(6) > input[type=\"radio\"]:nth-child(9)").check();
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click().wait(5000);
    cy.get("#filterFormDiv_filterSearchSelectContainer").should("not.be.visible").wait(10000);
    // Verify the count of the list is correct.
    cy.get("#viewStatus_viewStatus_count").then(($countOfList) => {
      cy.wait(5000);
      const count = $countOfList.text();
      cy.expect(count).to.not.eq("0");
      cy.expect(count).to.not.eq(" --");
      cy.get(".viewStatusCommand").contains("Add To List").click().wait(20000);
      cy.get("#mygrid_container li span").should("be.visible").then(($tabCount) => {
        expect($tabCount.text()).to.include(count);
      });
    });
    // Remove List
    cy.get("#gridLayerCommands [title=\"Options\"]").click();
    cy.get(".balloon-items .insertText").contains("Remove List").should("be.visible").click();
    cy.wait(3000);
  });
  // Property Search with owner name.
  it("Do a Property Search with owner name filter.", () => {
    cy.get(".filterItemText").contains("Property Type").click();
    cy.get("a").contains("Ownership").click();
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(8) > div:nth-child(6) > div:nth-child(6) > select").select("is");
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(8) > div:nth-child(6) > div:nth-child(6) > input[type=\"text\"]:nth-child(3)")
      .clear().type("John");
    cy.get("#filterFormDiv_filterFormContainer > button:nth-child(3) > span").click();
  });
  it("Do a Property Search with Styler Open", () => {
    cy.get(".filterItemText").contains("Characteristics").click();
    // Input the years in the box.
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type=\"text\"]:nth-child(3)")
      .clear().type("2000");
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type=\"text\"]:nth-child(5)")
      .clear().type("2018");
    // Last Market Sale 12 months
    cy.get(".filterItemText").contains("Last Market Sale").click();
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(6) > div:nth-child(6) > input[type=\"radio\"]:nth-child(9)").check();
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click().wait(5000);
    cy.get("#filterFormDiv_filterSearchSelectContainer").should("not.be.visible").wait(10000);
    // Verify the count of the list is correct.
    cy.get("#viewStatus_viewStatus_count").then(($countOfList) => {
      cy.wait(5000);
      const count = $countOfList.text();
      cy.expect(count).to.not.eq("0");
      cy.expect(count).to.not.eq(" --");
      cy.get(".viewStatusCommand").contains("Add To List").click().wait(20000);
      // Incremented even after a deleted list.
      cy.get("#mygrid_container li span").should("be.visible").then(($tabCount) => {
        expect($tabCount.text()).to.include(count);
      });
    });
    cy.get("#viewStatus_styler_toggler").click();
    cy.get("#viewStatus_styler_content").should("be.visible");
    // Remove List
    cy.get("#gridLayerCommands [title=\"Options\"]").click();
    cy.get(".balloon-items .insertText").contains("Remove List").should("be.visible").click();
    cy.wait(3000);
  });
  it("Do a Property Search with years built characteristic filter and last 12 months radio button selected. Add Results to list and save. Then delete the newly saved list.", () => {
    cy.get(".filterItemText").contains("Characteristics").click();
    // Input the years in the box.
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type=\"text\"]:nth-child(3)")
      .clear().type("2000");
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type=\"text\"]:nth-child(5)")
      .clear().type("2018");
    // Last Market Sale 12 months
    cy.get(".filterItemText").contains("Last Market Sale").click();
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(6) > div:nth-child(6) > input[type=\"radio\"]:nth-child(9)").check();
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click().wait(5000);
    cy.get("#filterFormDiv_filterSearchSelectContainer").should("not.be.visible").wait(10000);

    cy.get("#viewStatus_viewStatus_count").then(($countOfList) => {
      cy.wait(5000);
      const count = $countOfList.text();
      cy.expect(count).to.not.eq("0");
      cy.expect(count).to.not.eq(" --");
      cy.get(".viewStatusCommand").contains("Add To List").click().wait(20000);
      cy.get("#mygrid_container li span").should("be.visible").then(($tabCount) => {
        expect($tabCount.text()).to.include(count);
      });
    });
    // Get the results and save to USER folder.
    cy.get("[title=\"Save Results List\"]").click();
    cy.get("#mruDropDown_5").select("USER", {
      force: true,
    });
    cy.get(".mruFileInput").clear().type("SavedResultsTest");
    cy.get("#gridSaveAsOK").click().wait(5000);
    // Verify the title was saved accurately.
    cy.get("#mygrid_container li span").should("be.visible").then(($tabTitle) => {
      expect($tabTitle.text()).to.have.string("SavedResultsTest");
    });
    // Delete the newly created List
    cy.get("[title=\"Open Results List\"]").should("be.visible").click();
    cy.get(".insertText").contains("Browse Lists").click();
    cy.get("#mruDropDown_3").select("USER", {
      force: true,
    });
    cy.get(".browseTableSelect").first().select("SavedResultsTest");
    cy.get(".browseLists .ui-button-text").contains("Delete").click();
    cy.get(".browseLists .browseOK").first().click();
  });
  // Issues with Creating a target site from a result in the results list.
  it.skip("IN PROGRESS: Do a Property Search with years built characteristic filter and last 12 months radio button selected. Add Results to list and create a Target Site for first result.", () => {
    cy.get(".filterItemText").contains("Characteristics").click();
    // Input the years in the box.
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type=\"text\"]:nth-child(3)")
      .clear().type("2000");
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(6) > input[type=\"text\"]:nth-child(5)")
      .clear().type("2018");
    // Last Market Sale 12 months
    cy.get(".filterItemText").contains("Last Market Sale").click();
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(6) > div:nth-child(6) > input[type=\"radio\"]:nth-child(9)").check();
    cy.get("#filterFormDiv_filterFormContainer .ui-button-text").contains("OK").click().wait(5000);
    cy.get("#filterFormDiv_filterSearchSelectContainer").should("not.be.visible").wait(10000);

    cy.get("#viewStatus_viewStatus_count").then(($countOfList) => {
      cy.wait(5000);
      const count = $countOfList.text();
      cy.expect(count).to.not.eq("0");
      cy.expect(count).to.not.eq(" --");
      cy.get(".viewStatusCommand").contains("Add To List").click().wait(20000);
      cy.get("#mygrid_container li span").should("be.visible").then(($tabCount) => {
        expect($tabCount.text()).to.include(count);
      });
    });
    // Get the first result in the list and open the options. Click Create Target Site
    cy.get("#mygrid_container .objbox img").first().click({
      force: true,
    });
    cy.get("#mygrid_container .objbox img").first().click({
      force: true,
    });
    cy.get(".bt-content .balloon-items .insertText").contains("Create Target Site").click();
  });
  it("Do a Property Search with Aggregate Acreage field.", () => {
    cy.wait(10000);
    cy.get(".filterItemText").contains("Characteristics").click().wait(3000);
    // Input the years in the box.
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(30) > input[type=\"text\"]:nth-child(3)").clear().type(50);
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(30) > input[type=\"text\"]:nth-child(5)").clear().type(3000);
    cy.get("#filterFormDiv_filterFormContainer > button:nth-child(3) > span").click();
    cy.wait(5000);
  });
  it("Do a Property Search (Aggregate Acreage), zoom in one click and pan the map. Verify new results number appears.", () => {
    cy.wait(10000);
    cy.get(".filterItemText").contains("Characteristics").click().wait(3000);
    // Input the years in the box.
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(30) > input[type=\"text\"]:nth-child(3)").clear().type(50);
    cy.get("#filterFormDiv_filterFormContainer > div > div:nth-child(4) > div:nth-child(30) > input[type=\"text\"]:nth-child(5)").clear().type(3000);
    cy.get("#filterFormDiv_filterFormContainer > button:nth-child(3) > span").click();
    cy.get("#filterFormDiv_filterSearchSelectContainer").should("not.be.visible").wait(11000);
    cy.get("#viewStatus_viewStatus_count").then(($count) => {
      // Get the count of the first property search.
      const count1 = $count.text();
      expect(count1).to.not.equal("0");
      cy.wait(10000);
      // Zoom in on the map one click
      cy.get(".plus_in").click();
      cy.wait(3000);
      // Pan the Map to a new location
      cy.window().then((win) => {
        expect(win.Dmp).to.exist;
        // var map = win.Dmp.Map.getMap();
        // Lat y, Long x
        win.Dmp.Map.setView(win.map, {
          center: {
            x: -117.82495,
            y: 33.68862,
          },
          zoom: 15,
        });
      });
      // Give time for the map to load
      cy.wait(25000);
      cy.get("#viewStatus_viewStatus_count").then(($count2) => {
        const count2 = `${$count2.text()}`;
        expect(count1).to.not.equal(count2);
      });
    });
  });
});
