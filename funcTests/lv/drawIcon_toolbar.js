describe("Draw Icon Toolbar", () => {
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", {
      timeout: 40000,
    }).wait(20000);
    cy.get("#bookmarkIcon").click();
    cy.get(".insertText").contains("Irvine - Years Between").click().wait(8000);
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", {
        timeout: 40000,
      }).wait(30000);
      cy.get("#drawIcon").click();
      cy.get(".insertText").contains("Clear Drawings").click();
      cy.get(".ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text").contains("Yes").click();
      cy.wait(5000); // Give time for the command execute.
    }
  });

  it("Check Target Layer is on by default.", () => {
    cy.get("#drawIcon").click();
    cy.get(".balloon-header").contains("Markup").should("be.visible");
    cy.get("#drawIcon").click();
  });
  it("Verify Turn Measure On/Off functionality works.", () => {
    cy.get("#drawIcon").click();
    cy.get(".insertText").contains("Turn Measure On").should("be.visible").click();
    cy.get(".insertText").contains("Turn Measure Off").should("be.visible").click();
    cy.get(".insertText").contains("Turn Measure On").should("be.visible");
    cy.get("#drawIcon").click();
  });
  it("Draw Polygon", () => {
    cy.get("#drawIcon").click();
    cy.get(".insertText").contains("Draw Polygon").should("be.visible").click();
    cy.get("#MapPanel #geoStyler").should("be.visible").wait(4000);
    cy.wait(3000);
    // Draw the Polygon.
    cy.get("#labelCanvasId").click(1000, 850, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1250, 750, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1150, 550, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1350, 650, {
      force: true,
    });
    cy.get("#labelCanvasId").dblclick();
    // End draw
    cy.get("#geoStyler_accept").click().wait(2000);
    cy.wait(3000);
  });
  it("Draw Polygon and Click Copy Geometry with no fields inputted.", () => {
    cy.server();
    cy.route("POST", "/Transaction.aspx").as("getInfo");
    cy.get("#drawIcon").click();
    cy.get(".insertText").contains("Draw Polygon").should("be.visible").click();
    cy.get("#MapPanel #geoStyler").should("be.visible").wait(4000);
    // Draw the Polygon.
    cy.get("#labelCanvasId").click(900, 850, {
      force: true,
    });
    cy.get("#labelCanvasId").click(750, 750, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1150, 550, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1000, 650, {
      force: true,
    });
    cy.get("#labelCanvasId").dblclick();
    // End draw
    cy.get("#geoStyler_viewInfo").click();
    cy.get(".SE_unedit .command_name").contains("More").click();
    cy.get(".command_name").contains("Copy Geometry").click();
    cy.wait(5000);
    // Click Ok button
    cy.get(".editAttributeSubmit").last().click();
    // Copy Geometry Notes Modal
    cy.get(".editAttributeSubmit").last().click({
      force: true,
    });
    // ---Wait for the Transaction Request to appear.
    cy.wait("@getInfo").then(() => {
      cy.log("Success");
    });
  });
  it("Draw Line Command", () => {
    cy.get("#drawIcon").click();
    cy.get(".insertText").contains("Draw Line").should("be.visible").click();
    cy.get("#MapPanel #geoStyler").should("be.visible").wait(4000);
    // Draw the Polygon.
    cy.get("#labelCanvasId").click(1000, 850, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1250, 750, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1150, 550, {
      force: true,
    });
    cy.get("#labelCanvasId").dblclick();
    // End draw
    cy.get("#geoStyler_accept").click().wait(5000);
  });
  it("Draw Circle Command", () => {
    cy.get("#drawIcon").click();
    cy.get(".insertText").contains("Draw Circle").should("be.visible").click();
    cy.get("#MapPanel #geoStyler").should("be.visible").wait(4000);
    // Draw the Circle.
    cy.get("#labelCanvasId").click(800, 850, {
      force: true,
    });
    // cy.get('#labelCanvasId').click(1050,750, {force:true})
    // cy.get('#labelCanvasId').dblclick()
    // End draw
    cy.get("#geoStyler_accept").click().wait(5000);
  });
  it.skip("Draw Rectangle Command:IN PROGRESS", () => {
    cy.get("#drawIcon").click();
    cy.get(".insertText").contains("Draw Rectangle").should("be.visible").click();
    cy.get("#MapPanel #geoStyler").should("be.visible").wait(4000);
    // Draw the Rectangle.
    cy.get("#labelCanvasId").click(1000, 850, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1250, 750, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1150, 550, {
      force: true,
    });
    // cy.get('#labelCanvasId').click(1050,750, {force:true})
    // End draw
    cy.get("#geoStyler_accept").click().wait(5000);
  });
});

describe("Image Diff", () => {
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", {
      timeout: 40000,
    }).wait(30000);
    cy.get("#bookmarkIcon").click();
    cy.get(".insertText").contains("Irvine - Years Between").click().wait(8000);
    cy.get("#drawIcon").click();
    cy.get(".insertText").contains("Clear Drawings").click();
    cy.get(".ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text").contains("Yes").click();
    cy.wait(5000); // Give time for the command execute.
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", {
        timeout: 40000,
      }).wait(30000);
    }
  });

  it("Draw Polygon and check for Image diff. Test should fail", () => {
    cy.get("#drawIcon").click();
    cy.get(".insertText").contains("Draw Polygon").should("be.visible").click();
    cy.get("#MapPanel #geoStyler").should("be.visible").wait(4000);
    cy.wait(3000);

    cy.get("body").matchImageSnapshot();
    // Draw the Polygon.
    cy.get("#labelCanvasId").click(1000, 850, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1250, 750, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1150, 550, {
      force: true,
    });
    cy.get("#labelCanvasId").click(1350, 650, {
      force: true,
    });
    cy.get("#labelCanvasId").dblclick();
    // End draw
    cy.get("#geoStyler_accept").click().wait(2000);
    cy.wait(3000);

    // Compare the snapshots test should fail.
    cy.get("body").matchImageSnapshot();
  });
});

describe("Other Tests", () => {
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", {
      timeout: 40000,
    }).wait(20000);
    cy.get("#bookmarkIcon").click();
    cy.get(".insertText").contains("Irvine - Years Between").click().wait(8000);
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", {
        timeout: 40000,
      }).wait(20000);
    }
  });

  it("Pan the map", () => {
    cy.window().then((win) => {
      expect(win.Dmp).to.exist;
      // var map = win.Dmp.Map.getMap();
      const center = win.Dmp.Map.getCenter(win.map);
      cy.log(center);
      // Lat y, Long x
      win.Dmp.Map.setView(win.map, {
        center: {
          x: -117.9375,
          y: 33.93398,
        },
        zoom: 15,
      });
      cy.wait(2000);
    });
  });
  // Need to find a different way.
  it("Access Tooltip for a parcel (Show Location Information):IN PROGRESS", () => {
    cy.get("#labelCanvasId").trigger("mouseover", {
      clientX: 33.42827,
      clientY: -84.13005,
    });

    cy.get("#labelCanvasId").first().trigger("describemenu").then(() => {
      cy.wait(3000);
      // Click the menu item using JS
      cy.window().then((win) => {
        win.document.querySelector("#GoogleEarthMenu [title=\"Show Location Information\"]").click();
      });
    });
  });
});
