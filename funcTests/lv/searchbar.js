describe("Search Bar/Locate Tests", () => {
  // Initial start of the test
  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(10000);
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(10000);
    }
  });

  it("Search by Address", () => {
    const address = "273 N CALAVERAS ST, FRESNO, CA";
    // Enter a Address to search
    cy.searchAddress(address);
    cy.wait(10000);
    cy.get("#identify_results_section").should("be.visible");
  });
  it("Search by APN", () => {
    const apn = "650-663-30";
    // Enter a APN to search
    cy.searchAddress(apn);
    cy.wait(5000);
    cy.get("#identify_results_section").should("be.visible");
  });
  it("Search by Lat/Long", () => {
    const coordinates = "36.11158, -115.17558";
    // Enter a Lat/Long to search
    cy.get("#searchInputBox").clear().type(`${coordinates} {enter}`);
    cy.wait(10000);
    cy.get("#identify_results_section .title").contains(coordinates).should("be.visible");
  });
  it("Search by Street Intersection", () => {
    const intersection = "Long Beach Blvd & E Pacific Coast Hwy, Long Beach, CA 90813";
    // Enter a Intersection to search
    cy.get("#searchInputBox").clear().type(`${intersection} {enter}`);
    cy.wait(10000);
    cy.get("#identify_results_section .title").contains(intersection).should("be.visible");
  });
});
