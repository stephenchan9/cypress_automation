describe("Parcel Tests", () => {
  const title = Math.random().toString(36).substring(7);
  const noteType = Math.random().toString(36).substring(7);
  const note = Math.random().toString(36).substring(7);

  before(() => {
    cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", {
      timeout: 40000,
    }).wait(30000);
    cy.get("#bookmarkIcon").click();
    cy.get(".insertText").contains("Irvine - Years Between").click().wait(8000);
  });
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      cy.visit("https://localhost:44344/lib/3.0/App/LandVision/BDE/BDEST2_NewSummary.aspx?login=AutoUser1&account=AutomationBDE", { timeout: 40000 }).wait(30000);
    }
  });

  it("Parcel Copied 1.1: Verify Parcel is copied", () => {
    const address = "13322 ROPER AVE, NORWALK, CA 90650";
    cy.searchAddress(address);
    cy.get(".js-popout_card").click();
    // Poput should appear
    cy.get("[aria-labelledby=\"ui-dialog-title-SubjectSummary\"]").then(($modal) => {
      const display = $modal.css("display");
      cy.expect(display).to.equal("block");
    });
    cy.wait(5000);
    cy.get("body").screenshot("notCopied");
    cy.get("[aria-labelledby=\"ui-dialog-title-SubjectSummary\"] .ui-icon-closethick").click();
    // Popout should not appear.
    cy.get("[aria-labelledby=\"ui-dialog-title-SubjectSummary\"]").then(($modal) => {
      const display = $modal.css("display");
      cy.expect(display).to.equal("none");
    });
    cy.wait(5000);
    // Compare the snapshots test should fail.
    cy.get("body").screenshot("Copied");
    // Close all dialogs
    cy.get(".js-close_card > .fa").click();
  });
  it("Shared Documents 1.2: Manage Documents command. Upload a file and save.", () => {
    const address = "13322 ROPER AVE, NORWALK, CA 90650";
    cy.searchAddress(address);
    // ---Wait for the parcel to load

    cy.get(".title").contains("Shared Documents").click();
    cy.get(".command_name").contains("Manage Documents").scrollIntoView().click();
    cy.get(".ui-dialog-title").contains("Attachments").should("be.visible");
    cy.get("[aria-labelledby=\"ui-dialog-title-4\"] > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text").contains("Add").last().click();
    cy.upload_file2("TestAutomation.csv", "#attach_doc_browse_file");
    cy.get("#attach_doc_desc").type("Testing upload");
    cy.get(".ui-dialog-title").contains("Attach a file").should("be.visible").then(($modal) => {
      const id = $modal.attr("id");
      // Click the Ok Button
      cy.get(`[aria-labelledby=${id}] .ui-button-text`).contains("OK").click();
    });
    cy.get("#attach_doc_list_dialog a").contains("TestAutomation.csv").should("be.visible");
    // Remove the created attachment.
    cy.get("#attach_doc_list_dialog [title=\"Remove from list\"]").click();
    cy.get(".ui-button-text").contains("Yes").click();
    cy.get("[aria-labelledby=\"ui-dialog-title-4\"] > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text").contains("Close").click();
  });
  it("Shared Notes: Manage Notes. Upload a note and save.", () => {
    const address = "13322 ROPER AVE, NORWALK, CA 90650";
    cy.searchAddress(address);
    cy.get(".title").contains("Shared Notes").click();
    cy.get(".command_name").contains("Manage Notes").scrollIntoView().click();
    cy.get(".ui-dialog-title").contains("Notes").should("be.visible");
    cy.get("[aria-labelledby=\"ui-dialog-title-3\"] > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(1) > .ui-button-text").contains("Add").last().click({
      force: true,
    });
    cy.get(".editAttributeLabel").contains("Title").next().clear()
      .type(title);
    cy.get(".editAttributeLabel").contains("type").next().clear()
      .type(noteType);
    cy.get(".editAttributeLabel").contains("Note").next().clear()
      .type(note);

    cy.get(".editAttributeSubmit").click();
    cy.get("#AddNotesListDiv label u").first().contains(title).should("be.visible");
    // Remove the created attachment.
    cy.get("#AddNotesListDiv [title=\"Remove from list\"]").click();
    cy.get("[aria-labelledby=\"ui-dialog-title-3\"] > .ui-dialog-buttonpane > .ui-dialog-buttonset > :nth-child(2) > .ui-button-text").contains("Close").click();
  });
});
